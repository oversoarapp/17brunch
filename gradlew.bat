package tw.com.thinkPower.ThinkAPPrp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_new_process.*

class NewProcessFragment : Fragment() {

    override fun onCreateView (
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_new_process, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        newProcessBack.setOnClickListener {
            val manager = activity?.supportFragmentManager
            val transaction = manager?.beginTransaction()
            transaction?.replace(R.id.fragment_container, EditNewFragment())
            transaction?.commit()

            //TODO save process

        }

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when (tab?.position) {
                    0 -> {
                        if (!ProcessMainChildFragment().isAdded) {
                            val manager = childFragmentManager
                            val transaction = manager.beginTransaction()
                            transaction.add(R.id.childFrgment,ProcessMainChildFragment())
                            transaction.hide(ProcessCondiChildFragment())
                            transaction.commit()
                        } else {
                            val manager = childFragmentManager
                            val transaction = manager.beginTransaction()
                            transaction.hide(ProcessCondiChildFragment())
                            transaction.show()
                        }
                    }
                    1 -> {
                        