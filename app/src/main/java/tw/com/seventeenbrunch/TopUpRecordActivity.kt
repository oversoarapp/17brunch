package tw.com.seventeenbrunch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.search.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONObject
import tw.com.seventeenbrunch.Adapter.TopUpRecordAdapter
import tw.com.seventeenbrunch.Api.GetTopUpRecordApi
import tw.com.seventeenbrunch.Model.TopUpRecordDataModel
import tw.com.seventeenbrunch.Model.TopUpRecordModel
import tw.com.seventeenbrunch.Model.UserInfoModel
import java.io.IOException

class TopUpRecordActivity : AppCompatActivity() {

    companion object{
        const val RESPONSE = "response"
    }

    private var identity = ""
    private var nAdapter = TopUpRecordAdapter()
    private var topUpRecordDataModel: List<TopUpRecordDataModel>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search)

        initView()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("是否離開APP？")
                .setNegativeButton("確定") { dialog, which ->
                    finish()
                }
                .setPositiveButton("取消") { dialog, which ->
                    dialog.cancel()
                }
                .show()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun initView() {

        val response = intent.getStringExtra(RESPONSE)
        val listType = object : TypeToken<UserInfoModel>() {}.type
        val userInfoModel: UserInfoModel = Gson().fromJson(response, listType)
        val userInfoData = userInfoModel.result
        identity = userInfoData?.identity?:""

        searchRv.layoutManager = LinearLayoutManager(this)
        searchRv.adapter = nAdapter

        searchMenu.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        when (identity) {
            "1" -> {
                searchKey.hint = "使用期限"
                textView4.text = "店家儲值一覽"
                view7.setBackgroundColor(this.getColor(R.color.colorDarkPeach))
                layout.setBackgroundColor(this.getColor(R.color.colorLightPeach))
                GetTopUpRecordApi().getTopUpRecord("",userInfoData?.store?.getOrNull(0)?.id?:"","1",Callback())
                navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,intent.getStringExtra(RESPONSE)?:"",this).StoreMenu())

                searchKey.addTextChangedListener( object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        if (!topUpRecordDataModel.isNullOrEmpty()) {
                            if (s?.isNotEmpty() == true) {
                                val date = s.toString()
                                val newList =
                                    topUpRecordDataModel?.filter {
                                        it.use_time?.startsWith(date) ?: false
                                    }
                                nAdapter.updateData(newList,identity)
                            }
                        }
                    }
                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    }

                })
            }
            "0" -> {
                searchKey.hint = "儲值日期"
                navigation_view.menu.clear()
                navigation_view.inflateMenu(R.menu.customer_menu)
                textView4.text = "會員儲值一覽"
                view7.setBackgroundColor(this.getColor(R.color.colorYellow))
                layout.setBackgroundColor(this.getColor(R.color.memberMain))
                textView4.setTextColor(this.getColor(R.color.colorBlack))
                searchMenu.setImageResource(R.drawable.ic_menu_black_24dp)
                GetTopUpRecordApi().getTopUpRecord(userInfoData?.account?:"","","0",Callback())
                navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,intent.getStringExtra(RESPONSE)?:"",this).CustomerMenu())

                searchKey.addTextChangedListener( object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        if (!topUpRecordDataModel.isNullOrEmpty()) {
                            if (s?.isNotEmpty() == true) {
                                val date = s.toString()
                                val newList =
                                    topUpRecordDataModel?.filter {
                                        it.create_time?.startsWith(date) ?: false
                                    }
                                nAdapter.updateData(newList,identity)
                            }
                        }
                    }
                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    }

                })
            }
        }
    }

    inner class Callback : okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                e.printStackTrace()
                progressBar3.visibility = View.INVISIBLE
                android.app.AlertDialog.Builder(this@TopUpRecordActivity).setTitle("錯誤訊息")
                    .setMessage("無法正常執行，請稍後再試！")
                    .setPositiveButton("確定") { dialog, which ->
                        dialog.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {

            val mResponse = response.body()!!.string()
            val listType = object : TypeToken<TopUpRecordModel>() {}.type
            val topUpRecordModel: TopUpRecordModel = Gson().fromJson(mResponse.toString(), listType)
            topUpRecordDataModel = topUpRecordModel.result

            if (topUpRecordModel.success == true) {
                if (!topUpRecordDataModel.isNullOrEmpty()) {
                    runOnUiThread {
                        progressBar3.visibility = View.INVISIBLE
                        nAdapter.updateData(topUpRecordDataModel,identity)
                    }
                }
            } else {
                runOnUiThread {
                    progressBar3.visibility = View.INVISIBLE
                    android.app.AlertDialog.Builder(this@TopUpRecordActivity).setTitle("錯誤訊息")
                        .setMessage("資料或系統錯誤，請稍後再試！")
                        .setPositiveButton("確定") { dialog, which ->
                            dialog.cancel()
                        }
                        .show()
                }
            }
        }
    }
}