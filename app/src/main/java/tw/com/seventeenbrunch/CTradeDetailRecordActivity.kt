package tw.com.seventeenbrunch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_trade_detail_record.*
import okhttp3.Call
import okhttp3.Response
import org.json.JSONObject
import tw.com.seventeenbrunch.Adapter.TradeDetailRecordAdapter
import tw.com.seventeenbrunch.Api.GetTradeDetailRecordApi
import tw.com.seventeenbrunch.Model.TradeDetailRecordModel
import tw.com.seventeenbrunch.Model.TradeItemModel
import tw.com.seventeenbrunch.Model.UserInfoModel
import java.io.IOException
import java.net.UnknownHostException

class CTradeDetailRecordActivity : AppCompatActivity() {

    companion object {
        const val TRADENO = "tradeNo"
        const val TOTAL = "total"
        const val RESPONSE = "response"
    }

    private var total = 0
    private var tradeItemList: List<TradeItemModel>? = null
    private var tradeRecordModel: TradeDetailRecordModel? = null
    private var nAdapter = TradeDetailRecordAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trade_detail_record)

        initView()

    }

    private fun initView() {

//        val response = intent.getStringExtra(RESPONSE)
//        val listType = object : TypeToken<UserInfoModel>() {}.type
//        val userInfoModel: UserInfoModel = Gson().fromJson(response, listType)

        GetTradeDetailRecordApi().getTradeDetailRecord(intent.getStringExtra(TRADENO)?:"",Callback())

        total = intent?.getStringExtra(TOTAL)?.toInt()?:0
        tradeDetailRecordRv.layoutManager = LinearLayoutManager(this)
        tradeDetailRecordRv.adapter = nAdapter

        tradeDetailRecordMenu.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,intent.getStringExtra(RESPONSE)?:"",this).CustomerMenu())

    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                progressBar7.visibility = View.INVISIBLE
                androidx.appcompat.app.AlertDialog.Builder(this@CTradeDetailRecordActivity)
                    .setTitle("訊息提示")
                    .setMessage("無法正常執行，請稍後再試！")
                    .setCancelable(false)
                    .setPositiveButton("確定") {d,w ->
                        finish()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {

            val mResponse = response.body()?.string()
            val listType = object : TypeToken<TradeDetailRecordModel>() {}.type
            tradeRecordModel = Gson().fromJson(mResponse,listType)

            if (tradeRecordModel?.success == true && tradeRecordModel?.result?.item?.isNotEmpty() == true) {
                tradeItemList = tradeRecordModel?.result?.item
                var time = tradeRecordModel?.result?.createTime
                runOnUiThread {
                    progressBar7.visibility = View.INVISIBLE
                    nAdapter?.updateData(tradeItemList)
                    tradeDetailRecordNo.text = "訂單編號：${intent.getStringExtra(TRADENO)}"
                    tradeDetailRecordDate.text = "訂單日期：${time?.substring(0,time.length-3)}"
                    tradeDetailRecordStore.text = "店家：${tradeRecordModel?.result?.store}"
                    tradeDetailRecordName.text = "訂購人：${tradeRecordModel?.result?.userName}"
                    tradeDetailRecordPhone.text = "聯絡電話：${tradeRecordModel?.result?.account}"
                    val discount = tradeRecordModel?.result?.discount?.toInt()?:0
                    val total = intent?.getStringExtra(TOTAL)?.toInt()?:0
                    tradeDetailRecordTotal.text = "總計：$ $total"
                    tradeDetailRecordDiscount.text = "折扣：$ $discount"
                    tradeDetailRecordAmount.text = "小計：$ ${total+discount}"
                }
            } else {
                runOnUiThread {
                    progressBar7.visibility = View.INVISIBLE
                    androidx.appcompat.app.AlertDialog.Builder(this@CTradeDetailRecordActivity)
                        .setTitle("訊息提示")
                        .setMessage("資料或系統出現問題，請稍後再試！")
                        .setCancelable(false)
                        .setPositiveButton("確定") {d,w ->
                            finish()
                        }
                        .show()
                }
            }
        }
    }
}