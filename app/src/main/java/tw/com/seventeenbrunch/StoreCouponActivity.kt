package tw.com.seventeenbrunch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_store_coupon.*
import kotlinx.android.synthetic.main.activity_store_coupon.navigation_view
import okhttp3.Call
import okhttp3.Response
import org.json.JSONObject
import tw.com.seventeenbrunch.Api.*
import tw.com.seventeenbrunch.Model.*
import java.io.IOException

class StoreCouponActivity : AppCompatActivity() {

    companion object {
        const val USERRESPONSE = "userResponse"
    }
    
    private var mResponse = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_coupon)

        initView()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("是否離開APP？")
                .setNegativeButton("確定") { dialog, which ->
                    finish()
                }
                .setPositiveButton("取消") { dialog, which ->
                    dialog.cancel()
                }
                .show()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun initView() {

        mResponse = intent.getStringExtra(USERRESPONSE)?:""
        val listType = object : TypeToken<UserInfoModel>() {}.type
        val userInfoModel: UserInfoModel = Gson().fromJson(mResponse, listType)
        val userInfoData = userInfoModel.result
        storeCouponLimited.setText(userInfoData?.store?.getOrNull(0)?.limited?:"")
        storeCouponDiscount.setText(userInfoData?.store?.getOrNull(0)?.discount?:"")

        storeCouponDone.setOnClickListener {
            val limited = storeCouponLimited.text.toString()
            val discount = storeCouponDiscount.text.toString()
            if (limited != userInfoData?.store?.getOrNull(0)?.limited?:"" || discount != userInfoData?.store?.getOrNull(0)?.discount?:"") {
                AlertDialog.Builder(this)
                    .setTitle("訊息提示")
                    .setMessage("折扣金設定為:\n消費滿 $limited 元,折扣 $discount 元\n折扣金額將以此類推\n消費滿${limited.toInt()*2}元,折扣 ${discount.toInt()*2}元\n消費滿${limited.toInt()*3}元,折扣 ${discount.toInt()*3}元\n以上資訊是否正確？")
                    .setNegativeButton("確定") {d,w ->
                        progressBar5.visibility = View.VISIBLE
                        SetStoreCouponApi().setStoreCoupon(userInfoData?.store?.getOrNull(0)?.id?:"",limited,discount,Callback())
                    }
                    .setPositiveButton("取消") {d,w ->
                        d.cancel()
                    }
                    .show()
            } else {
                Toast.makeText(this,"資料未更動",Toast.LENGTH_LONG).show()
            }
        }

        storeCouponMenu.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        navigation_view.setNavigationItemSelectedListener (
            NavLis(drawerLayout,mResponse,this@StoreCouponActivity).StoreMenu()
        )
    }

    inner class Callback : okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                e.printStackTrace()
                progressBar5.visibility = View.INVISIBLE
                android.app.AlertDialog.Builder(this@StoreCouponActivity).setTitle("錯誤訊息")
                    .setMessage("無法正常執行，請稍後再試！")
                    .setPositiveButton("確定") { dialog, which ->
                        dialog.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {

            mResponse = response.body()?.string()?:""
            val jb = JSONObject(mResponse)
            if (jb.getBoolean("success")) {
                navigation_view.setNavigationItemSelectedListener (NavLis(drawerLayout,mResponse,this@StoreCouponActivity).StoreMenu())
                val info = jb.getJSONObject("result")
                val store = info.getJSONArray("store").getJSONObject(0)
                runOnUiThread {
                    Toast.makeText(this@StoreCouponActivity,"設定成功!",Toast.LENGTH_LONG).show()
                    storeCouponLimited.setText(store.getString("limited"))
                    storeCouponDiscount.setText(store.getString("discount"))
                    progressBar5.visibility = View.INVISIBLE
                }
            } else {
                runOnUiThread {
                    progressBar5.visibility = View.INVISIBLE
                    AlertDialog.Builder(this@StoreCouponActivity)
                        .setTitle("訊息提示")
                        .setMessage("系統或資料錯誤，請稍後再試！")
                        .setPositiveButton("確定") {d,w ->
                            d.cancel()
                        }
                        .show()
                }
            }
        }
    }
}