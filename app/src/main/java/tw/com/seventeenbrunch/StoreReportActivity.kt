package tw.com.seventeenbrunch

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_report.*
import java.text.SimpleDateFormat
import java.util.*
import android.widget.DatePicker
import android.app.DatePickerDialog
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.view.KeyEvent
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_report.drawerLayout
import kotlinx.android.synthetic.main.activity_report.navigation_view
import kotlinx.android.synthetic.main.activity_store_menu_detail.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONObject
import tw.com.seventeenbrunch.Adapter.ReportAdapter
import tw.com.seventeenbrunch.Api.GetReportApi
import tw.com.seventeenbrunch.Model.ReportModel
import tw.com.seventeenbrunch.Model.UserInfoModel
import java.io.IOException


class StoreReportActivity :AppCompatActivity() {

    companion object{
        const val RESPONSE = "response"
    }

    private var itemData = ""
    private var nAdapter: ReportAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report)

        initView()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            androidx.appcompat.app.AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("是否離開APP？")
                .setNegativeButton("確定") { dialog, which ->
                    finish()
                }
                .setPositiveButton("取消") { dialog, which ->
                    dialog.cancel()
                }
                .show()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun initView() {

        val response = intent.getStringExtra(RESPONSE)
        val listType = object : TypeToken<UserInfoModel>() {}.type
        val userInfoModel:UserInfoModel = Gson().fromJson(response, listType)
        val userDataModel = userInfoModel.result
        val storeId = userDataModel?.store?.getOrNull(0)?.id?:""

        reportStartDate.text = getNow()
        reportEndDate.text = getNow()

        nAdapter = ReportAdapter {
            val mIntent = Intent(this,ReportDetailActivity::class.java)
            mIntent.putExtra(ReportDetailActivity.ITEMDATA,itemData)
            mIntent.putExtra(ReportDetailActivity.USERRESPONSE,intent?.getStringExtra(RESPONSE))
            mIntent.putExtra(ReportDetailActivity.POSITION,it)
            startActivity(mIntent)
        }

        reportRv.adapter = nAdapter
        reportRv.layoutManager = LinearLayoutManager(this)

        GetReportApi().getReport(storeId,getNow(),getNow(),Callback())

        reportMenu.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,intent.getStringExtra(RESPONSE)?:"",this).StoreMenu())

        reportStartDate.setOnClickListener {
            datePicker(reportStartDate)
        }

        reportEndDate.setOnClickListener {
            datePicker(reportEndDate)
        }

        reportSearch.setOnClickListener {
            if (reportStartDate.text.toString().isNotEmpty() && reportEndDate.text.toString().isNotEmpty()) {
                progressBar15.visibility = View.VISIBLE
                GetReportApi().getReport(storeId,reportStartDate.text.toString(),reportEndDate.text.toString(),Callback())
            }
        }

        tabLayout.addOnTabSelectedListener(object :TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab?) {
                val c = Calendar.getInstance()
                when (tab?.text) {
                    "日統計" -> {
                        reportStartDate.text = getNow()
                        reportEndDate.text = getNow()
                        progressBar15.visibility = View.VISIBLE
                        GetReportApi().getReport(storeId,getNow(),getNow(),Callback())
                    }
                    "年統計" -> {
                        val year = c.get(Calendar.YEAR)
                        reportStartDate.text = "$year-01-01"
                        reportEndDate.text = getNow()
                        progressBar15.visibility = View.VISIBLE
                        GetReportApi().getReport(storeId,"$year-01-01",getNow(),Callback())
                    }
                    "月統計" -> {
                        val m = String.format("%02d",c.get(Calendar.MONTH)+1)
                        reportStartDate.text = c.get(Calendar.YEAR).toString()+"-"+m+"-"+"01"
                        reportEndDate.text = getNow()
                        progressBar15.visibility = View.VISIBLE
                        GetReportApi().getReport(storeId,c.get(Calendar.YEAR).toString()+"-"+m+"-"+"01",getNow(),Callback())
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                val c = Calendar.getInstance()
                when (tab?.text) {
                    "日統計" -> {
                        reportStartDate.text = getNow()
                        reportEndDate.text = getNow()
                        progressBar15.visibility = View.VISIBLE
                        GetReportApi().getReport(storeId,getNow(),getNow(),Callback())
                    }
                    "年統計" -> {
                        val year = c.get(Calendar.YEAR)
                        reportStartDate.text = "$year-01-01"
                        reportEndDate.text = getNow()
                        progressBar15.visibility = View.VISIBLE
                        GetReportApi().getReport(storeId,"$year-01-01",getNow(),Callback())
                    }
                    "月統計" -> {
                        val m = String.format("%02d",c.get(Calendar.MONTH)+1)
                        reportStartDate.text = c.get(Calendar.YEAR).toString()+"-"+m+"-"+"01"
                        reportEndDate.text = getNow()
                        progressBar15.visibility = View.VISIBLE
                        GetReportApi().getReport(storeId,c.get(Calendar.YEAR).toString()+"-"+m+"-"+"01",getNow(),Callback())
                    }
                }
            }
        })
    }

    private fun datePicker(dateView: TextView) {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        DatePickerDialog(dateView.context,
            DatePickerDialog.OnDateSetListener { view, year, month, day ->
                var dateTime = if (day.toString().length == 1) {
                    "$year-0${month + 1}-0$day"
                } else {
                    "$year-0${month + 1}-$day"
                }
                dateView.text = dateTime
            }, year, month, day
        ).show()
    }

    private fun getNow(): String {
        return if (android.os.Build.VERSION.SDK_INT >= 24){
            SimpleDateFormat("yyyy-MM-dd").format(Date())
        }else{
            var tms = Calendar.getInstance()
            tms.get(Calendar.YEAR).toString() + "-" + tms.get(Calendar.MONTH).toString() + "-" + tms.get(
                Calendar.DAY_OF_MONTH).toString()
        }
    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
        }

        override fun onResponse(call: Call, response: Response) {

            itemData = response.body()?.string()?:""
            val listType = object :TypeToken<ReportModel>(){}.type
            val reportModel: ReportModel = Gson().fromJson(itemData,listType)
            if (reportModel.success ) {
                runOnUiThread {
                    var total = reportModel.result?.total?:0
                    var discount = reportModel.result?.discount?:0
                    reportTotal.text = "總營業額: $ ${total+discount} 元 "
                    reportOrderQty.text = "總單量: ${reportModel.result?.orderQty} 單 "
                    reportDiscount.text = "總折扣: $ ${reportModel.result?.discount} 元 "
                    progressBar15.visibility = View.INVISIBLE
                    nAdapter?.updateData(reportModel.result.detail)
                }
            } else {
                runOnUiThread {
                    progressBar15.visibility = View.INVISIBLE
                    reportTotal.text = ""
                    reportOrderQty.text = ""
                    reportDiscount.text = ""
                    nAdapter?.updateData(null)
                    AlertDialog.Builder(this@StoreReportActivity)
                        .setTitle("訊息提示")
                        .setMessage("目前無統計資料")
                        .show()
                }
            }
        }
    }
}