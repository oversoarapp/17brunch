package tw.com.seventeenbrunch

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import java.io.*
import java.net.Socket;
import java.net.UnknownHostException
import java.nio.charset.Charset
import java.util.logging.Handler

/**
 * Created by haoguibao on 16/2/18.
 * Description : 封装Pos机打印工具类
 * Revision :
 */
class Pos {
    //定义编码方式
    var context: Context? = null
    public var encoding: Charset? = null
    public var sock : Socket? = null
    // 通过socket流进行读写
    public var socketOut: OutputStream? = null
    public var writer: OutputStreamWriter? = null

    /**
     * 初始化Pos实例
     *
     * @param ip 打印机IP
     * @param port  打印机端口号
     * @param encoding  编码
     * @throws IOException
     */
    fun pos(mContext: Context,ip:String, port:Int, encoding:Charset ) {
        try {
            context = mContext
            sock = Socket(ip, port)
            socketOut = DataOutputStream(sock!!.getOutputStream())
            this.encoding = encoding
            writer = OutputStreamWriter(socketOut!!, encoding)
        } catch (e: UnknownHostException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    /**
     * 关闭IO流和Socket
     *
     * @throws IOException
     */
    public fun closeIOAndSocket() {

        try {
            writer?.close()
            socketOut?.close()
            sock?.close()
        } catch (e:IOException) {
            e.printStackTrace()
        }
    }


    /**
     * 打印二维码
     *
     * @param qrData 二维码的内容
     * @throws IOException
     */
    public fun qrCode(qrData: String) {
        try {
            var moduleSize = 8
            var length = qrData.toByteArray(charset("GBK")).size

            //打印二维码矩阵
            writer?.write(0x1D);// init
            writer?.write("(k");// adjust height of barcode
            writer?.write(length + 3); // pl
            writer?.write(0); // ph
            writer?.write(49); // cn
            writer?.write(80); // fn
            writer?.write(48); //
            writer?.write(qrData);

            writer?.write(0x1D);
            writer?.write("(k");
            writer?.write(3);
            writer?.write(0);
            writer?.write(49);
            writer?.write(69);
            writer?.write(48);

            writer?.write(0x1D);
            writer?.write("(k");
            writer?.write(3);
            writer?.write(0);
            writer?.write(49);
            writer?.write(67);
            writer?.write(moduleSize);

            writer?.write(0x1D);
            writer?.write("(k");
            writer?.write(3); // pl
            writer?.write(0); // ph
            writer?.write(49); // cn
            writer?.write(81); // fn
            writer?.write(48); // m

            writer?.flush();
        }catch (e:IOException) {
            e.printStackTrace()
        }
    }

    /**
     * 进纸并全部切割
     *
     * @return
     * @throws IOException
     */
    public fun feedAndCut() {

        try {
            writer?.write(0x1D)
            writer?.write(86)
            writer?.write(65)
            //        writer?.write(0)
            //切纸前走纸多少
            writer?.write(100)
            writer?.flush()
        }catch (e:IOException){
            e.printStackTrace()
        }

        //另外一种切纸的方式
        //        byte[] bytes = {29, 86, 0};
        //        socketOut.write(bytes);
    }

    /**
     * 打印换行
     *
     * @return length 需要打印的空行数
     * @throws IOException
     */
    public fun printLine(lineNum: Int) {
        try {
            for (i in 0 until lineNum) {
                writer?.write("\n")
            }
            writer?.flush()
        }catch (e:IOException) {
            e.printStackTrace()
        }
    }

    /**
     * 打印换行(只换一行)
     *
     * @throws IOException
     */
    public fun printLine() {
        try {
            writer?.write("\n")
            writer?.flush()
        }catch (e:IOException) {
            e.printStackTrace()
        }
    }

    /**
     * 打印空白(一个Tab的位置，约4个汉字)
     *
     * @param length 需要打印空白的长度,
     * @throws IOException
     */
    public fun printTabSpace(length: Int) {
        try {
            for (i in 0 until length) {
                writer?.write("\t")
            }
            writer?.flush()
        }catch (e:IOException) {
            e.printStackTrace()
        }
    }

    /**
     * 打印空白（一个汉字的位置）
     *
     * @param length 需要打印空白的长度,
     * @throws IOException
     */
    public fun printWordSpace(length: Int) {


        try {
            for (i in 0 until length) {
                writer?.write("  ")
            }
            writer?.flush()
        }catch (e:IOException) {
            e.printStackTrace()
        }

    }

    /**
     * 打印位置调整
     *
     * @param position 打印位置  0：居左(默认) 1：居中 2：居右
     * @throws IOException
     */
    public fun printLocation(position: Int) {
        try {
            writer?.write(0x1B)
            writer?.write(97)
            writer?.write(position)
            writer?.flush()
        }catch (e:IOException) {
            e.printStackTrace()
        }
    }

    /**
     * 绝对打印位置
     *
     * @throws IOException
     */
    public fun printLocation(light: Int,weight: Int) {

        try {
            writer?.write(0x1B);
            writer?.write(0x24);
            writer?.write(light);
            writer?.write(weight);
            writer?.flush();
        }catch (e:IOException){
            e.printStackTrace()
        }
    }


    /**
     * 打印文字
     *
     * @param text
     * @throws IOException
     */
    public fun printText(text: String) {

        var s = text
        var content = s.toByteArray(charset("gbk"))
        socketOut?.write(content);
        socketOut?.flush();
    }

    /**
     * 新起一行，打印文字
     *
     * @param text
     * @throws IOException
     */
    public fun printTextNewLine(text: String) {
        //换行

        try {
            writer?.write("\n")
            writer?.flush()
            val content = text.toByteArray(charset("gbk"))
            socketOut?.write(content)
            socketOut?.flush()
        }catch (e:IOException) {
            e.printStackTrace()
        }
    }


    /**
     * 初始化打印机
     *
     * @throws IOException
     */
    fun initPos() {
        try {
            writer?.write(0x1B);
            writer?.write(0x40);
            writer?.flush();
        }catch (e:IOException) {
            e.printStackTrace()
        }
    }

    /**
     * 加粗
     *
     * @param flag false为不加粗
     * @return
     * @throws IOException
     */
    public fun bold(flag:Boolean) {
        try {
            if (flag) {
                //常规粗细
                writer?.write(0x1B);
                writer?.write(69);
                writer?.write(0xF);
                writer?.flush();
            } else {
                //加粗
                writer?.write(0x1B);
                writer?.write(69);
                writer?.write(0);
                writer?.flush();
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}