package tw.com.seventeenbrunch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import tw.com.seventeenbrunch.Adapter.StoreMenuDetailAdapter
import kotlinx.android.synthetic.main.activity_store_menu_detail.*
import kotlinx.android.synthetic.main.activity_store_menu_detail.drawerLayout
import okhttp3.Call
import okhttp3.Response
import org.json.JSONException
import org.json.JSONObject
import tw.com.seventeenbrunch.Api.ActionToMenuItemApi
import tw.com.seventeenbrunch.Api.ActionToMenuTypeApi
import tw.com.seventeenbrunch.Api.GetMenuApi
import tw.com.seventeenbrunch.Model.OrderMenuItemModel
import tw.com.seventeenbrunch.Model.OrderMenuModel
import java.io.IOException

class StoreMenuDetailActivity : AppCompatActivity() {

    companion object {
        const val POSITION = "position"
        const val TYPENAME = "typeName"
        const val USERSTOREID = "userStoreId"
        const val USERRESPONSE = "userResponse"
    }

    private var action = ""
    private var storeId = ""
    private var menuType = ""
    private var nAdapter : StoreMenuDetailAdapter? = null
    private var menuItemList:List<OrderMenuItemModel>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_menu_detail)

        initView()

    }

    private fun initView() {

//        val mResponse = intent.getStringExtra(USERRESPONSE)
//        val listType = object : TypeToken<UserInfoModel>() {}.type
//        val userInfoModel: UserInfoModel = Gson().fromJson(mResponse, listType)

        storeId = intent.getStringExtra(USERSTOREID)?:""
        storeMenuDetailTitle.setText(intent.getStringExtra(TYPENAME))

        GetMenuApi().getMenu(storeId,Callback())

        val removeData:(Int) -> Unit = {
            action = "刪除"
            progressBar9.visibility = View.VISIBLE
            ActionToMenuItemApi().actionToMenuItem(menuItemList?.getOrNull(it)?.id?:"",menuItemList?.getOrNull(it)?.name?:"","Delete","","",storeId,"",Callback())
        }

        val editData:(Int,String,String) -> Unit = {position,newName,price ->
            if (menuItemList?.getOrNull(position)?.name != newName || menuItemList?.getOrNull(position)?.price != price ) {
                action = "修改"
                progressBar9.visibility = View.VISIBLE
                ActionToMenuItemApi().actionToMenuItem(menuItemList?.getOrNull(position)?.id?:"",menuItemList?.getOrNull(position)?.name?:"","Edit","",price,storeId,newName, Callback())
            }
        }

        storeMenuDetailRv.layoutManager = LinearLayoutManager(this)
        nAdapter = StoreMenuDetailAdapter(this,removeData,editData)
        storeMenuDetailRv.adapter = nAdapter

        storeMenuDetailMenu.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,intent.getStringExtra(USERRESPONSE)?:"",this).StoreMenu())

        storeMenuDetailEdit.setOnClickListener {
            when (storeMenuDetailEdit.text) {
                "修改" -> {
                    storeMenuDetailTitle.isEnabled = true
                    storeMenuDetailTitle.isFocusableInTouchMode = true
                    storeMenuDetailEdit.text = "完成"
                }
                "完成" -> {
                    progressBar9.visibility = View.VISIBLE
                    ActionToMenuTypeApi().actionToMenuType(menuType,"Edit",storeMenuDetailTitle.text.toString(),storeId,EditMenuTypeCallback())
                }
            }
        }

        storeMenuDetailAdd.setOnClickListener {
            var itemName = storeMenuDetailName.text.toString()
            var itemPrice = storeMenuDetailPrice.text.toString()
            if (itemName.isNotEmpty() && itemPrice.isNotEmpty()) {
                action = "新增"
                progressBar9.visibility = View.VISIBLE
                ActionToMenuItemApi().actionToMenuItem("",itemName,"Add",menuType,itemPrice,storeId,"",Callback()
                )
            }
        }
    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                android.app.AlertDialog.Builder(this@StoreMenuDetailActivity).setTitle("錯誤訊息")
                    .setMessage("無法正常執行，請稍後再試！")
                    .setPositiveButton("確定") { dialog, which ->
                        dialog.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {

            val result = response.body()!!.string()
            Log.d("actionToMenuItem",result)
            val listType = object : TypeToken<OrderMenuModel>() {}.type
            val orderMenuModel: OrderMenuModel = Gson().fromJson(result.toString(), listType)
            if (orderMenuModel.success == true && !orderMenuModel.result.isNullOrEmpty()) {
                //菜單項目
                var menuList = orderMenuModel.result.getOrNull(intent.getIntExtra(POSITION,0))
                var itemNameList = mutableListOf<String>()
                var itemPriceList = mutableListOf<String>()
                //項目中的細項
                menuType = menuList?.id?:""
                menuItemList = menuList?.item
                if (!menuItemList.isNullOrEmpty()) {
                    for (i in 0 until menuItemList?.size!!) {
                        itemNameList.add(menuItemList?.getOrNull(i)?.name?: "")
                        itemPriceList.add(menuItemList?.getOrNull(i)?.price?:"")
                    }
                }
                runOnUiThread {
                    if (action.isNotEmpty()) {
                        Toast.makeText(
                            this@StoreMenuDetailActivity,
                            "${action}成功！",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (action == "新增") {
                        storeMenuDetailName.text.clear()
                        storeMenuDetailPrice.text.clear()
                    }
                    progressBar9.visibility = View.INVISIBLE
                    nAdapter?.updateData(itemNameList,itemPriceList)
                }
            } else {
                runOnUiThread {
                    val msg = orderMenuModel.msg
                    runOnUiThread {
                        progressBar9.visibility = View.INVISIBLE
                        android.app.AlertDialog.Builder(this@StoreMenuDetailActivity)
                            .setTitle("錯誤訊息")
                            .setMessage(msg)
                            .setPositiveButton("確定") { dialog, which ->
                                dialog.cancel()
                            }
                            .show()
                    }
                }
            }
        }
    }

    inner class EditMenuTypeCallback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                android.app.AlertDialog.Builder(this@StoreMenuDetailActivity).setTitle("錯誤訊息")
                    .setMessage("無法正常執行，請稍後再試！")
                    .setPositiveButton("確定") { dialog, which ->
                        dialog.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {

            val mResponse = response.body()?.string()
            try {
                val jsonObject = JSONObject(mResponse)
                val success = jsonObject.getBoolean("success")
                if (success) {
                    runOnUiThread {
                        Toast.makeText(this@StoreMenuDetailActivity,"修改成功！",Toast.LENGTH_SHORT).show()
                        storeMenuDetailEdit.text = "修改"
                        progressBar9.visibility = View.INVISIBLE
                        storeMenuDetailTitle.isEnabled = false
                        storeMenuDetailTitle.isFocusableInTouchMode = false
                    }
                } else {
                    runOnUiThread {
                        android.app.AlertDialog.Builder(this@StoreMenuDetailActivity).setTitle("錯誤訊息")
                            .setMessage("網路或資料出現錯誤，請稍後再試！")
                            .setPositiveButton("確定") { dialog, which ->
                                dialog.cancel()
                            }
                            .show()
                        progressBar9.visibility = View.INVISIBLE
                        storeMenuDetailEdit.text = "修改"
                        storeMenuDetailTitle.isEnabled = false
                        storeMenuDetailTitle.isFocusableInTouchMode = false
                    }
                }
            }catch (e:JSONException) {
                e.printStackTrace()
            }
        }
    }
}