package tw.com.seventeenbrunch

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_store_main.*
import okhttp3.Call
import okhttp3.Response
import tw.com.seventeenbrunch.Api.EditInfoApi
import tw.com.seventeenbrunch.Api.EditStoreApi
import tw.com.seventeenbrunch.Api.LoginApi
import tw.com.seventeenbrunch.Model.UserInfoDataModel
import tw.com.seventeenbrunch.Model.UserInfoModel
import tw.com.seventeenbrunch.Model.UserInfoStoreModel
import java.io.IOException

class StoreActivity : AppCompatActivity() {

    companion object {
        const val USERRESPONSE = "userResponse"
    }

    private var userInfoData: UserInfoDataModel? = null
    private var userInfoStoreData:UserInfoStoreModel? = null
    private var flag = 0
    private var mResponse = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_main)

        initView()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("是否離開APP？")
                .setNegativeButton("確定") { dialog, which ->
                    finish()
                }
                .setPositiveButton("取消") { dialog, which ->
                    dialog.cancel()
                }
                .show()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun initView() {

        mResponse = intent.getStringExtra(USERRESPONSE)
        val listType = object : TypeToken<UserInfoModel>() {}.type
        val userInfoModel: UserInfoModel = Gson().fromJson(mResponse, listType)
        userInfoData = userInfoModel.result
        userInfoStoreData = userInfoModel.result?.store?.get(0)
        checkValues()
        //關閉狀態 -> status == 0，反之 == 1
        if (userInfoStoreData?.status == "0") {
            val array = resources.getStringArray(R.array.store_status_array)
            val adapter: ArrayAdapter<String> =
                ArrayAdapter<String>(this, R.layout.spinner_white_item,array)
            adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
            storeMainSpinner.adapter = adapter
            storeMainSpinner.setSelection(1)
            storeMainSpinner.background = resources.getDrawable(R.drawable.spinner_red_shape)
        } else {
            val array = resources.getStringArray(R.array.store_status_array)
            val adapter: ArrayAdapter<String> =
                ArrayAdapter<String>(this, R.layout.spinner_black_item,array)
            adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
            storeMainSpinner.adapter = adapter
            storeMainSpinner.setSelection(0)
            storeMainSpinner.background = resources.getDrawable(R.drawable.spinner_green_shape)
        }

        storeMainName.setText(userInfoStoreData?.store_name)
        storeMainPhone.setText(userInfoStoreData?.phone)
        storeMainAds.setText(userInfoStoreData?.address)
        storeMainBh.setText(userInfoStoreData?.business_hours)
        storeMainEd.text = "${userInfoStoreData?.expiry_date}天"
        storeMainMb.text = userInfoData?.account?:""
        storeMainUserName.setText(userInfoData?.name)
        storeMainUserEmail.setText(userInfoData?.email)

        storeRefresh.setOnClickListener {
            var acc = userInfoData?.account?:""
            var pwd = userInfoData?.password?:""
            var identity = userInfoData?.identity.toString()
            var token = userInfoData?.token?:""
            progressBar2.visibility = View.VISIBLE
            LoginApi().login(acc,pwd,identity,token,RefreshCallback())
        }

        storeMainMenu.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        storeMainSpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var mStatus = ""
                when (storeMainSpinner.selectedItemPosition) {
                    0 -> mStatus = "1"
                    1 -> mStatus = "0"
                }
                if (userInfoStoreData?.status != mStatus) {
                    flag = 0
                    progressBar2.visibility = View.VISIBLE
                    EditStoreApi().editStore(
                        userInfoStoreData?.id ?: "",
                        userInfoStoreData?.store_name ?: "",
                        userInfoStoreData?.address ?: "",
                        userInfoStoreData?.phone ?: "",
                        userInfoStoreData?.business_hours ?: "",
                        mStatus,
                        Callback()
                    )
                }
            }
        }

        navigation_view.setNavigationItemSelectedListener (NavLis(drawerLayout,mResponse,this).StoreMenu())

        storeMainEdit.setOnClickListener {
            when (storeMainEdit.text) {
                "修改" -> {
//                    storeMainSpinner.isEnabled = true
                    storeMainName.isFocusableInTouchMode = true
                    storeMainName.isEnabled = true
                    storeMainPhone.isFocusableInTouchMode = true
                    storeMainPhone.isEnabled = true
                    storeMainAds.isFocusableInTouchMode = true
                    storeMainAds.isEnabled = true
                    storeMainBh.isFocusableInTouchMode = true
                    storeMainBh.isEnabled = true
                    storeMainUserEmail.isFocusableInTouchMode = true
                    storeMainUserEmail.isEnabled = true
                    storeMainUserName.isFocusableInTouchMode = true
                    storeMainUserName.isEnabled = true
                    storeMainEdit.text = "完成"
                }
                "完成" -> {
                    val id = userInfoStoreData?.id ?: ""
                    val name = storeMainName.text.toString()
                    val phone = storeMainPhone.text.toString()
                    val ads = storeMainAds.text.toString()
                    val bh = storeMainBh.text.toString()
                    var status = ""
                    when (storeMainSpinner.selectedItemPosition) {
                        0 -> status = "1"
                        1 -> status = "0"
                    }
                    val userName = storeMainUserName.text.toString()
                    val userEmail = storeMainUserEmail.text.toString()
                    if (userName != userInfoData?.name || userEmail != userInfoData?.email) {
                        //個人資訊有變更
                        flag = 1
                    }
                    if (name != userInfoStoreData?.store_name || phone != userInfoStoreData?.phone || ads != userInfoStoreData?.address || bh != userInfoStoreData?.business_hours || status != userInfoStoreData?.status) {
                        //如果店家資料有變更
                        progressBar2.visibility = View.VISIBLE
                        EditStoreApi().editStore(id, name, ads, phone, bh, status, Callback())
                    } else if (flag == 1) {
                        //如果只有個人資料變更
                        flag = 0
                        progressBar2.visibility = View.VISIBLE
                        EditInfoApi().editInfo(
                            userInfoData?.account ?: "",
                            userName,
                            userEmail,
                            "",
                            userInfoData?.store?.getOrNull(0)?.id?:"",
                            "1",
                            Callback()
                        )
                    } else {
                        storeMainEdit.text = "修改"
//                        storeMainSpinner.isEnabled = false
                        storeMainName.isFocusableInTouchMode = false
                        storeMainName.isEnabled = false
                        storeMainPhone.isFocusableInTouchMode = false
                        storeMainPhone.isEnabled = false
                        storeMainAds.isFocusableInTouchMode = false
                        storeMainAds.isEnabled = false
                        storeMainBh.isFocusableInTouchMode = false
                        storeMainBh.isEnabled = false
                        storeMainUserName.isEnabled = false
                        storeMainUserName.isFocusableInTouchMode = false
                        storeMainUserEmail.isEnabled = false
                        storeMainUserEmail.isFocusableInTouchMode = false
                    }
                }
            }
        }
    }

//    private fun countDate(end:String):Int {
//        var dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
//        var startTime: Date = dateFormat.parse(getNow())
//        var endTime: Date = dateFormat.parse(end)
//        val diff = startTime.time - endTime.time
//        var days = diff / (1000 * 60 * 60 * 24)
//        return days.toInt()
//    }

//    private fun getNow(): String {
//        return if (android.os.Build.VERSION.SDK_INT >= 24){
//            SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(Date())
//        }else{
//            var tms = Calendar.getInstance()
//            tms.get(Calendar.YEAR).toString() + "-" + tms.get(Calendar.MONTH).toString() + "-" + tms.get(Calendar.DAY_OF_MONTH).toString() + " " + tms.get(Calendar.HOUR_OF_DAY).toString() + ":" + tms.get(Calendar.MINUTE).toString() +":" + tms.get(Calendar.SECOND).toString() +"." + tms.get(Calendar.MILLISECOND).toString()
//        }
//    }

    private fun checkValues() {
        var ed = userInfoStoreData?.expiry_date?:0
        if (ed == 0) {
            flag = 2
            AlertDialog.Builder(this@StoreActivity)
                .setTitle("訊息提示")
                .setCancelable(false)
                .setMessage("APP使用期限已經到期，暫停使用，敬請儲值續約！")
                .setPositiveButton("確定") { d, w ->
                    if (userInfoStoreData?.status != "0") {
                        EditStoreApi().editStore(
                            userInfoStoreData?.id ?: "",
                            userInfoStoreData?.store_name ?: "",
                            userInfoStoreData?.address ?: "",
                            userInfoStoreData?.phone ?: "",
                            userInfoStoreData?.business_hours ?: "",
                            "0",
                            Callback()
                        )
                    } else {
                        finish()
                    }
                }
                .show()
        }
        if ( ed in 1..7 ){
            AlertDialog.Builder(this@StoreActivity)
                .setTitle("訊息提示")
                .setMessage("APP使用期限即將到期，敬請儲值續約！")
                .setPositiveButton("確定") { d, w ->
                    d.cancel()
                }
                .show()
        }
    }

    inner class RefreshCallback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                e.printStackTrace()
                progressBar2.visibility = View.INVISIBLE
                android.app.AlertDialog.Builder(this@StoreActivity).setTitle("錯誤訊息")
                    .setMessage("無法正常執行，請稍後再試！")
                    .setPositiveButton("確定") { dialog, which ->
                        dialog.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {
            mResponse = response.body()?.string()?:""
            val listType = object : TypeToken<UserInfoModel>() {}.type
            val userInfoModel: UserInfoModel = Gson().fromJson(mResponse, listType)
            if (userInfoModel.success == true) {
                userInfoData = userInfoModel.result
                userInfoStoreData = userInfoData?.store?.getOrNull(0)
                runOnUiThread {
                    progressBar2.visibility = View.INVISIBLE
                    checkValues()
                    //關閉狀態 -> status == 0，反之 == 1
                    if (userInfoStoreData?.status == "0") {
                        val array = resources.getStringArray(R.array.store_status_array)
                        val adapter: ArrayAdapter<String> =
                            ArrayAdapter<String>(this@StoreActivity, R.layout.spinner_white_item,array)
                        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                        storeMainSpinner?.adapter = adapter
                        storeMainSpinner.setSelection(1)
                        storeMainSpinner.background = resources.getDrawable(R.drawable.spinner_red_shape)
                    } else {
                        val array = resources.getStringArray(R.array.store_status_array)
                        val adapter: ArrayAdapter<String> =
                            ArrayAdapter<String>(this@StoreActivity, R.layout.spinner_black_item,array)
                        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                        storeMainSpinner?.adapter = adapter
                        storeMainSpinner.setSelection(0)
                        storeMainSpinner.background = resources.getDrawable(R.drawable.spinner_green_shape)
                    }
                    storeMainName.setText(userInfoStoreData?.store_name)
                    storeMainPhone.setText(userInfoStoreData?.phone)
                    storeMainAds.setText(userInfoStoreData?.address)
                    storeMainBh.setText(userInfoStoreData?.business_hours)
                    storeMainEd.text = "${userInfoStoreData?.expiry_date}天"
                    storeMainMb.text = userInfoData?.account ?: ""
                    storeMainUserName.setText(userInfoData?.name)
                    storeMainUserEmail.setText(userInfoData?.email)
                }
            }
        }
    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                e.printStackTrace()
                progressBar2.visibility = View.INVISIBLE
                android.app.AlertDialog.Builder(this@StoreActivity).setTitle("錯誤訊息")
                    .setMessage("無法正常執行，請稍後再試！")
                    .setPositiveButton("確定") { dialog, which ->
                        dialog.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {

            mResponse = response.body()?.string()?:""
            val listType = object : TypeToken<UserInfoModel>() {}.type
            val userInfoModel:UserInfoModel = Gson().fromJson(mResponse, listType)

            if (userInfoModel.success == true) {   
                userInfoData = userInfoModel.result
                userInfoStoreData = userInfoModel.result?.store?.get(0)
                navigation_view.setNavigationItemSelectedListener (NavLis(drawerLayout,mResponse,this@StoreActivity).StoreMenu())
                when (flag) {
                    //0為預設(只有店家變更or個人資料變更)
                    0 -> {
                        runOnUiThread {
                            progressBar2.visibility = View.INVISIBLE
                            storeMainEdit.text = "修改"
                            Toast.makeText(this@StoreActivity, "修改成功！", Toast.LENGTH_LONG).show()
                            storeMainName.isFocusableInTouchMode = false
                            storeMainName.isEnabled = false
                            storeMainPhone.isFocusableInTouchMode = false
                            storeMainPhone.isEnabled = false
                            storeMainAds.isFocusableInTouchMode = false
                            storeMainAds.isEnabled = false
                            storeMainBh.isFocusableInTouchMode = false
                            storeMainBh.isEnabled = false
                            storeMainUserName.isEnabled = false
                            storeMainUserName.isFocusableInTouchMode = false
                            storeMainUserEmail.isEnabled = false
                            storeMainUserEmail.isFocusableInTouchMode = false

                            when (userInfoStoreData?.status) {
                                "0" -> {
                                    val array = resources.getStringArray(R.array.store_status_array)
                                    val adapter: ArrayAdapter<String> =
                                        ArrayAdapter<String>(this@StoreActivity, R.layout.spinner_white_item,array)
                                    adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                    storeMainSpinner?.adapter = adapter
                                    storeMainSpinner.setSelection(1)
                                    storeMainSpinner.background = resources.getDrawable(R.drawable.spinner_red_shape)
                                }
                                "1" -> {
                                    val array = resources.getStringArray(R.array.store_status_array)
                                    val adapter: ArrayAdapter<String> =
                                        ArrayAdapter<String>(this@StoreActivity, R.layout.spinner_black_item,array)
                                    adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
                                    storeMainSpinner?.adapter = adapter
                                    storeMainSpinner.setSelection(0)
                                    storeMainSpinner.background = resources.getDrawable(R.drawable.spinner_green_shape)
                                }
                            }

                        }
                    }
                    1 -> {
                        //個人資料有變更，打api、將flag改為預設值0
                        flag = 0
                        val userName = storeMainUserName.text.toString()
                        val userEmail = storeMainUserEmail.text.toString()
                        runOnUiThread {
                            EditInfoApi().editInfo(userInfoData?.account?:"",userName,userEmail,"",userInfoData?.store?.getOrNull(0)?.id?:"", "1",Callback())
                        }
                    }
                    2 -> {
                        //app使用期限到期，強制關閉接單狀態
                        flag = 0
                        runOnUiThread {
                            finish()
                        }
                    }
                }
            } else {
                runOnUiThread {
                    Toast.makeText(this@StoreActivity, "修改失敗，請稍後再試！", Toast.LENGTH_LONG).show()
                    progressBar2.visibility = View.INVISIBLE
                }
            }
        }
    }
}