package tw.com.seventeenbrunch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_customer_info.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONObject
import tw.com.seventeenbrunch.Adapter.StoreListAdapter
import tw.com.seventeenbrunch.Api.EditInfoApi
import tw.com.seventeenbrunch.Model.UserInfoDataModel
import tw.com.seventeenbrunch.Model.UserInfoModel
import java.io.IOException

class CustomerInfoActivity : AppCompatActivity() {

    companion object{
        const val RESPONSE = "response"
    }

//    private var mHeight = 0
    private var nAdapter:StoreListAdapter?= null
    private var mResponse = ""
    private var userInfoData:UserInfoDataModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_info)

        initView()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("是否離開APP？")
                .setNegativeButton("確定") { dialog, which ->
                    finish()
                }
                .setPositiveButton("取消") { dialog, which ->
                    dialog.cancel()
                }
                .show()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun initView() {

        mResponse = intent.getStringExtra(RESPONSE)
        val listType = object : TypeToken<UserInfoModel>() {}.type
        val userInfoModel: UserInfoModel = Gson().fromJson(mResponse, listType)
        userInfoData = userInfoModel.result

//        val lp = view21.layoutParams
//        mHeight = lp.height
//        if (userInfoData?.address?.length?:0 >= 10) {
//            lp.height = mHeight+100
//            view21.layoutParams = lp
//        }
        customerInfoName.setText(userInfoData?.name)
        customerInfoEmail.setText(userInfoData?.email)
        customerInfoAds.setText(userInfoData?.address)
        customerInfoPhone.text = userInfoData?.account

        nAdapter = StoreListAdapter {id,points ->
            var store = ""
            userInfoData?.store?.forEach {model->
                if (model.id != id) {
                    store += "${model.id},"
                }
            }

            if (store.endsWith(",")) {
                store = store.substring(0,store.length-1 )
            }

            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("您在此店家的餘額為$ $points \n 是否確認刪除此店家？")
                .setNegativeButton("確定") {d,w ->
                    EditInfoApi().editInfo(
                        userInfoData?.account ?: "",
                        userInfoData?.name?:"",
                        userInfoData?.email?:"",
                        userInfoData?.address?:"",
                        store,
                        "0",
                        Callback()
                    )
                }
                .setPositiveButton("取消") {d,w ->
                    d.cancel()
                }
                .show()
        }
        customerInfoRv.layoutManager = LinearLayoutManager(this)
        customerInfoRv.adapter = nAdapter
        nAdapter?.updateData(userInfoData?.points)

        customerInfoMenu.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        navigation_view.setNavigationItemSelectedListener(
            NavLis(drawerLayout,mResponse,this).CustomerMenu()
        )

        customerInfoDone.setOnClickListener {
            var store = ""
            userInfoData?.store?.forEach {model->
                store += "${model.id},"
            }
            val name = customerInfoName.text.toString()
            val email = customerInfoEmail.text.toString()
            val ads = customerInfoAds.text.toString()
            if (name != userInfoData?.name || email != userInfoData?.email || ads != userInfoData?.address) {
                progressBar14.visibility = View.VISIBLE
                EditInfoApi().editInfo(
                    userInfoData?.account ?: "",
                    name,
                    email,
                    ads,
                    store.substring(0,store.length-1),
                    "0",
                    Callback()
                )
            } else {
                Toast.makeText(this,"資料未變更",Toast.LENGTH_SHORT).show()
            }
        }

//        customerInfoAds.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(s: Editable?) {
//                if (s?.isNotEmpty() == true) {
//                    if (s.length >= 10) {
//                        lp.height = mHeight+100
//                        view21.layoutParams = lp
//                    } else {
//                        lp.height = mHeight
//                        view21.layoutParams = lp
//                    }
//                }
//            }

//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
//            }
//
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//            }
//
//        })

    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                e.printStackTrace()
                progressBar14.visibility = View.INVISIBLE
                android.app.AlertDialog.Builder(this@CustomerInfoActivity).setTitle("錯誤訊息")
                    .setMessage("無法正常執行，請稍後再試！")
                    .setPositiveButton("確定") { dialog, which ->
                        dialog.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {
            mResponse = response.body()?.string()?:""
            val listType = object : TypeToken<UserInfoModel>() {}.type
            val userInfoModel: UserInfoModel = Gson().fromJson(mResponse, listType)
            userInfoData = userInfoModel.result
            if (userInfoModel.success == true){
                runOnUiThread {
                    navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,mResponse,this@CustomerInfoActivity).CustomerMenu())
                    progressBar14.visibility = View.INVISIBLE
                    nAdapter?.updateData(userInfoData?.points)
                    Toast.makeText(this@CustomerInfoActivity,"修改成功！",Toast.LENGTH_LONG).show()
                }
            } else {
                runOnUiThread {
                    progressBar14.visibility = View.INVISIBLE
                    android.app.AlertDialog.Builder(this@CustomerInfoActivity).setTitle("錯誤訊息")
                        .setMessage("系統或資料有誤，請稍後再試！")
                        .setPositiveButton("確定") { dialog, which ->
                            dialog.cancel()
                        }
                        .show()
                }
            }
        }
    }
}