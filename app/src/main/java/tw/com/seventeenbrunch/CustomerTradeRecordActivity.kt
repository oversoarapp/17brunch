package tw.com.seventeenbrunch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_customer_trade_record.*
import kotlinx.android.synthetic.main.customer_info_dialog.*
import okhttp3.Call
import okhttp3.Response
import org.json.JSONObject
import tw.com.seventeenbrunch.Adapter.CustomerTradeRecordAdapter
import tw.com.seventeenbrunch.Api.GetTradeRecordApi
import tw.com.seventeenbrunch.Model.TradeRecordDataModel
import tw.com.seventeenbrunch.Model.TradeRecordModel
import tw.com.seventeenbrunch.Model.UserInfoModel
import java.io.IOException

class CustomerTradeRecordActivity : AppCompatActivity(){

    companion object {
        const val USERRESPONSE = "userResponse"
    }

    private var nAdapter: CustomerTradeRecordAdapter? = null
    private var tradeRecordDataList: List<TradeRecordDataModel>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_trade_record)

        initView()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("是否離開APP？")
                .setNegativeButton("確定") { dialog, which ->
                    finish()
                }
                .setPositiveButton("取消") { dialog, which ->
                    dialog.cancel()
                }
                .show()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun initView() {

        val response = intent.getStringExtra(USERRESPONSE)
        val listType = object : TypeToken<UserInfoModel>() {}.type
        val userInfoModel: UserInfoModel = Gson().fromJson(response, listType)
        val userInfo = userInfoModel.result

        customerInfoName.text = userInfo?.name
        var points = ""
        for (i in 0 until userInfo?.points?.size!!) {
            points += "${userInfo.points.getOrNull(i)?.store_name}:${userInfo.points.getOrNull(i)?.points}元"
        }
        customerInfoPoints.text = points
        val userAcc = userInfo?.account?:""
        GetTradeRecordApi().getTradeRecord(userAcc,"0","0",Callback())

        nAdapter = CustomerTradeRecordAdapter(this) {
            val mIntent = Intent()
            mIntent.putExtra(CTradeDetailRecordActivity.TRADENO,tradeRecordDataList?.getOrNull(it)?.trade_no)
            mIntent.putExtra(CTradeDetailRecordActivity.TOTAL,tradeRecordDataList?.getOrNull(it)?.total)
            mIntent.putExtra(CTradeDetailRecordActivity.RESPONSE,intent.getStringExtra(
                USERRESPONSE))
            mIntent.setClass(this,CTradeDetailRecordActivity::class.java)
            startActivity(mIntent)
        }

        customerTradeRv.layoutManager = LinearLayoutManager(this)
        customerTradeRv.adapter = nAdapter
        customerTradeTabLayout.addTab(customerTradeTabLayout.newTab())
        customerTradeTabLayout.addTab(customerTradeTabLayout.newTab())
        customerTradeTabLayout.getTabAt(0)?.text = "目前訂單"
        customerTradeTabLayout.getTabAt(1)?.text = "過去訂單"

        customerTradeMenu.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,intent.getStringExtra(USERRESPONSE)?:"",this).CustomerMenu())

        customerTradeTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
                progressBar.visibility = View.VISIBLE
                GetTradeRecordApi().getTradeRecord(userAcc,"0",tab?.position.toString(),Callback())
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                progressBar.visibility = View.VISIBLE
                GetTradeRecordApi().getTradeRecord(userAcc,"0",tab?.position.toString(),Callback())
            }
        })
    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                progressBar.visibility = View.INVISIBLE
                AlertDialog.Builder(this@CustomerTradeRecordActivity)
                    .setTitle("訊息提示")
                    .setMessage("無法正常執行，請稍後再試！")
                    .setCancelable(false)
                    .setPositiveButton("確定") {d,w ->
                        finish()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {

            val mResponse = response.body()?.string()
            val listType = object : TypeToken<TradeRecordModel>() {}.type
            val tradeRecordModel: TradeRecordModel = Gson().fromJson(mResponse,listType)

            if (tradeRecordModel.success) {
                if (tradeRecordModel.result.isNotEmpty()) {
                    tradeRecordDataList = tradeRecordModel.result
                    runOnUiThread {
                        progressBar.visibility = View.INVISIBLE
                        nAdapter?.updateData(tradeRecordDataList)
                    }
                } else {
                    runOnUiThread {
                        progressBar.visibility = View.INVISIBLE
                        nAdapter?.updateData(null)
                        AlertDialog.Builder(this@CustomerTradeRecordActivity)
                            .setTitle("訊息提示")
                            .setMessage("目前尚無訂單紀錄！")
                            .setPositiveButton("確定") {d,w ->
                                d.cancel()
                            }
                            .show()
                    }
                }
            } else {
                runOnUiThread {
                    progressBar.visibility = View.INVISIBLE
                    nAdapter?.updateData(null)
                    AlertDialog.Builder(this@CustomerTradeRecordActivity)
                        .setTitle("訊息提示")
                        .setMessage("目前尚無訂單紀錄！")
                        .setPositiveButton("確定") {d,w ->
                            d.cancel()
                        }
                        .show()
                }
            }
        }
    }
}