package tw.com.seventeenbrunch

import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import tw.com.seventeenbrunch.Api.ForgetPwdApi
import kotlinx.android.synthetic.main.activity_forget_pwd.*
import okhttp3.Call
import okhttp3.Response
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException

class ForgetPwdActivity: AppCompatActivity(){

  private var identity = ""

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_forget_pwd)

    initView()

  }

  private fun initView() {

    forgetPwdDone.setOnClickListener {
      val acc = forgetPwdAccount.text.toString()
      val email = forgetPwdEmail.text.toString()

      if (acc.isNotEmpty() && email.isNotEmpty() && identity != "") {
        progressBar.visibility = View.VISIBLE
        ForgetPwdApi().forgetPwd(acc,identity,email,Callback())
      } else {
        Toast.makeText(this,"欄位與身分不可空白！",Toast.LENGTH_LONG).show()
      }
    }

    forgetPwdCancel.setOnClickListener {
      finish()
    }

    radioGroup2.setOnCheckedChangeListener { group, checkedId ->
      when (checkedId) {
        R.id.customer -> {
          identity = "0"
        }
        R.id.store -> {
          identity = "1"
        }
      }
    }
  }

  inner class Callback: okhttp3.Callback {

    override fun onFailure(call: Call, e: IOException) {
      runOnUiThread {
        e.printStackTrace()
        progressBar.visibility = View.INVISIBLE
        android.app.AlertDialog.Builder(this@ForgetPwdActivity)
          .setTitle("錯誤訊息")
          .setMessage("無法正常執行，請稍後再試！" )
          .setPositiveButton("關閉") { dialog, which -> dialog.cancel() }
          .show()
      }
    }

    override fun onResponse(call: Call, response: Response) {
      try {
        val result = response.body()?.string()?:""
        val jsonObject = JSONObject(result)
        val success = jsonObject.getBoolean("success")
        runOnUiThread {
          if (success) {
            progressBar.visibility = View.INVISIBLE
            android.app.AlertDialog.Builder(this@ForgetPwdActivity).setTitle("訊息提示")
              .setMessage("已將密碼改為預設值：000000")
              .setCancelable(false)
              .setPositiveButton("確定") { dialog, which -> finish() }
              .show()
          } else {
            progressBar.visibility = View.INVISIBLE
            android.app.AlertDialog.Builder(this@ForgetPwdActivity).setTitle("訊息提示")
              .setMessage("網路或資料錯誤，請稍後再試！")
              .setCancelable(false)
              .setPositiveButton("關閉") { dialog, which -> dialog.cancel() }
              .show()
          }
        }
      } catch (e:JSONException) {
        e.printStackTrace()
      }
    }
  }
}