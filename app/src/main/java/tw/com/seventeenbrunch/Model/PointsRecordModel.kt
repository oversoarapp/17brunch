package tw.com.seventeenbrunch.Model

data class PointsRecordModel(
    val store_name:String? = null,
    val create_time:String? = null,
    val points:String? = null,
    val expiry_date:String? = null,
    val use_time:String? = null
)