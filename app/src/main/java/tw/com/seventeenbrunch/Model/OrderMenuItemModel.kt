package tw.com.seventeenbrunch.Model

data class OrderMenuItemModel(
    val name: String? = null,
    val menu_type: Int? = null,
    val price: String? = null,
    val id:String? =null
)