package tw.com.seventeenbrunch.Model

data class ReportModel(
    val msg: String,
    val result: ReportDataModel,
    val success: Boolean
)