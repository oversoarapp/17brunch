package tw.com.seventeenbrunch.Model

data class TradeDetailRecordModel(
    val msg: String? = null,
    val result: TradeDetailRecordDataModel? = null,
    val success: Boolean? = null
)