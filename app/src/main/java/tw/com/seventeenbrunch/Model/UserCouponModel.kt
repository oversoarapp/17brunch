package tw.com.seventeenbrunch.Model

data class UserCouponModel(
    val msg: String,
    val result: List<UserCouponDataModel>,
    val success: Boolean
)