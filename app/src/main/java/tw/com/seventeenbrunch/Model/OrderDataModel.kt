package tw.com.seventeenbrunch.Model

data class OrderDataModel (
    var itemName: String? = null,
    var itemQty: Int? = null,
    var itemPrice: Int? = null,
    var itemDec: String? = null,
    var itemAmount: Int? = null,
    var menuType: String? = null
)
