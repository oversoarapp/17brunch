package tw.com.seventeenbrunch.Model

data class ReportDataModel(
    val detail: List<ReportItemModel>? = null,
    val orderQty: Int? = null,
    val total: Int? = null,
    val discount:Int? = null
)