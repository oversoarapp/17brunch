package tw.com.seventeenbrunch.Model

data class ReportItemModel(
    val items: List<ReportItemDetailModel>? = null,
    val menuType: String? = null,
    val menuName: String? = null,
    val itemQty: String? = null,
    val amount: String? = null
)