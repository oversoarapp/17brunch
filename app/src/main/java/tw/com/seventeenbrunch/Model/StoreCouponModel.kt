package tw.com.seventeenbrunch.Model

data class StoreCouponModel(
    val msg: String? = null,
    val result: List<StoreCouponDataModel>? = null,
    val success: Boolean? = null
)