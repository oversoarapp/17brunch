package tw.com.seventeenbrunch.Model

data class UserCouponDataModel(
    val menu_type: String? = null,
    val points:  Int? = null,
    val limited: Int? = null,
    val discount: Int? = null,
    val name: String? = null
)