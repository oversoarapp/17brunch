package tw.com.seventeenbrunch.Model

data class TradeDetailRecordDataModel(
    val account: String? = null,
    val createTime: String? = null,
    val item: List<TradeItemModel>? = null,
    val tradeNo: String? = null,
    val userName: String? = null,
    val store:String? = null,
    val discount:String? = null
)