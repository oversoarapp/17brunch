package tw.com.seventeenbrunch.Model

data class UserInfoStoreModel(
    val address: String? = null,
    val business_hours: String? = null,
    val create_time: String? = null,
    val id: String? = null,
    val phone: String? = null,
    val points: Int? = null,
    val expiry_date:Int? = null,
    val store_name: String? = null,
    val limited: String? = null,
    val discount: String? = null,
    val status: String? = null,
    val mobilePhone:String? = null
)