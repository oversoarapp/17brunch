package tw.com.seventeenbrunch.Model

data class OrderMenuTypeModel(
    val id: String? = null,
    val name: String? = null,
    val item: List<OrderMenuItemModel>? = null
)