package tw.com.seventeenbrunch.Model

data class ReportItemDetailModel(
    val item_name: String? = null,
    val item_qty: String? = null,
    val item_price:String? = null,
    val amount:String? = null
)