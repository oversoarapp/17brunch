package tw.com.seventeenbrunch.Model

data class TopUpRecordDataModel(
    val account:String? = null,
    val create_time: String? = null,
    val expiry_date: String? = null,
    val points: String? = null,
    val status: Int? = null,
    val store_id: Int? = null,
    val store_name: String? = null,
    val top_up_no: String? = null,
    val use_time:String? = null
)