package tw.com.seventeenbrunch.Model

data class AllStoreModel(
    val msg: String? = null,
    val result: List<AllStoreInfoModel>? = null,
    val success: Boolean? = null
)