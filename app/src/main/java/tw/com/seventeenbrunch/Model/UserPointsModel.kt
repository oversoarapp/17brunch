package tw.com.seventeenbrunch.Model

data class UserPointsModel (
    val id:String? = null,
    val points:Int? = null,
    val store_name:String? = null
)