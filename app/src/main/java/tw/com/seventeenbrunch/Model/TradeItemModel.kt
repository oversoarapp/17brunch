package tw.com.seventeenbrunch.Model

data class TradeItemModel(
    val item_name: String? = null,
    val item_qty: Int? = null,
    val item_dec: String? = null,
    val price:String? = null,
    val amount: String? = null
)