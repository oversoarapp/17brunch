package tw.com.seventeenbrunch.Model

data class TradeRecordModel(
    val msg: String,
    val result: List<TradeRecordDataModel>,
    val success: Boolean
)