package tw.com.seventeenbrunch.Model

data class CouponUsedDataModel(
    var couponName: String? = null,
    var couponType: String? = null,
    var couponPoints: String? = null
)