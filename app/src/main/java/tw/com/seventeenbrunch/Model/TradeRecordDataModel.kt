package tw.com.seventeenbrunch.Model

data class TradeRecordDataModel(
    val account: String? = null,
    val create_time: String? = null,
    val id: Int? = null,
    val status: String? = null,
    val store: String? = null,
    val trade_no: String? = null,
    val total: String? = null
)