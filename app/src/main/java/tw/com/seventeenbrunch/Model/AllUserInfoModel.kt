package tw.com.seventeenbrunch.Model

data class AllUserInfoModel(
    val account: String? = null,
    val address: String? = null,
    val create_time: String? = null,
    val email: String? = null,
    val id: String? = null,
    val identity: String? = null,
    val name: String? = null,
    val password: String? = null,
    val points: String? = null,
    val status: Int? = null,
    val token: String? = null,
    val pointsRecord:PointsRecordModel? = null
)