package tw.com.seventeenbrunch.Model

data class AllStoreInfoModel(
    val id: String? = null,
    val store_name: String? = null,
    val address: String? = null,
    val phone: String? = null,
    val business_hours: String? = null,
    val points: String? = null,
    val status: String? = null,
    val create_time: String? = null,
    val expiry_date: String? = null,
    val pointsRecord: PointsRecordModel? = null
)