package tw.com.seventeenbrunch.Model

import java.io.Serializable

data class UserInfoModel(
    val msg: String? = null,
    val result: UserInfoDataModel? = null,
    val success: Boolean? = null
)