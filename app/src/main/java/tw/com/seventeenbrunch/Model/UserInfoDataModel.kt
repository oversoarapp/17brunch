package tw.com.seventeenbrunch.Model

data class UserInfoDataModel(
    val account: String? = null,
    val address: String? = null,
    val create_time: String? = null,
    val email: String? = null,
    val id: Int? = null,
    val identity: String? = null,
    val name: String? = null,
    var password: String? = null,
    val points: List<UserPointsModel>? = null,
    val status: Int? = null,
    val store: List<UserInfoStoreModel>? = null,
    val token: String? = null
)