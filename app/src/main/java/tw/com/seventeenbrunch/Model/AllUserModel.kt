package tw.com.seventeenbrunch.Model

data class AllUserModel(
    val msg: String? = null,
    val result: List<AllUserInfoModel>? = null,
    val success: Boolean? = null
)