package tw.com.seventeenbrunch.Model

data class OrderMenuModel(
    val result: List<OrderMenuTypeModel>,
    val msg: String? = null,
    val success: Boolean? = null
)