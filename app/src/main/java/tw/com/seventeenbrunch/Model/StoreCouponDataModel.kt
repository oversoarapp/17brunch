package tw.com.seventeenbrunch.Model

data class StoreCouponDataModel(
    val discount: String? = null,
    val id: String? = null,
    val limited: String? = null,
    val menu_type: String? = null,
    val name: String? = null,
    val store_id: String? = null
)