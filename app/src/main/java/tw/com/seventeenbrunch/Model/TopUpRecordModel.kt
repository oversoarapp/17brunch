package tw.com.seventeenbrunch.Model

data class TopUpRecordModel(
    val msg: String? = null,
    val result: List<TopUpRecordDataModel>? = null,
    val success: Boolean? = null
)