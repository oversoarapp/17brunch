package tw.com.seventeenbrunch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.search.*
import okhttp3.Call
import okhttp3.Response
import org.json.JSONObject
import tw.com.seventeenbrunch.Adapter.MemberSearchAdapter
import tw.com.seventeenbrunch.Api.GetAllUserApi
import tw.com.seventeenbrunch.Model.AllUserInfoModel
import tw.com.seventeenbrunch.Model.AllUserModel
import tw.com.seventeenbrunch.Model.UserInfoModel
import java.io.IOException

class StoreMemberListActivity : AppCompatActivity() {

    companion object{
        const val USERRESPONSE = "userResponse"
        const val FROM = "from"
    }

    private var allUserInfoModel: List<AllUserInfoModel>? = null
    private var nAdapter = MemberSearchAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search)

        initView()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("是否離開APP？")
                .setNegativeButton("確定") { dialog, which ->
                    finish()
                }
                .setPositiveButton("取消") { dialog, which ->
                    dialog.cancel()
                }
                .show()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun initView() {

        if (intent?.getStringExtra(FROM) == "store") {
            textView4.text = "會員資訊一覽"
        }

        navigation_view.menu.clear()
        navigation_view.inflateMenu(R.menu.store_menu)
        val mResponse = intent?.getStringExtra(USERRESPONSE)
        val listType = object :TypeToken<UserInfoModel>(){}.type
        val userModel:UserInfoModel = Gson().fromJson(mResponse,listType)

        view7.setBackgroundColor(this.getColor(R.color.colorDarkPeach))
        layout.setBackgroundColor(this.getColor(R.color.colorLightPeach))
        textView4.setTextColor(this.getColor(R.color.colorWhite))

        searchRv.layoutManager = LinearLayoutManager(this)
        searchRv.adapter = nAdapter
        GetAllUserApi().getAllUser(userModel.result?.store?.get(0)?.id?:"",Callback())

        searchKey.addTextChangedListener( object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if (!allUserInfoModel.isNullOrEmpty()) {
                    if (s?.isNotEmpty() == true) {
                        val num = s.toString()
                        val newList =
                            allUserInfoModel?.filter {
                                it.account?.startsWith(num) ?: false
                            }
                        nAdapter.updateData(newList)
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })

        searchMenu.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,mResponse?:"",this).StoreMenu())

    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                e.printStackTrace()
                progressBar3.visibility = View.INVISIBLE
                android.app.AlertDialog.Builder(this@StoreMemberListActivity).setTitle("錯誤訊息")
                    .setMessage("無法正常執行，請稍後再試！")
                    .setPositiveButton("確定") { dialog, which ->
                        dialog.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {

            val mResponse = response.body()!!.string()
            val listType = object : TypeToken<AllUserModel>() {}.type
            val allUserModel: AllUserModel = Gson().fromJson(mResponse.toString(), listType)
            allUserInfoModel = allUserModel.result

            if (allUserModel.success == true) {
                if (!allUserInfoModel.isNullOrEmpty()) {
                    runOnUiThread {
                        progressBar3.visibility = View.INVISIBLE
                        nAdapter.updateData(allUserInfoModel)
                    }
                } else {
                    runOnUiThread {
                        progressBar3.visibility = View.INVISIBLE
                        AlertDialog.Builder(this@StoreMemberListActivity)
                            .setTitle("訊息提示")
                            .setMessage("目前無資料！")
                            .show()
                    }
                }
            } else {
                runOnUiThread {
                    progressBar3.visibility = View.INVISIBLE
                    AlertDialog.Builder(this@StoreMemberListActivity)
                        .setTitle("訊息提示")
                        .setMessage("目前無資料！")
                        .setPositiveButton("確定") { dialog, which ->
                            dialog.cancel()
                        }
                        .show()
                }
            }
        }
    }
}