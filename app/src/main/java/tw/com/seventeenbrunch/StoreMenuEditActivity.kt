package tw.com.seventeenbrunch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_store_menu_detail.*
import tw.com.seventeenbrunch.Adapter.StoreMenuEditAdapter
import kotlinx.android.synthetic.main.activity_store_menu_edit.*
import kotlinx.android.synthetic.main.activity_store_menu_edit.drawerLayout
import kotlinx.android.synthetic.main.activity_store_menu_edit.navigation_view
import okhttp3.Call
import okhttp3.Response
import org.json.JSONObject
import tw.com.seventeenbrunch.Api.ActionToMenuTypeApi
import tw.com.seventeenbrunch.Api.GetMenuApi
import tw.com.seventeenbrunch.Model.OrderMenuModel
import tw.com.seventeenbrunch.Model.OrderMenuTypeModel
import tw.com.seventeenbrunch.Model.UserInfoDataModel
import tw.com.seventeenbrunch.Model.UserInfoModel
import java.io.IOException

class StoreMenuEditActivity : AppCompatActivity() {

    companion object {
        const val USERRESPONSE = "userResponse"
    }

    private var storeId = ""
    private var flag = 0
    private var storeMenuEditAdapter: StoreMenuEditAdapter? = null
    private var menuList:List<OrderMenuTypeModel>? = null
    private var userInfoData: UserInfoDataModel? = null

    override fun onRestart() {
        super.onRestart()

        progressBar8.visibility = View.VISIBLE
        GetMenuApi().getMenu(storeId,Callback())

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_menu_edit)

        initView()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("是否離開APP？")
                .setNegativeButton("確定") { dialog, which ->
                    finish()
                }
                .setPositiveButton("取消") { dialog, which ->
                    dialog.cancel()
                }
                .show()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun initView() {

        val response = intent.getStringExtra(USERRESPONSE)
        val listType = object : TypeToken<UserInfoModel>() {}.type
        val userInfoModel: UserInfoModel = Gson().fromJson(response, listType)
        userInfoData = userInfoModel.result

        storeId = userInfoData?.store?.getOrNull(0)?.id?:""
        GetMenuApi().getMenu(storeId,Callback())

        val removeData:(String)->Unit = {
            progressBar8.visibility = View.VISIBLE
            ActionToMenuTypeApi().actionToMenuType(it,"Delete","",storeId,Callback())
        }

        val nextPageClick:(Int)->Unit = {
            val mIntent = Intent()
            mIntent.putExtra(StoreMenuDetailActivity.POSITION,it)
            mIntent.putExtra(StoreMenuDetailActivity.TYPENAME,menuList?.getOrNull(it)?.name)
            mIntent.putExtra(StoreMenuDetailActivity.USERSTOREID,storeId)
            mIntent.putExtra(StoreMenuDetailActivity.USERRESPONSE,intent.getStringExtra(USERRESPONSE))
            mIntent.setClass(this,StoreMenuDetailActivity::class.java)
            startActivity(mIntent)
        }

        storeMenuEditAdapter = StoreMenuEditAdapter(this,removeData,nextPageClick)
        storeMenuEditRv.layoutManager = LinearLayoutManager(this)
        storeMenuEditRv.adapter = storeMenuEditAdapter

        storeMenuEditMenu.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        storeMenuEditAdd.setOnClickListener {
            if (storeMenuEditText.text.toString().isNotEmpty()) {
                flag = 1
                progressBar8.visibility = View.VISIBLE
                ActionToMenuTypeApi().actionToMenuType("","Add",storeMenuEditText.text.toString(),storeId,Callback())
            }
        }

        navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,intent.getStringExtra(USERRESPONSE)?:"",this).StoreMenu())

    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                e.printStackTrace()
                progressBar8.visibility = View.INVISIBLE
                android.app.AlertDialog.Builder(this@StoreMenuEditActivity).setTitle("錯誤訊息")
                    .setMessage("無法正常執行，請稍後再試！")
                    .setPositiveButton("確定") { dialog, which ->
                        dialog.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {

            val result = response.body()!!.string()
            Log.d("menu-action",result)
            val listType = object : TypeToken<OrderMenuModel>() {}.type
            val orderMenuModel: OrderMenuModel = Gson().fromJson(result.toString(), listType)

            if (orderMenuModel.success == true && !orderMenuModel.result.isNullOrEmpty()) {
                menuList = orderMenuModel.result
//                var menuTypeList = mutableListOf<String>()
//                for (i in 0 until menuList?.size!!) {
//                    menuTypeList.add(menuList?.getOrNull(i)?.name?:"")
//                }
                runOnUiThread {
                    progressBar8.visibility = View.INVISIBLE
                    storeMenuEditAdapter?.updateData(menuList!!)
                    if (flag ==1 ) {
                        Toast.makeText(this@StoreMenuEditActivity,"新增成功！",Toast.LENGTH_SHORT).show()
                        storeMenuEditText.text.clear()
                        flag = 0
                    }
                }
            } else {
                runOnUiThread {
                    progressBar8.visibility = View.INVISIBLE
                }
            }
        }
    }
}