package tw.com.seventeenbrunch

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.os.PowerManager
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.io.IOException
import java.net.URL
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T



class MyFirebaseMessagingService : FirebaseMessagingService() {

    private var TAG = "MyFirebaseMsgService"

    override fun onNewToken(p0: String) {
        Log.e("NEW_TOKEN", p0)
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        if (remoteMessage != null) {
            super.onMessageReceived(remoteMessage)
        }
        Log.d("firebase", "From: " + remoteMessage!!.from!!)
        Log.d("firebase", "Notification Message Body: " + remoteMessage.notification!!.body!!)

        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG, "收到推送 Message titleArray payload: " + remoteMessage.data)
        }

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            Log.d(TAG, "收到通知 Message Notification Body: " + remoteMessage.notification!!.body!!)
        }

        val notification = remoteMessage.notification
        val data = remoteMessage.data
        sendNotification(notification!!, data)
        sendNotification(remoteMessage.notification!!.title, remoteMessage.notification!!.body, remoteMessage.data)
        sendMessage()
    }

    private fun sendMessage() {
        val intent = Intent("custom-event-name")
        // You can also include some extra data.
        intent.putExtra("message", "This is my message!")
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
        Log.d("sender", "Broadcasting message")
    }
    /**
     * Create and show a custom notification containing the received FCM message.
     *
     * @param notification FCM notification payload received.
     * @param data FCM titleArray payload received.
     */

    private fun sendNotification(title: String?, messageBody: String?, data: Map<String, String>) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val data_title = data["title"]

        if (title != null)
            intent.putExtra("title", data_title)

        intent.putExtra("messageId", data["messageId"])
        intent.putExtra("message", data["message"])
        val data_activity = data["action"]

        if (data_activity != null) {
            val action_data = data["action_data"]
            intent.putExtra("action", data_activity)
            intent.putExtra("action_data", action_data)
        }

        val pendingIntent = PendingIntent.getActivity(this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT)
        val largeIcon = BitmapFactory.decodeResource(
            resources, R.mipmap.ic_launcher)
        //resources, R.drawable.fcm)

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this)
            .setSmallIcon(R.drawable.tmp_logo)
//            .setSmallIcon(R.drawable.logo)
            .setLargeIcon(largeIcon)
            .setContentTitle(title)
            .setContentText(messageBody)
            .setPriority(Notification.PRIORITY_MAX)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

    @SuppressLint("InvalidWakeLockTag")
    private fun sendNotification(notification: RemoteMessage.Notification, data: Map<String, String>) {
        val icon = BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher)
        //val icon = BitmapFactory.decodeResource(resources, R.drawable.fcm)

        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val notificationBuilder = NotificationCompat.Builder(this, "channel_id")
            .setContentTitle(notification.title)
            .setContentText(notification.body)
            .setAutoCancel(true)
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setContentIntent(pendingIntent)
            .setContentInfo(notification.title)
            .setPriority(Notification.PRIORITY_MAX)
            .setLargeIcon(icon)
            .setColor(Color.BLACK)
            .setLights(Color.BLACK, 1000, 300)
            .setDefaults(Notification.DEFAULT_VIBRATE)
            .setSmallIcon(R.drawable.tmp_logo)
//            .setSmallIcon(R.drawable.logo)

        try {
            val picture_url = data["picture_url"]

            if (picture_url != null && "" != picture_url) {
                val url = URL(picture_url)
                val bigPicture = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                notificationBuilder.setStyle(
                    NotificationCompat.BigPictureStyle().bigPicture(bigPicture).setSummaryText(notification.body)
                )
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Notification Channel is required for Android O and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                "channel_id", "channel_name", NotificationManager.IMPORTANCE_HIGH
            )

            channel.description = "channel description"
            channel.setShowBadge(true)
            channel.canShowBadge()
            channel.enableLights(true)
            channel.lightColor = Color.RED
            channel.enableVibration(true)
            channel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0, notificationBuilder.build())

        val pm = getSystemService(Context.POWER_SERVICE) as PowerManager
        val wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK or PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG")
        wl.acquire(15000)
    }
}