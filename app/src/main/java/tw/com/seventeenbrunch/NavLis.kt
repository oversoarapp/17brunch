package tw.com.seventeenbrunch

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import okhttp3.Callback

class NavLis(drawer:DrawerLayout,mResponse:String,activity:Activity) {

    private var activity = activity
    private var drawer = drawer
    private var mResponse = mResponse
//    private var acc = acc
//    private var progressBar = progress
//    private var callback = callback

    inner class CustomerMenu :NavigationView.OnNavigationItemSelectedListener{
        override fun onNavigationItemSelected(item: MenuItem): Boolean {
            val mIntent = Intent()
            when (item.title) {
                "會員首頁" -> {
                    return if (activity.localClassName != "CustomerActivity") {
                        drawer.closeDrawers()
                        mIntent.putExtra(CustomerActivity.USERRESPONSE, mResponse)
                        mIntent.setClass(activity, CustomerActivity::class.java)
                        activity.startActivity(mIntent)
                        activity.finish()
                        true
                    } else
                        false
                }

                "會員資訊設定" -> {
                    return if (activity.localClassName != "CustomerInfoActivity") {
                        drawer.closeDrawers()
                        mIntent.putExtra(CustomerInfoActivity.RESPONSE, mResponse)
                        mIntent.setClass(activity, CustomerInfoActivity::class.java)
                        activity.startActivity(mIntent)
                        activity.finish()
                        true
                    } else
                        false
                }

                "會員儲值一覽" -> {
                    return if (activity.localClassName != "TopUpRecordActivity") {
                        drawer.closeDrawers()
                        mIntent.putExtra(TopUpRecordActivity.RESPONSE, mResponse)
                        mIntent.setClass(activity, TopUpRecordActivity::class.java)
                        activity.startActivity(mIntent)
                        activity.finish()
                        true
                    } else
                        false
                }

                "訂餐紀錄" -> {
                    return if (activity.localClassName != "CustomerTradeRecordActivity") {
                        drawer.closeDrawers()
                        mIntent.putExtra(CustomerTradeRecordActivity.USERRESPONSE, mResponse)
                        mIntent.setClass(activity, CustomerTradeRecordActivity::class.java)
                        activity.startActivity(mIntent)
                        activity.finish()
                        true
                    } else
                        false
                }

                "修改密碼" -> {
                    return if (activity.localClassName != "CustomerEditPwdActivity") {
                        drawer.closeDrawers()
                        mIntent.putExtra(CustomerEditPwdActivity.USERRESPONSE, mResponse)
                        mIntent.setClass(activity, CustomerEditPwdActivity::class.java)
                        activity.startActivity(mIntent)
                        activity.finish()
                        true
                    } else
                        false
                }

                "登出" -> {
                    drawer.closeDrawers()
                    val sp = activity.getSharedPreferences("userData", Context.MODE_PRIVATE)
                    sp?.edit()?.clear()?.apply()
                    activity.startActivity(Intent(activity, MainActivity::class.java))
                    activity.finish()
//                    progressBar.visibility = View.VISIBLE
//                    LogoutApi().logout(acc, "0", callback)
                    return true
                }
                else -> return false
            }
        }
    }
    inner class StoreMenu:NavigationView.OnNavigationItemSelectedListener{
        override fun onNavigationItemSelected(item: MenuItem): Boolean {
            val mIntent = Intent()
            when (item.title) {
                "店家資訊設定" -> {
                    return if (activity.localClassName != "StoreActivity") {
                        drawer.closeDrawers()
                        mIntent.setClass(activity, StoreActivity::class.java)
                        mIntent.putExtra(StoreActivity.USERRESPONSE, mResponse)
                        activity.startActivity(mIntent)
                        activity.finish()
                        true
                    } else
                        false
                }
                "會員儲值設定" -> {
                    return if (activity.localClassName != "StoreTopUpActivity") {
                    drawer.closeDrawers()
                    mIntent.setClass(activity, StoreTopUpActivity::class.java)
                    mIntent.putExtra(StoreTopUpActivity.USERRESPONSE, mResponse)
                    activity.startActivity(mIntent)
                    activity.finish()
                        true
                    } else
                        false
                }

                "會員資訊一覽" -> {
                    return if (activity.localClassName != "StoreMemberListActivity") {
                        drawer.closeDrawers()
                        mIntent.setClass(activity, StoreMemberListActivity::class.java)
                        mIntent.putExtra(StoreMemberListActivity.FROM, "store")
                        mIntent.putExtra(StoreMemberListActivity.USERRESPONSE, mResponse)
                        activity.startActivity(mIntent)
                        activity.finish()
                        true
                    } else
                        false
                }

                "店家儲值一覽" -> {
                    return if (activity.localClassName != "TopUpRecordActivity") {
                        drawer.closeDrawers()
                        mIntent.setClass(activity, TopUpRecordActivity::class.java)
                        mIntent.putExtra(TopUpRecordActivity.RESPONSE, mResponse)
                        activity.startActivity(mIntent)
                        activity.finish()
                        true
                    } else
                        false
                }

                "折扣金設定" -> {
                    return if (activity.localClassName != "StoreCouponActivity") {
                        drawer.closeDrawers()
                        mIntent.setClass(activity, StoreCouponActivity::class.java)
                        mIntent.putExtra(StoreCouponActivity.USERRESPONSE, mResponse)
                        activity.startActivity(mIntent)
                        activity.finish()
                        true
                    } else
                        false
                }

                "修改密碼" -> {
                    return if (activity.localClassName != "StoreEditPwdActivity") {
                        drawer.closeDrawers()
                        mIntent.setClass(activity, StoreEditPwdActivity::class.java)
                        mIntent.putExtra(StoreEditPwdActivity.USERRESPONSE, mResponse)
                        activity.startActivity(mIntent)
                        activity.finish()
                        true
                    } else
                        false
                }

                "菜單修改" -> {
                    return if (activity.localClassName != "StoreMenuEditActivity") {
                        drawer.closeDrawers()
                        mIntent.putExtra(StoreMenuEditActivity.USERRESPONSE, mResponse)
                        mIntent.setClass(activity, StoreMenuEditActivity::class.java)
                        activity.startActivity(mIntent)
                        activity.finish()
                        true
                    } else
                        false
                }

                "訂餐紀錄" -> {
                    return if (activity.localClassName != "StoreTradeRecordActivity") {
                        drawer.closeDrawers()
                        mIntent.setClass(activity, StoreTradeRecordActivity::class.java)
                        mIntent.putExtra(StoreTradeRecordActivity.USERRESPONSE, mResponse)
                        activity.startActivity(mIntent)
                        activity.finish()
                        true
                    } else
                        false
                }

                "登出" -> {
//                    progressBar.visibility = View.VISIBLE
//                    LogoutApi().logout(acc,"1",callback)
                    val sp = activity.getSharedPreferences("userData", Context.MODE_PRIVATE)
                    sp?.edit()?.clear()?.apply()
                    activity.startActivity(Intent(activity, MainActivity::class.java))
                    activity.finish()
                    return true
                }

                "標籤機設定" -> {
                    return if (activity.localClassName != "PrintActivity") {
                        drawer.closeDrawers()
                        mIntent.setClass(activity, PrintActivity::class.java)
                        mIntent.putExtra(PrintActivity.USERRESPONSE, mResponse)
                        activity.startActivity(mIntent)
                        activity.finish()
                        true
                    } else
                        false
                }

                "統計報表" -> {
                    return if (activity.localClassName != "StoreReportActivity") {
                        drawer.closeDrawers()
                        mIntent.setClass(activity, StoreReportActivity::class.java)
                        mIntent.putExtra(StoreReportActivity.RESPONSE, mResponse)
                        activity.startActivity(mIntent)
                        activity.finish()
                        true
                    } else
                        false
                }
                else -> return false
            }
        }
    }
}