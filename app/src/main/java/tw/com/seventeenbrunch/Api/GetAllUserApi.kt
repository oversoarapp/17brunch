package tw.com.seventeenbrunch.Api

import tw.com.seventeenbrunch.seventeenbrunch.getAllStoreApiUrl
import okhttp3.*
import tw.com.seventeenbrunch.seventeenbrunch.getAllUserApiUrl
import java.util.concurrent.TimeUnit

class GetAllUserApi {

    val getAllUserClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS).build()

    fun getAllUser (storeId:String,callback: Callback){

        val body = FormBody.Builder()
            .add("storeId",storeId)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(getAllUserApiUrl)
            .build()

        val call: Call = getAllUserClient.newCall(request)

        call.enqueue(callback)
    }

}