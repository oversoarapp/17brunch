package tw.com.seventeenbrunch.Api

import tw.com.seventeenbrunch.seventeenbrunch.editStoreApiUrl
import okhttp3.*
import java.util.concurrent.TimeUnit

class EditStoreApi {

    val editStoreClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS).build()

    fun editStore (storeId: String,name: String,address: String,phone: String,businessHours:String,status:String,callback: Callback){

        val body = FormBody.Builder()
            .add("storeId",storeId)
            .add("name",name)
            .add("address",address)
            .add("phone",phone)
            .add("businessHours",businessHours)
            .add("status",status)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(editStoreApiUrl)
            .build()

        val call: Call = editStoreClient.newCall(request)

        call.enqueue(callback)
    }

}