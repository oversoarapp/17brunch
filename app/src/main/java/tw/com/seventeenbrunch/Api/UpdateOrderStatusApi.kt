package tw.com.seventeenbrunch.Api

import okhttp3.*
import tw.com.seventeenbrunch.seventeenbrunch.getMenuApiUrl
import tw.com.seventeenbrunch.seventeenbrunch.updateOrderStatusApiUrl
import java.util.concurrent.TimeUnit

class UpdateOrderStatusApi {

    val updateOrderStatusClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS).build()

    fun updateOrderStatus (tradeNo: String,callback: Callback){

        val body = FormBody.Builder()
            .add("tradeNo",tradeNo)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(updateOrderStatusApiUrl)
            .build()

        val call: Call = updateOrderStatusClient.newCall(request)

        call.enqueue(callback)
    }
}