package tw.com.seventeenbrunch.Api

import tw.com.seventeenbrunch.seventeenbrunch.signUpApiUrl
import okhttp3.*
import java.util.concurrent.TimeUnit

class SignUpApi {

  val singUpClient: OkHttpClient = OkHttpClient().newBuilder()
    .connectTimeout(15, TimeUnit.SECONDS)
    .writeTimeout(10, TimeUnit.SECONDS)
    .readTimeout(15, TimeUnit.SECONDS).build()

  fun signUp (name: String,account: String,password: String,email: String,address: String,identity: String,storeName: String,storeBh:String,storeAds:String,storePhone:String ,token:String,callback: Callback){
//    fun signUp (name: String,account: String,password: String,email: String,address: String,identity: String,callback: Callback){

    val body = FormBody.Builder()
      .add("identity",identity)
      .add("name",name)
      .add("account",account)
      .add("password",password)
      .add("address",address)
      .add("email",email)
      .add("storeName",storeName)
      .add("storePhone",storePhone)
      .add("storeBh",storeBh)
      .add("storeAds",storeAds)
      .add("token",token)
      .build()

    val request = Request.Builder()
      .post(body)
      .url(signUpApiUrl)
      .build()

    val call: Call = singUpClient.newCall(request)

    call.enqueue(callback)
  }

}