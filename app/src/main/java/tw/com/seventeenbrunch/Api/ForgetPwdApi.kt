package tw.com.seventeenbrunch.Api

import tw.com.seventeenbrunch.seventeenbrunch.forgetPwdApiUrl
import okhttp3.*
import java.util.concurrent.TimeUnit

class ForgetPwdApi {

    val forgetPwdClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS).build()

    fun forgetPwd (account: String,identity: String,email: String,callback: Callback) {

        val body = FormBody.Builder()
            .add("account",account)
            .add("email",email)
            .add("identity",identity)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(forgetPwdApiUrl)
            .build()

        val call: Call = forgetPwdClient.newCall(request)

        call.enqueue(callback)

    }

}