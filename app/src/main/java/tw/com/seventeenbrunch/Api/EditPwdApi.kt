package tw.com.seventeenbrunch.Api

import tw.com.seventeenbrunch.seventeenbrunch.editPwdApiUrl
import okhttp3.*
import java.util.concurrent.TimeUnit

class EditPwdApi {

    val editPwdClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS).build()

    fun editPwd (account: String,identity: String,newPwd: String,callback: Callback) {

        val body = FormBody.Builder()
            .add("account",account)
            .add("identity",identity)
            .add("newPwd",newPwd)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(editPwdApiUrl)
            .build()

        val call: Call = editPwdClient.newCall(request)

        call.enqueue(callback)

    }

}