package tw.com.seventeenbrunch.Api

import tw.com.seventeenbrunch.seventeenbrunch.getAllStoreApiUrl
import okhttp3.*
import java.util.concurrent.TimeUnit

class GetAllStoreApi {

    val getAllStoreClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS).build()

    fun getAllStore (callback: Callback){

        val request = Request.Builder()
            .get()
            .url(getAllStoreApiUrl)
            .build()

        val call: Call = getAllStoreClient.newCall(request)

        call.enqueue(callback)
    }

}