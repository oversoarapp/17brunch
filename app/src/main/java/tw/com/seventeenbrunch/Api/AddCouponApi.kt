package tw.com.seventeenbrunch.Api

import okhttp3.*
import tw.com.seventeenbrunch.seventeenbrunch.addCouponApiUrl
import java.util.concurrent.TimeUnit

class AddCouponApi {

  val addCouponClient: OkHttpClient = OkHttpClient().newBuilder()
    .connectTimeout(15, TimeUnit.SECONDS)
    .writeTimeout(10, TimeUnit.SECONDS)
    .readTimeout(15, TimeUnit.SECONDS).build()

  fun addCoupon (account: String,couponName: String,menuType: String,storeId:String,limited:String,discount:String,callback: Callback){

    val body = FormBody.Builder()
      .add("account",account)
      .add("name",couponName)
      .add("menuType",menuType)
      .add("storeId",storeId)
      .add("limited",limited)
      .add("discount",discount)
      .build()

    val request = Request.Builder()
      .post(body)
      .url(addCouponApiUrl)
      .build()

    val call: Call = addCouponClient.newCall(request)

    call.enqueue(callback)
  }
}