package tw.com.seventeenbrunch.Api

import okhttp3.*
import tw.com.seventeenbrunch.seventeenbrunch.editInfoApiUrl
import java.util.concurrent.TimeUnit

class EditInfoApi {

    val editInfoClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS)
        .build()

    fun editInfo (account:String,name:String,email:String,ads:String,store:String,identity:String,callback: Callback) {

        val body = FormBody.Builder()
            .add("account",account)
            .add("name",name)
            .add("email",email)
            .add("address",ads)
            .add("store",store)
            .add("identity",identity)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(editInfoApiUrl)
            .build()

        val call: Call = editInfoClient.newCall(request)

        call.enqueue(callback)

    }
}