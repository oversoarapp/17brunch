package tw.com.seventeenbrunch.Api

import tw.com.seventeenbrunch.seventeenbrunch.loginApiUrl
import okhttp3.*
import java.util.concurrent.TimeUnit

class LoginApi {

  val loginClient: OkHttpClient = OkHttpClient().newBuilder()
    .connectTimeout(15, TimeUnit.SECONDS)
    .writeTimeout(10, TimeUnit.SECONDS)
    .readTimeout(15, TimeUnit.SECONDS).build()

  fun login (account: String,password: String,identity: String,token: String,callback: Callback) {

    val body = FormBody.Builder()
      .add("account",account)
      .add("password",password)
      .add("identity",identity)
      .add("token",token)
      .build()

    val request = Request.Builder()
      .post(body)
      .url(loginApiUrl)
      .build()

    val call: Call = loginClient.newCall(request)

    call.enqueue(callback)

  }
}