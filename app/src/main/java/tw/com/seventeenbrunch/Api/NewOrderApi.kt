package tw.com.seventeenbrunch.Api

import tw.com.seventeenbrunch.seventeenbrunch.loginApiUrl
import okhttp3.*
import tw.com.seventeenbrunch.seventeenbrunch.newOrderApiUrl
import java.util.concurrent.TimeUnit

class NewOrderApi {

  val newOrderClient: OkHttpClient = OkHttpClient().newBuilder()
    .connectTimeout(15, TimeUnit.SECONDS)
    .writeTimeout(10, TimeUnit.SECONDS)
    .readTimeout(15, TimeUnit.SECONDS).build()

  fun newOrder (account: String,storeId: String,orderItem: String,total: String,discount:String,callback: Callback) {

    val body = FormBody.Builder()
      .add("account",account)
      .add("storeId",storeId)
      .add("orderItem",orderItem)
      .add("tradeTotal",total)
      .add("discount",discount)
      .build()

    val request = Request.Builder()
      .post(body)
      .url(newOrderApiUrl)
      .build()

    val call: Call = newOrderClient.newCall(request)

    call.enqueue(callback)

  }
}