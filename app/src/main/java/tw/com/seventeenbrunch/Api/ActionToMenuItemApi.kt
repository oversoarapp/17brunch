package tw.com.seventeenbrunch.Api

import tw.com.seventeenbrunch.seventeenbrunch.actionToMenuItemApiUrl
import okhttp3.*
import java.util.concurrent.TimeUnit

class ActionToMenuItemApi {

    val actionToMenuItemClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS).build()

    fun actionToMenuItem (itemId:String,itemName: String,action: String,menuType: String,price:String,storeId:String,newName:String,callback: Callback){

        val body = FormBody.Builder()
            .add("itemId",itemId)
            .add("itemName",itemName)
            .add("action",action)
            .add("menuType",menuType)
            .add("price",price)
            .add("storeId",storeId)
            .add("newName",newName)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(actionToMenuItemApiUrl)
            .build()

        val call: Call = actionToMenuItemClient.newCall(request)

        call.enqueue(callback)
    }

}