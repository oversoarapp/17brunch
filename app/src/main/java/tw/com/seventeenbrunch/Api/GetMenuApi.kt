package tw.com.seventeenbrunch.Api

import okhttp3.*
import tw.com.seventeenbrunch.seventeenbrunch.getMenuApiUrl
import java.util.concurrent.TimeUnit

class GetMenuApi {

    val getMenuClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS).build()

    fun getMenu (storeId:String,callback: Callback){

        val body = FormBody.Builder()
            .add("storeId",storeId)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(getMenuApiUrl)
            .build()

        val call: Call = getMenuClient.newCall(request)

        call.enqueue(callback)
    }

}