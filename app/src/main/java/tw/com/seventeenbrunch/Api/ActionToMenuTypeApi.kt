package tw.com.seventeenbrunch.Api

import tw.com.seventeenbrunch.seventeenbrunch.actionToMenuTypeApiUrl
import okhttp3.*
import java.util.concurrent.TimeUnit

class ActionToMenuTypeApi {

    val actionToMenuTypeClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS).build()

    fun actionToMenuType (menuType: String,action: String,name: String,storeId:String,callback: Callback){

        val body = FormBody.Builder()
            .add("menuType",menuType)
            .add("action",action)
            .add("name",name)
            .add("storeId",storeId)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(actionToMenuTypeApiUrl)
            .build()

        val call: Call = actionToMenuTypeClient.newCall(request)

        call.enqueue(callback)
    }

}