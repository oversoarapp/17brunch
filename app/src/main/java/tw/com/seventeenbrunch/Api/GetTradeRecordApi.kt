package tw.com.seventeenbrunch.Api

import okhttp3.*
import tw.com.seventeenbrunch.seventeenbrunch.getMenuApiUrl
import tw.com.seventeenbrunch.seventeenbrunch.getTradeRecordApiUrl
import java.util.concurrent.TimeUnit

class GetTradeRecordApi {

    val getTradeRecordClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS).build()

    fun getTradeRecord (account:String,identity:String,type:String,callback: Callback){

        val body = FormBody.Builder()
            .add("user",account)
            .add("identity",identity)
            .add("type",type)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(getTradeRecordApiUrl)
            .build()

        val call: Call = getTradeRecordClient.newCall(request)

        call.enqueue(callback)
    }

}