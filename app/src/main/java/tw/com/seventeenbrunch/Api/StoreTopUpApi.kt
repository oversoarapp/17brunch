package tw.com.seventeenbrunch.Api

import tw.com.seventeenbrunch.seventeenbrunch.storeTopUpApiUrl
import okhttp3.*
import java.util.concurrent.TimeUnit

class StoreTopUpApi {

  val storeTopUpClient: OkHttpClient = OkHttpClient().newBuilder()
    .connectTimeout(15, TimeUnit.SECONDS)
    .writeTimeout(10, TimeUnit.SECONDS)
    .readTimeout(15, TimeUnit.SECONDS).build()

  fun storeTopUp (userAcc: String,expiryDate:String,points: String,storeId: String,storeName: String,callback: Callback){

    val body = FormBody.Builder()
      .add("userAcc",userAcc)
      .add("storeId",storeId)
      .add("storeName",storeName)
      .add("points",points)
      .add("expiryDate",expiryDate)
      .build()

    val request = Request.Builder()
      .post(body)
      .url(storeTopUpApiUrl)
      .build()

    val call: Call = storeTopUpClient.newCall(request)

    call.enqueue(callback)
  }

}