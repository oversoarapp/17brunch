package tw.com.seventeenbrunch.Api

import tw.com.seventeenbrunch.seventeenbrunch.addStoreApiUrl
import okhttp3.*
import java.util.concurrent.TimeUnit

class AddStoreApi {

    val addStoreClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS).build()

    fun addStore (name: String,address: String,phone: String,businessHours:String,callback: Callback){

        val body = FormBody.Builder()
            .add("name",name)
            .add("address",address)
            .add("phone",phone)
            .add("businessHours",businessHours)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(addStoreApiUrl)
            .build()

        val call: Call = addStoreClient.newCall(request)

        call.enqueue(callback)
    }

}