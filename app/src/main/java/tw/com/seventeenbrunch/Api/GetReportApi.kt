package tw.com.seventeenbrunch.Api

import okhttp3.*
import tw.com.seventeenbrunch.seventeenbrunch.getReportApiUrl
import java.util.concurrent.TimeUnit

class GetReportApi {

  val getReportClient: OkHttpClient = OkHttpClient().newBuilder()
    .connectTimeout(15, TimeUnit.SECONDS)
    .writeTimeout(10, TimeUnit.SECONDS)
    .readTimeout(15, TimeUnit.SECONDS).build()

  fun getReport(storeId:String,startDate:String,endDate:String,callback: Callback) {

    val body = FormBody.Builder()
      .add("storeId",storeId)
      .add("startDate",startDate)
      .add("endDate",endDate)
      .build()

    val request = Request.Builder()
      .post(body)
      .url(getReportApiUrl)
      .build()

    val call: Call = getReportClient.newCall(request)

    call.enqueue(callback)
  }
}