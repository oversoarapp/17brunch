package tw.com.seventeenbrunch.Api

import tw.com.seventeenbrunch.seventeenbrunch.customerTopUpApiUrl
import okhttp3.*
import tw.com.seventeenbrunch.seventeenbrunch.couponTopUpApiUrl
import java.util.concurrent.TimeUnit

class CouponTopUpApi {

  val couponTopUpClient: OkHttpClient = OkHttpClient().newBuilder()
    .connectTimeout(15, TimeUnit.SECONDS)
    .writeTimeout(10, TimeUnit.SECONDS)
    .readTimeout(15, TimeUnit.SECONDS).build()

  fun couponTopUp (userAcc: String,couponName:String,menuType:String,topUpAcc: String,points: String,storeId: String,callback: Callback){

    val body = FormBody.Builder()
      .add("userAcc",userAcc)
      .add("topUpAcc",topUpAcc)
      .add("storeId",storeId)
      .add("points",points)
      .add("menuType",menuType)
      .add("couponName",couponName)
      .build()

    val request = Request.Builder()
      .post(body)
      .url(couponTopUpApiUrl)
      .build()

    val call: Call = couponTopUpClient.newCall(request)

    call.enqueue(callback)
  }

}