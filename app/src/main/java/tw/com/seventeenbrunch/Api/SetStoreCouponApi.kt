package tw.com.seventeenbrunch.Api

import okhttp3.*
import tw.com.seventeenbrunch.seventeenbrunch.setStoreCouponApiUrl
import java.util.concurrent.TimeUnit

class SetStoreCouponApi {

    val setStoreCouponClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS).build()

    fun setStoreCoupon(storeId:String,limited:String,discount:String,callback: Callback) {

        val body = FormBody.Builder()
            .add("storeId",storeId)
            .add("limited",limited)
            .add("discount",discount)
            .build()

        val request = Request.Builder()
            .url(setStoreCouponApiUrl)
            .post(body)
            .build()

        val call:Call = setStoreCouponClient.newCall(request)

        call.enqueue(callback)
    }
}