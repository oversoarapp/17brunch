package tw.com.seventeenbrunch.Api

import okhttp3.*
import tw.com.seventeenbrunch.seventeenbrunch.getMenuApiUrl
import tw.com.seventeenbrunch.seventeenbrunch.getTradeDetailRecordApiUrl
import tw.com.seventeenbrunch.seventeenbrunch.getTradeRecordApiUrl
import java.util.concurrent.TimeUnit

class GetTradeDetailRecordApi {

    val getTradeDetailRecordClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS).build()

    fun getTradeDetailRecord (tradeNo:String,callback: Callback){

        val body = FormBody.Builder()
            .add("tradeNo",tradeNo)
            .build()

        val request = Request.Builder()
            .post(body)
            .url(getTradeDetailRecordApiUrl)
            .build()

        val call: Call = getTradeDetailRecordClient.newCall(request)

        call.enqueue(callback)
    }

}