package tw.com.seventeenbrunch.Api

import tw.com.seventeenbrunch.seventeenbrunch.customerTopUpApiUrl
import okhttp3.*
import java.util.concurrent.TimeUnit

class CustomerTopUpApi {

  val customerTopUpClient: OkHttpClient = OkHttpClient().newBuilder()
    .connectTimeout(15, TimeUnit.SECONDS)
    .writeTimeout(10, TimeUnit.SECONDS)
    .readTimeout(15, TimeUnit.SECONDS).build()

  fun customerTopUp (userAcc: String,topUpAcc: String,points: String,storeId: String,callback: Callback){

    val body = FormBody.Builder()
      .add("userAcc",userAcc)
      .add("topUpAcc",topUpAcc)
      .add("storeId",storeId)
      .add("points",points)
      .build()

    val request = Request.Builder()
      .post(body)
      .url(customerTopUpApiUrl)
      .build()

    val call: Call = customerTopUpClient.newCall(request)

    call.enqueue(callback)
  }

}