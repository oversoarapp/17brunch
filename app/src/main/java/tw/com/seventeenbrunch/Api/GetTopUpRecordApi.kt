package tw.com.seventeenbrunch.Api

import kotlinx.android.synthetic.main.print_test.view.*
import okhttp3.*
import tw.com.seventeenbrunch.seventeenbrunch.getTopUpRecordApiUrl
import java.util.concurrent.TimeUnit

class GetTopUpRecordApi {

    val getTopUpRecordClient: OkHttpClient = OkHttpClient().newBuilder()
        .connectTimeout(15, TimeUnit.SECONDS)
        .writeTimeout(10, TimeUnit.SECONDS)
        .readTimeout(15, TimeUnit.SECONDS).build()

    fun getTopUpRecord (account:String,storeId:String,identity:String,callback: Callback) {

        val body = FormBody.Builder()
            .add("account",account)
            .add("storeId",storeId)
            .add("identity",identity)
            .build()

        val requestBody = Request.Builder()
            .post(body)
            .url(getTopUpRecordApiUrl)
            .build()

        val call: Call = getTopUpRecordClient.newCall(requestBody)

        call.enqueue(callback)
    }
}