package tw.com.seventeenbrunch

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.Call
import okhttp3.Response
import org.json.JSONObject
import tw.com.seventeenbrunch.Api.LoginApi
import java.io.IOException
import android.content.Context.MODE_PRIVATE
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.include_white.*
import org.json.JSONException
import tw.com.seventeenbrunch.Model.UserInfoModel


class MainActivity : AppCompatActivity() {

    private var sp:SharedPreferences? = null
    private var whichUser = ""
    private var token = ""
    private var isLogin = false
    private var flag = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.include_white)

        getToken()
        sp = getSharedPreferences("userData", Context.MODE_PRIVATE)
        val acc = sp?.getString("acc","")?:""
        if (acc.isNotEmpty()) {
            isLogin = true
            val pwd = sp?.getString("pwd","")?:""
            whichUser = sp?.getString("identity","")?:""
            progressBar13.visibility = View.VISIBLE
            LoginApi().login(acc,pwd,whichUser,token,Callback())
        } else {
            setContentView(R.layout.activity_main)
            flag = 1
            initView()
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("是否離開APP？")
                .setNegativeButton("確定") { dialog, which ->
                    finish()
                }
                .setPositiveButton("取消") { dialog, which ->
                    dialog.cancel()
                }
                .show()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun getToken() {

        FirebaseMessaging.getInstance().subscribeToTopic("global")
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(
            this@MainActivity,
            OnSuccessListener<InstanceIdResult> { instanceIdResult ->
                token = instanceIdResult.token
                Log.i("FCMToken", token)
            })
        FirebaseMessaging.getInstance().isAutoInitEnabled = true

    }

    private fun initView() {

        radioGroup2.setOnCheckedChangeListener { group, checkedId ->
            when (group.checkedRadioButtonId) {
                R.id.customer -> {
                    whichUser = "0"
                }
                R.id.store -> {
                    whichUser = "1"
                }
//                R.id.company -> {
//                    whichUser = "2"
//                }
            }
        }

        mainSignUp.setOnClickListener {
            if (whichUser.isNotEmpty()) {
//                if (whichUser != "2") {
                val intent = Intent(this, SignUpActivity::class.java)
                intent.putExtra(SignUpActivity.TOKEN,token)
                intent.putExtra(SignUpActivity.IDENTITY,whichUser)
                startActivity(intent)
//                } else {
//                    Toast.makeText(this,"總部人員帳號已設定！",Toast.LENGTH_LONG).show()
//                }
            } else {
                Toast.makeText(this,"請選擇您的身分！",Toast.LENGTH_LONG).show()
            }
        }

        mainLogin.setOnClickListener {
            val acc = mainAccount.text.toString()
            val pwd = mainPwd.text.toString()
            if (acc.isNotEmpty() && pwd.isNotEmpty() && whichUser.isNotEmpty() ) {
                progressBar.visibility = View.VISIBLE
                LoginApi().login(acc,pwd,whichUser,token,Callback())
            } else {
                Toast.makeText(this,"欄位不可空白！", Toast.LENGTH_SHORT).show()
            }
        }

        mainForgetPwd.setOnClickListener {
            startActivity(Intent(this, ForgetPwdActivity::class.java))
        }

    }

    inner class Callback : okhttp3.Callback {

        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                if (isLogin) {
                    progressBar13.visibility = View.INVISIBLE
                } else {
                    progressBar.visibility = View.INVISIBLE
                }
                e.printStackTrace()
                android.app.AlertDialog.Builder(this@MainActivity).setTitle("錯誤訊息")
                    .setCancelable(false)
                    .setMessage("無法正常執行，請稍後再試！")
                    .setPositiveButton("確定") { dialog, which ->
                        dialog.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {

            runOnUiThread {
                if (isLogin) {
                    progressBar13.visibility = View.INVISIBLE
                } else {
                    progressBar.visibility = View.INVISIBLE
                }
            }
            try {
                val mResponse = response.body()?.string()
                val listType = object : TypeToken<UserInfoModel>() {}.type
                val userInfoModel: UserInfoModel = Gson().fromJson(mResponse, listType)

                if(userInfoModel.success == true ) {
                    if (userInfoModel.result?.status?:"0" != "0") {
                        val edit = sp?.edit()
                        if (!isLogin) {
                            edit?.putString("acc", mainAccount.text.toString())
                            edit?.putString("pwd", mainPwd.text.toString())
                            edit?.putString("identity", whichUser)
                            edit?.apply()
                        }
                        edit?.putString("token", token)
                        edit?.apply()
                        val intent = Intent()
                        when (whichUser) {
                            "0" -> {
                                intent.setClass(this@MainActivity,CustomerActivity::class.java)
                                intent.putExtra(CustomerActivity.USERRESPONSE,mResponse)
                                runOnUiThread {
                                    startActivity(intent)
                                    finish()
                                }
                            }
                            "1" -> {
                                intent.setClass(this@MainActivity,StoreActivity::class.java)
                                intent.putExtra(StoreActivity.USERRESPONSE,mResponse)
                                runOnUiThread {
                                    startActivity(intent)
                                    finish()
                                }
                            }
                        }
                    } else {
                        runOnUiThread {
                            AlertDialog.Builder(this@MainActivity)
                                .setTitle("錯誤訊息")
                                .setMessage("您的帳號尚未開通！")
                                .setPositiveButton("確定") { dialog, which ->
                                    finish()
                                }
                                .show()
                        }
                    }
                } else {
                    if (sp?.getString("acc","")?.isNotEmpty() == true) {
                        sp?.edit()?.clear()?.apply()
                    }
                    val msg = userInfoModel.msg
                    runOnUiThread {
                        AlertDialog.Builder(this@MainActivity).setTitle("錯誤訊息")
                            .setMessage(msg)
                            .setPositiveButton("確定") { dialog, which ->
                                if (flag == 0 ) {
                                    finish()
                                } else {
                                    dialog.cancel()
                                }
                            }
                            .show()
                    }
                }
            } catch (e:JSONException) {
                e.printStackTrace()
            }
        }
    }
}


