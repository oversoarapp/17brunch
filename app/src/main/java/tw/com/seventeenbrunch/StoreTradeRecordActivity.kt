package tw.com.seventeenbrunch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_store_trade_record.*
import kotlinx.android.synthetic.main.activity_store_trade_record.progressBar
import okhttp3.Call
import okhttp3.Response
import org.json.JSONObject
import tw.com.seventeenbrunch.Adapter.StoreTradeRecordAdapter
import tw.com.seventeenbrunch.Api.GetTradeRecordApi
import tw.com.seventeenbrunch.Api.UpdateOrderStatusApi
import tw.com.seventeenbrunch.Model.TradeRecordDataModel
import tw.com.seventeenbrunch.Model.TradeRecordModel
import tw.com.seventeenbrunch.Model.UserInfoDataModel
import tw.com.seventeenbrunch.Model.UserInfoModel
import java.io.IOException

class StoreTradeRecordActivity : AppCompatActivity() {

    companion object {
        const val USERRESPONSE = "userResponse"
    }

    private var tradeRecordDataList: List<TradeRecordDataModel>? = null
    private var nAdapter: StoreTradeRecordAdapter? = null
    private var userInfoData: UserInfoDataModel? = null
    private var from = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_trade_record)

        initView()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("是否離開APP？")
                .setNegativeButton("確定") { dialog, which ->
                    finish()
                }
                .setPositiveButton("取消") { dialog, which ->
                    dialog.cancel()
                }
                .show()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun initView() {

        val response = intent.getStringExtra(USERRESPONSE)
        val listType = object : TypeToken<UserInfoModel>() {}.type
        val userInfoModel: UserInfoModel = Gson().fromJson(response, listType)
        userInfoData = userInfoModel.result

        storeTradeRecordTabLayout.addTab(storeTradeRecordTabLayout.newTab())
        storeTradeRecordTabLayout.addTab(storeTradeRecordTabLayout.newTab())
        storeTradeRecordTabLayout.getTabAt(0)?.text = "目前訂單"
        storeTradeRecordTabLayout.getTabAt(1)?.text = "過去訂單"

        GetTradeRecordApi().getTradeRecord(userInfoData?.store?.getOrNull(0)?.id?:"","1","0",Callback())

        val itemClick:(Int)->Unit = {
            val mIntent = Intent()
            mIntent.putExtra(STradeDetailRecordActivity.TRADENO,tradeRecordDataList?.getOrNull(it)?.trade_no)
            mIntent.putExtra(STradeDetailRecordActivity.TOTAL,tradeRecordDataList?.getOrNull(it)?.total)
            mIntent.putExtra(STradeDetailRecordActivity.RESPONSE,intent.getStringExtra(USERRESPONSE))
            mIntent.putExtra(STradeDetailRecordActivity.FROM,from)
            mIntent.setClass(this,STradeDetailRecordActivity::class.java)
            startActivity(mIntent)
        }
        val updateStatusClick:(Int)->Unit = { position ->
            progressBar.visibility = View.VISIBLE
            UpdateOrderStatusApi().updateOrderStatus(tradeRecordDataList?.getOrNull(position)?.trade_no?:"",UpdateStatusCallback())
        }

        nAdapter = StoreTradeRecordAdapter(this,itemClick,updateStatusClick)
        storeTradeRecordRv.layoutManager = LinearLayoutManager(this)
        storeTradeRecordRv.adapter = nAdapter

        storeTradeRecordTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
                progressBar.visibility = View.VISIBLE
                GetTradeRecordApi().getTradeRecord(userInfoData?.store?.getOrNull(0)?.id?:"","1",tab?.position.toString(),Callback())
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                from = tab?.text.toString()
                progressBar.visibility = View.VISIBLE
                GetTradeRecordApi().getTradeRecord(userInfoData?.store?.getOrNull(0)?.id?:"","1",tab?.position.toString(),Callback())
            }
        })

        storeTradeRecordMenu.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,response?:"",this).StoreMenu())

    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                progressBar.visibility = View.INVISIBLE
                androidx.appcompat.app.AlertDialog.Builder(this@StoreTradeRecordActivity)
                    .setTitle("訊息提示")
                    .setMessage("無法正常執行，請稍後再試！")
                    .setCancelable(false)
                    .setPositiveButton("確定") {d,w ->
                        d.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {

            val mResponse = response.body()?.string()
            val listType = object : TypeToken<TradeRecordModel>() {}.type
            val tradeRecordModel: TradeRecordModel = Gson().fromJson(mResponse,listType)

            if (tradeRecordModel.success) {
                if (tradeRecordModel.result.isNotEmpty()) {
                    tradeRecordDataList = tradeRecordModel.result
                    runOnUiThread {
                        progressBar.visibility = View.INVISIBLE
                        nAdapter?.updateData(tradeRecordDataList,storeTradeRecordTabLayout?.selectedTabPosition?:0)
                    }
                } else {
                    runOnUiThread {
                        progressBar.visibility = View.INVISIBLE
                        nAdapter?.updateData(null,0)
                        androidx.appcompat.app.AlertDialog.Builder(this@StoreTradeRecordActivity)
                            .setTitle("訊息提示")
                            .setMessage("目前尚無訂單紀錄！")
                            .setPositiveButton("確定") {d,w ->
                                d.cancel()
                            }
                            .show()
                    }
                }
            } else {
                runOnUiThread {
                    progressBar.visibility = View.INVISIBLE
                    nAdapter?.updateData(null,0)
                    androidx.appcompat.app.AlertDialog.Builder(this@StoreTradeRecordActivity)
                        .setTitle("訊息提示")
                        .setMessage("目前尚無訂單紀錄！")
                        .setPositiveButton("確定") {d,w ->
                            d.cancel()
                        }
                        .show()
                }
            }
        }
    }

    inner class UpdateStatusCallback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                progressBar.visibility = View.INVISIBLE
                androidx.appcompat.app.AlertDialog.Builder(this@StoreTradeRecordActivity)
                    .setTitle("訊息提示")
                    .setMessage("無法正常執行，請稍後再試！")
                    .setCancelable(false)
                    .setPositiveButton("確定") {d,w ->
                        d.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {

            val mResponse = response.body()?.string()
            val jsonObject = JSONObject(mResponse)
            val success = jsonObject.getBoolean("success")

            if (success) {
                runOnUiThread {
                    Toast.makeText(this@StoreTradeRecordActivity,"狀態變更成功！",Toast.LENGTH_SHORT).show()
                    GetTradeRecordApi().getTradeRecord(userInfoData?.store?.getOrNull(0)?.id?:"","1",storeTradeRecordTabLayout?.selectedTabPosition.toString(),Callback())
                }
            } else {
                runOnUiThread {
                    progressBar.visibility = View.INVISIBLE
                    androidx.appcompat.app.AlertDialog.Builder(this@StoreTradeRecordActivity)
                        .setTitle("訊息提示")
                        .setMessage("資料或系統出現問題，請稍後再試！")
                        .setCancelable(false)
                        .setPositiveButton("確定") {d,w ->
                            d.cancel()
                        }
                        .show()
                }
            }
        }
    }
}