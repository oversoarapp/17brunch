package tw.com.seventeenbrunch.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tw.com.seventeenbrunch.Model.OrderMenuItemModel
import tw.com.seventeenbrunch.Model.TradeRecordDataModel
import tw.com.seventeenbrunch.R

class CustomerTradeRecordAdapter(context: Context,val itemClick:(Int) -> Unit) : RecyclerView.Adapter<CustomerTradeRecordAdapter.CustomerTradeRecordViewHolder>() {

    private var dataList: List<TradeRecordDataModel>? = null
    private var context = context

    fun updateData(dataList:List<TradeRecordDataModel>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): CustomerTradeRecordViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_customer_trade_record, parent, false)
        return CustomerTradeRecordViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(holder: CustomerTradeRecordViewHolder, position: Int) {
        var createTime = dataList?.getOrNull(position)?.create_time?:""
        createTime = createTime.substring(0,createTime.length-3)
        holder.itemCustomerTradeRecordDate.text = createTime
        holder.itemCustomerTradeRecordNo.text = dataList?.getOrNull(position)?.trade_no?:""
        holder.itemCustomerTradeRecordTotal.text = "$ ${dataList?.getOrNull(position)?.total?:""}"
        when (dataList?.getOrNull(position)?.status) {
            "0" -> holder.itemCustomerTradeRecordStatus.text = "未領取"
            "1" -> holder.itemCustomerTradeRecordStatus.text = "已領取"
        }
        holder.itemView.setOnClickListener {
            itemClick(position)
        }
    }

    inner class CustomerTradeRecordViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val itemCustomerTradeRecordDate = itemView.findViewById<TextView>(R.id.itemCustomerTradeRecordDate)
        val itemCustomerTradeRecordNo = itemView.findViewById<TextView>(R.id.itemCustomerTradeRecordNo)
        val itemCustomerTradeRecordTotal = itemView.findViewById<TextView>(R.id.itemCustomerTradeRecordTotal)
        val itemCustomerTradeRecordStatus = itemView.findViewById<TextView>(R.id.itemCustomerTradeRecordStatus)

    }
}