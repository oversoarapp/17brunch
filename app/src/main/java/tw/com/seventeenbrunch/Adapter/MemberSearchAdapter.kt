package tw.com.seventeenbrunch.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tw.com.seventeenbrunch.Model.AllUserInfoModel
import tw.com.seventeenbrunch.R

class MemberSearchAdapter : RecyclerView.Adapter<MemberSearchAdapter.ViewHolder>() {

    private var dataList: List<AllUserInfoModel>? = null
    private var flag = 0

    fun updateData(dataList:List<AllUserInfoModel>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_search, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemSearchName.text = "姓名:${dataList?.getOrNull(position)?.name?:""}"
        holder.itemSearch2.text = "手機:${dataList?.getOrNull(position)?.account?:""}"
        holder.itemSearch3.text = "地址:${dataList?.getOrNull(position)?.address?:""}"
        holder.itemSearch4.text = "剩餘金額:${dataList?.getOrNull(position)?.points?:""}"

        holder.itemView.setOnClickListener {
            val pointsRecord = dataList?.getOrNull(position)?.pointsRecord
            if (flag == 0) {
                holder.pointsLayout.visibility = View.VISIBLE
                holder.itemSearchEd.visibility = View.GONE
                holder.itemSearchDate.text = "儲值日期:${pointsRecord?.create_time?.substring(0,16)?:""}"
                holder.itemSearchPoints.text = "儲值金額:${pointsRecord?.points?:""}"
                holder.itemSearchStore.text = "儲值店家:${pointsRecord?.store_name?:""}"
                flag = 1
            } else {
                holder.pointsLayout.visibility = View.GONE
                flag = 0
            }
        }
    }

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val pointsLayout:LinearLayout = itemView.findViewById(R.id.pointsLayout)
        val itemSearchName:TextView = itemView.findViewById(R.id.itemSearchName)
        val itemSearch2:TextView = itemView.findViewById(R.id.itemSearch2)
        val itemSearch3:TextView = itemView.findViewById(R.id.itemSearch3)
        val itemSearch4:TextView = itemView.findViewById(R.id.itemSearch4)
        val itemSearchDate:TextView = itemView.findViewById(R.id.itemSearchDate)
        val itemSearchPoints:TextView = itemView.findViewById(R.id.itemSearchPoints)
        val itemSearchStore:TextView = itemView.findViewById(R.id.itemSearchStore)
        val itemSearchEd:TextView = itemView.findViewById(R.id.itemSearchEd)
    }
}