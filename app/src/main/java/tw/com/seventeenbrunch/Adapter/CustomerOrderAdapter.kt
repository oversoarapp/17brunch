package tw.com.seventeenbrunch.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tw.com.seventeenbrunch.Model.OrderDataModel
import tw.com.seventeenbrunch.Model.OrderMenuItemModel
import tw.com.seventeenbrunch.R

class CustomerOrderAdapter(orderList: MutableList<OrderDataModel>,val itemClick:(String,String,Int) -> Unit) : RecyclerView.Adapter<CustomerOrderAdapter.CustomerOrderViewHolder>() {

    private var dataList: List<OrderMenuItemModel>? = null
    private var orderList = orderList

    fun updateData(dataList:List<OrderMenuItemModel>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): CustomerOrderViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_customer_order, parent, false)
        return CustomerOrderViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(holder: CustomerOrderViewHolder, position: Int) {

        holder.itemCustomerOrderName.text = dataList?.getOrNull(position)?.name
        holder.itemCustomerOrderPrice.text = "$ ${dataList?.getOrNull(position)?.price}"

//        orderList?.mapIndexed { index, orderDataModel ->
//            if (orderDataModel.itemName == dataList?.getOrNull(position)?.name) {
//                holder.itemCustomerOrderQty.setText("${orderDataModel.itemQty?:0}")
//                holder.itemCustomerOrderAdd.isEnabled = false
//                holder.itemCustomerOrderAdd.isFocusableInTouchMode = false
//            }
//        }

        holder.itemCustomerOrderAdd.setOnClickListener {
            if (holder.itemCustomerOrderQty.text.toString().isNotEmpty() && holder.itemCustomerOrderQty.text.toString() != "0") {
                itemClick(holder.itemCustomerOrderName.text.toString(),holder.itemCustomerOrderQty.text.toString(), position)
            }
        }

        holder.itemCustomerOrderPlus.setOnClickListener {
            if (holder.itemCustomerOrderQty.text.toString() != "" ) {
                val qty = holder.itemCustomerOrderQty.text.toString().toInt()
                val mQty = qty+1
                holder.itemCustomerOrderQty.setText(mQty.toString())
            } else {
                holder.itemCustomerOrderQty.setText("1")
            }
        }

        holder.itemCustomerOrderMinus.setOnClickListener {
            if (holder.itemCustomerOrderQty.text.toString() != "" && holder.itemCustomerOrderQty.text.toString() != "0") {
                if (holder.itemCustomerOrderQty.text.toString() == "1") {
                    holder.itemCustomerOrderQty.setText("")
                } else {
                    val qty = holder.itemCustomerOrderQty.text.toString().toInt()
                    val mQty = qty - 1
                    holder.itemCustomerOrderQty.setText(mQty.toString())
                }
            }
        }
    }

    inner class CustomerOrderViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val itemCustomerOrderName: TextView = itemView.findViewById<TextView>(R.id.itemCustomerOrderName)
        val itemCustomerOrderPrice = itemView.findViewById<TextView>(R.id.itemCustomerOrderPrice)
        val itemCustomerOrderMinus = itemView.findViewById<ImageView>(R.id.itemCustomerOrderMinus)
        val itemCustomerOrderPlus = itemView.findViewById<ImageView>(R.id.itemCustomerOrderPlus)
        val itemCustomerOrderQty = itemView.findViewById<EditText>(R.id.itemCustomerOrderQty)
        val itemCustomerOrderAdd = itemView.findViewById<ImageView>(R.id.itemCustomerOrderAdd)
    }
}