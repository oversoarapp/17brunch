//package tw.com.seventeenbrunch.Adapter
//
//import android.content.Context
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.widget.TextView
//import androidx.recyclerview.widget.RecyclerView
//import tw.com.seventeenbrunch.Model.AllStoreInfoModel
//import tw.com.seventeenbrunch.R
//
//class CompanyStoreManageAdapter(context: Context,val onItemClick: (Int) -> Unit) : RecyclerView.Adapter<CompanyStoreManageAdapter.CompanyStoreManageViewHolder>() {
//
//    private var dataList: List<AllStoreInfoModel>? = null
//    private var context = context
//
//    fun updateData(dataList:List<AllStoreInfoModel>?) {
//        this.dataList = dataList
//        notifyDataSetChanged()
//    }
//
//    override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): CompanyStoreManageViewHolder {
//        val view = LayoutInflater.from(parent.context)
//            .inflate(R.layout.item_search, parent, false)
//        return CompanyStoreManageViewHolder(view)
//    }
//
//    override fun getItemCount(): Int {
//        return dataList?.size?:0
//    }
//
//    override fun onBindViewHolder(holder: CompanyStoreManageViewHolder, position: Int) {
//        holder.itemManageStoreName.text = dataList?.get(position)?.store_name?:""
//        holder.itemManageStorePhone.text = dataList?.get(position)?.phone?:""
//        holder.itemManageStoreAds.text = dataList?.get(position)?.address?:""
//        holder.itemManageStoreBh.text = dataList?.get(position)?.business_hours?:""
//        holder.itemManageStorePoints.text = "$ ${dataList?.get(position)?.points?:""}"
//
//        holder.itemView.setOnClickListener {
//            onItemClick(position)
//        }
//    }
//
//    inner class CompanyStoreManageViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
//        val itemManageStoreName = itemView.findViewById<TextView>(R.id.itemManageStoreName)
//        val itemManageStorePhone = itemView.findViewById<TextView>(R.id.itemManageStorePhone)
//        val itemManageStoreAds = itemView.findViewById<TextView>(R.id.itemManageStoreAds)
//        val itemManageStoreBh = itemView.findViewById<TextView>(R.id.itemManageStoreBh)
//        val itemManageStorePoints = itemView.findViewById<TextView>(R.id.itemManageStorePoints)
//    }
//
//}