package tw.com.seventeenbrunch.Adapter

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tw.com.seventeenbrunch.Model.TradeRecordDataModel
import tw.com.seventeenbrunch.Model.UserInfoDataModel
import tw.com.seventeenbrunch.Model.UserInfoStoreModel
import tw.com.seventeenbrunch.Model.UserPointsModel
import tw.com.seventeenbrunch.R

class StoreListAdapter(val itemClick:(String,Int)->Unit) : RecyclerView.Adapter<StoreListAdapter.ViewHolder>() {

    private var dataList: List<UserPointsModel>? = null

    fun updateData(dataList:List<UserPointsModel>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_store_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemName.text = dataList?.getOrNull(position)?.store_name
        holder.itemPoints.text = "$ ${dataList?.getOrNull(position)?.points}"
        holder.itemDelete.setOnClickListener {
            itemClick(dataList?.getOrNull(position)?.id?:"",dataList?.getOrNull(position)?.points?:0)
        }

    }

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val itemName = itemView.findViewById<TextView>(R.id.itemName)
        val itemPoints = itemView.findViewById<TextView>(R.id.itemPoints)
        val itemDelete = itemView.findViewById<Button>(R.id.itemDelete)
    }
}