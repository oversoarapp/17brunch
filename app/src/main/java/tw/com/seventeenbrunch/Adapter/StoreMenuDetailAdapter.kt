package tw.com.seventeenbrunch.Adapter

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tw.com.seventeenbrunch.R

class StoreMenuDetailAdapter(context: Context,val removeData:(Int) -> Unit,val editData:(Int,String,String) -> Unit) : RecyclerView.Adapter<StoreMenuDetailAdapter.StoreMenuDetailViewHolder>() {

    private var nameData:MutableList<String>? = null
    private var priceData:MutableList<String>? = null
    private var context = context
    private var flag = 0

    fun updateData(nameData:MutableList<String>,priceData:MutableList<String>) {
        this.nameData = nameData
        this.priceData = priceData
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): StoreMenuDetailViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_store_menu_detail, parent, false)
        return StoreMenuDetailViewHolder(view)
    }

    override fun getItemCount(): Int {
        return nameData?.size?:0
    }

    override fun onBindViewHolder(holder: StoreMenuDetailViewHolder, position: Int) {
        holder.itemStoreMenuDetailEdit.text = "修改"
        holder.itemStoreMenuDetailName.setText(nameData?.getOrNull(position))
        holder.itemStoreMenuDetailPrice.setText(priceData?.getOrNull(position))
        holder.itemStoreMenuDetailDelete.setOnClickListener {
            AlertDialog.Builder(context)
                .setTitle("訊息提示")
                .setMessage("是否確定刪除項目？")
                .setNegativeButton("確定") {d,w ->
                    removeData(position)
                    d.cancel()
                }
                .setPositiveButton("取消") {d,w ->
                    d.cancel()
                }
                .show()
        }
        holder.itemStoreMenuDetailEdit.setOnClickListener {
            if (flag == 0) {
                flag = 1
                holder.itemStoreMenuDetailName.isEnabled = true
                holder.itemStoreMenuDetailPrice.isEnabled = true
                holder.itemStoreMenuDetailName.isFocusableInTouchMode = true
                holder.itemStoreMenuDetailPrice.isFocusableInTouchMode = true
                holder.itemStoreMenuDetailEdit.text = "完成"
            } else {
                flag = 0
                editData(position,holder.itemStoreMenuDetailName.text.toString(),holder.itemStoreMenuDetailPrice.text.toString())
                holder.itemStoreMenuDetailEdit.text = "修改"
                holder.itemStoreMenuDetailName.isEnabled = false
                holder.itemStoreMenuDetailPrice.isEnabled = false
                holder.itemStoreMenuDetailName.isFocusableInTouchMode = false
                holder.itemStoreMenuDetailPrice.isFocusableInTouchMode = false
            }
        }
    }

    inner class StoreMenuDetailViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val itemStoreMenuDetailName = itemView.findViewById<EditText>(R.id.itemStoreMenuDetailName)
        val itemStoreMenuDetailPrice = itemView.findViewById<EditText>(R.id.itemStoreMenuDetailPrice)
        val itemStoreMenuDetailDelete = itemView.findViewById<ImageView>(R.id.itemStoreMenuDetailDelete)
        val itemStoreMenuDetailEdit = itemView.findViewById<Button>(R.id.itemStoreMenuDetailEdit)
    }
}