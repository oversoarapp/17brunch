//package tw.com.seventeenbrunch.Adapter
//
//import android.annotation.SuppressLint
//import android.content.Context
//import android.content.Intent
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.widget.BaseAdapter
//import kotlinx.android.synthetic.main.item_store_menu_edit.view.*
//import android.widget.AbsListView
//import android.widget.Toast
//import androidx.appcompat.app.AlertDialog
//import tw.com.seventeenbrunch.Model.OrderMenuTypeModel
//import tw.com.seventeenbrunch.R
//import tw.com.seventeenbrunch.StoreMenuDetailActivity
//
//
//class StoreMenuEditAdapter(
//    private var context: Context,val removeData:(String)->Unit,val nextPageClick:(Int)->Unit) : BaseAdapter() {
//
//    var data :List<OrderMenuTypeModel>?= null
//
//    fun updateData(data:List<OrderMenuTypeModel>) {
//        this.data = data
//        notifyDataSetChanged()
//    }
//
//    override fun getCount(): Int {
//        return data?.size?:0
//    }
//
//    override fun getItem(position: Int): Any? {
//        return null
//    }
//
//    override fun getItemId(position: Int): Long {
//        return 0
//    }
//
//    @SuppressLint("ViewHolder")
//    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
//        val name = data?.getOrNull(position)?.name
//
//        var inflator = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//        var mView = inflator.inflate(R.layout.item_store_menu_edit, null)
//
//        mView.itemStoreMenuEditText.text = name
//
//        mView.setOnClickListener {
//            nextPageClick(position)
//        }
//
//        mView.setOnLongClickListener {
//            AlertDialog.Builder(context)
//                .setTitle("訊息提示")
//                .setMessage("注意！刪除類別會將其中的細項一併刪除\n請問是否刪除類別:$name？")
//                .setNegativeButton("取消") { d, w -> d.cancel() }
//                .setPositiveButton("確定") { d, w ->
//                    removeData(data?.getOrNull(position)?.id?:"")
//                    d.cancel()
//                }
//                .show()
//            return@setOnLongClickListener false
//        }
//
//        val param = AbsListView.LayoutParams(
//            300,
//            300
//        )
//        mView.layoutParams = param
//
//        return mView
//    }
//}
