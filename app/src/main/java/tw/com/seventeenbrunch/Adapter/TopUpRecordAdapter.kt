package tw.com.seventeenbrunch.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tw.com.seventeenbrunch.Model.OrderMenuItemModel
import tw.com.seventeenbrunch.Model.TopUpRecordDataModel
import tw.com.seventeenbrunch.Model.TradeRecordDataModel
import tw.com.seventeenbrunch.R
import java.text.SimpleDateFormat
import java.util.*

class TopUpRecordAdapter: RecyclerView.Adapter<TopUpRecordAdapter.ViewHolder>() {

    private var dataList: List<TopUpRecordDataModel>? = null
    private var identity = ""

    fun updateData(dataList:List<TopUpRecordDataModel>?,identity:String) {
        this.dataList = dataList
        this.identity = identity
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_top_up_record, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemRecordDate.text = "儲值日期:${dataList?.getOrNull(position)?.create_time?.substring(0,16)}"
        holder.itemRecordPoints.text = "儲值金額:${dataList?.getOrNull(position)?.points}"
        holder.itemRecordStore.text = "儲值店家:${dataList?.getOrNull(position)?.store_name}"

        when (identity) {
            "1" -> {
                var dateFormat = SimpleDateFormat("yyyy-MM-dd")
                var startDate = dateFormat.parse(dataList?.getOrNull(position)?.use_time)
                var c = Calendar.getInstance()
                c.time = startDate
                c.add(Calendar.DAY_OF_MONTH,dataList?.getOrNull(position)?.expiry_date?.toInt()?:0)
                var endDate = dateFormat.format(c.time)
                holder.itemRecordEd.text = "使用期限:\n ${dateFormat.format(startDate)} - $endDate"
            }
            "0" -> {
                holder.itemRecordEd.visibility = View.GONE
            }
        }
    }

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val itemRecordDate = itemView.findViewById<TextView>(R.id.itemRecordDate)
        val itemRecordStore = itemView.findViewById<TextView>(R.id.itemRecordStore)
        val itemRecordPoints  = itemView.findViewById<TextView>(R.id.itemRecordPoints)
        val itemRecordEd = itemView.findViewById<TextView>(R.id.itemRecordEd)
    }
}