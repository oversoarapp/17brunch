package tw.com.seventeenbrunch.Adapter

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tw.com.seventeenbrunch.Model.TradeItemModel
import tw.com.seventeenbrunch.Model.TradeRecordDataModel
import tw.com.seventeenbrunch.R

class TradeDetailRecordAdapter : RecyclerView.Adapter<TradeDetailRecordAdapter.TradeDetailRecordViewHolder>() {

    private var dataList: List<TradeItemModel>? = null

    fun updateData(dataList:List<TradeItemModel>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): TradeDetailRecordViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_trade_detail_record, parent, false)
        return TradeDetailRecordViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(holder: TradeDetailRecordViewHolder, position: Int) {
        holder.itemTradeDetailName.text = dataList?.getOrNull(position)?.item_name?:""
        holder.itemTradeDetailQty.text = "x${dataList?.getOrNull(position)?.item_qty?:""}"
        holder.itemTradeDetailPrice.text = "$${dataList?.getOrNull(position)?.price?:""}"
        holder.itemTradeDetailTotal.text = "$${dataList?.getOrNull(position)?.amount?:""}"
        if (dataList?.getOrNull(position)?.item_dec?.isNotEmpty() == true) {
            holder.itemTradeDetailDec.visibility = View.VISIBLE
            holder.itemTradeDetailDec.text = "備註:${dataList?.getOrNull(position)?.item_dec?:""}"
        } else {
            holder.itemTradeDetailDec.visibility = View.GONE
        }
    }

    inner class TradeDetailRecordViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val itemTradeDetailName = itemView.findViewById<TextView>(R.id.itemTradeDetailName)
        val itemTradeDetailPrice = itemView.findViewById<TextView>(R.id.itemTradeDetailPrice)
        val itemTradeDetailQty = itemView.findViewById<TextView>(R.id.itemTradeDetailQty)
        val itemTradeDetailTotal = itemView.findViewById<TextView>(R.id.itemTradeDetailTotal)
        val itemTradeDetailDec = itemView.findViewById<TextView>(R.id.itemTradeDetailDec)
    }
}