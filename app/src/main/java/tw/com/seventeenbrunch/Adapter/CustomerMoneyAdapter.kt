package tw.com.seventeenbrunch.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tw.com.seventeenbrunch.R

class CustomerMoneyAdapter(data: MutableList<String>) : RecyclerView.Adapter<CustomerMoneyAdapter.ViewHolder>() {

    private var dataList = data

    fun updateData(dataList:MutableList<String>?) {
        this.dataList = dataList!!
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemListTv.text = dataList.getOrNull(position)
    }

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val itemListTv: TextView = itemView.findViewById<TextView>(R.id.tv)
    }
}