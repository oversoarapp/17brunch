package tw.com.seventeenbrunch.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tw.com.seventeenbrunch.Model.StoreCouponDataModel
import tw.com.seventeenbrunch.R

class StoreCouponAdapter(context: Context,val itemClick:(Int) -> Unit) : RecyclerView.Adapter<StoreCouponAdapter.StoreCouponViewHolder>() {

    private var dataList: List<StoreCouponDataModel>? = null
    private var context = context

    fun updateData(dataList:List<StoreCouponDataModel>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): StoreCouponViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_store_coupon, parent, false)
        return StoreCouponViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(holder: StoreCouponViewHolder, position: Int) {

        holder.itemStoreCouponName.text = dataList?.getOrNull(position)?.name
        holder.itemStoreCouponLimited.text = "$ ${dataList?.getOrNull(position)?.limited}"
        holder.itemStoreCouponDiscount.text = "- ${dataList?.getOrNull(position)?.discount}"
        holder.itemStoreCouponDelete.setOnClickListener {
            itemClick(position)
        }

    }

    inner class StoreCouponViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val itemStoreCouponName = itemView.findViewById<TextView>(R.id.itemStoreCouponName)
        val itemStoreCouponLimited = itemView.findViewById<TextView>(R.id.itemStoreCouponLimited)
        val itemStoreCouponDiscount = itemView.findViewById<TextView>(R.id.itemStoreCouponDiscount)
        val itemStoreCouponDelete = itemView.findViewById<ImageView>(R.id.itemStoreCouponDelete)
    }
}