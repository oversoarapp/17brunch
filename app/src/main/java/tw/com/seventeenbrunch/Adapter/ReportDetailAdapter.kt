package tw.com.seventeenbrunch.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tw.com.seventeenbrunch.Model.ReportItemDetailModel
import tw.com.seventeenbrunch.R


class ReportDetailAdapter: RecyclerView.Adapter<ReportDetailAdapter.ViewHolder>() {

    private var dataList: List<ReportItemDetailModel>? = null

    fun updateData(dataList:List<ReportItemDetailModel>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_report, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.name.text = dataList?.getOrNull(position)?.item_name?:""
        holder.price.text = "$ ${dataList?.getOrNull(position)?.item_price?:""}"
        holder.qty.text = "x ${dataList?.getOrNull(position)?.item_qty?:""}"
        holder.total.text = "$ ${dataList?.getOrNull(position)?.amount?:""}"

    }

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val name = itemView.findViewById<TextView>(R.id.itemReportName)
        val price = itemView.findViewById<TextView>(R.id.itemReportPrice)
        val qty = itemView.findViewById<TextView>(R.id.itemReportQty)
        val total = itemView.findViewById<TextView>(R.id.itemReportTotal)
    }
}