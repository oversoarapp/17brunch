package tw.com.seventeenbrunch.Adapter

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tw.com.seventeenbrunch.Model.TradeRecordDataModel
import tw.com.seventeenbrunch.R

class StoreTradeRecordAdapter(context: Context,val itemClick:(Int)->Unit,val updateStatusClick:(Int)->Unit) : RecyclerView.Adapter<StoreTradeRecordAdapter.StoreTradeRecordViewHolder>() {

    private var dataList: List<TradeRecordDataModel>? = null
    private var context = context
    private var type = 0

    fun updateData(dataList:List<TradeRecordDataModel>?,type:Int) {
        this.dataList = dataList
        this.type = type
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): StoreTradeRecordViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_store_trade_record, parent, false)
        return StoreTradeRecordViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(holder: StoreTradeRecordViewHolder, position: Int) {
        var createTime = dataList?.getOrNull(position)?.create_time?:""
        createTime = createTime.substring(0,createTime.length-3)
        holder.itemStoreTradeRecordDate.text = createTime
        holder.itemStoreTradeRecordAcc.text = dataList?.getOrNull(position)?.account?:""
        holder.itemStoreTradeRecordNo.text = dataList?.getOrNull(position)?.trade_no?:""
        holder.itemStoreTradeRecordTotal.text = "$ ${dataList?.getOrNull(position)?.total?:""}"

        when (dataList?.getOrNull(position)?.status) {
            "0" -> {
                holder.itemStoreTradeRecordStatus.text = "未領取"
                if (type ==1) {
                    holder.itemStoreTradeRecordStatus.isEnabled = false
                    holder.itemStoreTradeRecordStatus.isFocusableInTouchMode = false
                } else {
                    holder.itemStoreTradeRecordStatus.isEnabled = true
                    holder.itemStoreTradeRecordStatus.isFocusableInTouchMode = true
                }
            }
            "1" -> {
                holder.itemStoreTradeRecordStatus.text = "已領取"
                holder.itemStoreTradeRecordStatus.isEnabled = false
                holder.itemStoreTradeRecordStatus.isFocusableInTouchMode = false
            }
        }

        holder.itemStoreTradeRecordStatus.setOnClickListener {
            AlertDialog.Builder(context)
                .setTitle("訊息提示")
                .setMessage("請問顧客是否已確認領取？")
                .setNegativeButton("確定") {d,w ->
                    updateStatusClick(position)
                }
                .setPositiveButton("取消") {d,w ->
                    d.cancel()
                }
                .show()
        }

        holder.itemView.setOnClickListener {
            itemClick(position)
        }

    }

    inner class StoreTradeRecordViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val itemStoreTradeRecordDate = itemView.findViewById<TextView>(R.id.itemStoreTradeRecordDate)
        val itemStoreTradeRecordAcc = itemView.findViewById<TextView>(R.id.itemStoreTradeRecordAcc)
        val itemStoreTradeRecordNo = itemView.findViewById<TextView>(R.id.itemStoreTradeRecordNo)
        val itemStoreTradeRecordTotal = itemView.findViewById<TextView>(R.id.itemStoreTradeRecordTotal)
        val itemStoreTradeRecordStatus = itemView.findViewById<Button>(R.id.itemStoreTradeRecordStatus)
    }
}