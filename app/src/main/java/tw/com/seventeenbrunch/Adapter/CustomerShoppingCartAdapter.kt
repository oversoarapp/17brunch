package tw.com.seventeenbrunch.Adapter

import android.app.AlertDialog
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import org.w3c.dom.Text
import tw.com.seventeenbrunch.Model.CouponUsedDataModel
import tw.com.seventeenbrunch.Model.OrderDataModel
import tw.com.seventeenbrunch.Model.UserCouponDataModel
import tw.com.seventeenbrunch.R

class CustomerShoppingCartAdapter(context: Context,val countPrice:(Int) -> Unit,val itemCancel:(Int)->Unit,val saveItemDec:(Int,String)->Unit) : RecyclerView.Adapter<CustomerShoppingCartAdapter.CustomerShoppingCartViewHolder>() {

    var itemData = mutableListOf<OrderDataModel>()
    var context = context

    fun updateData(itemData:MutableList<OrderDataModel>) {
//        this.typeFlagList = flagList
//        this.haveCouponList = haveCouponList
//        this.couponDataList = couponDataList
        this.itemData = itemData
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): CustomerShoppingCartViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_customer_shopping_cart, parent, false)
        return CustomerShoppingCartViewHolder(view)
    }

    override fun getItemCount(): Int {
        return itemData.size
    }

    override fun onBindViewHolder(holder: CustomerShoppingCartViewHolder, position: Int) {

        holder.itemShoppingCartDec.removeTextChangedListener(holder.decTextWatcher)
        holder.itemShoppingCartQty.removeTextChangedListener(holder.qtyTextWatcher)
        holder.itemShoppingCartName.text = itemData.getOrNull(position)?.itemName
        holder.itemShoppingCartPrice.text = "$ ${itemData.getOrNull(position)?.itemPrice}"
        holder.itemShoppingCartAmount.text = "$ ${itemData.getOrNull(position)?.itemAmount}"
        holder.itemShoppingCartQty.setText(itemData.getOrNull(position)?.itemQty.toString())
        holder.itemShoppingCartDec.setText(itemData.getOrNull(position)?.itemDec)

        holder.itemShoppingCartDelete.setOnClickListener {
            itemCancel(position)
        }

        holder.decTextWatcher = DecTextWatcher(position)
        holder.itemShoppingCartDec.addTextChangedListener(holder.decTextWatcher)

        holder.itemShoppingCartPlus.setOnClickListener {
            var qty = holder.itemShoppingCartQty.text.toString().toInt()
            holder.itemShoppingCartQty.setText("${qty+1}")
        }

        holder.itemShoppingCartMinus.setOnClickListener {
            var qty = holder.itemShoppingCartQty.text.toString().toInt()
            if (qty > 1 ) {
                holder.itemShoppingCartQty.setText("${qty - 1}")
            }
        }

        holder.qtyTextWatcher = QtyTextWatcher(holder.itemShoppingCartQty,holder.itemShoppingCartAmount,position)
        holder.itemShoppingCartQty.addTextChangedListener(holder.qtyTextWatcher)

    }

    inner class CustomerShoppingCartViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val itemShoppingCartName: TextView = itemView.findViewById<TextView>(R.id.itemShoppingCartName)
        val itemShoppingCartAmount: TextView = itemView.findViewById<TextView>(R.id.itemShoppingCartAmount)
        val itemShoppingCartPrice = itemView.findViewById<TextView>(R.id.itemShoppingCartPrice)
        val itemShoppingCartDec = itemView.findViewById<EditText>(R.id.itemShoppingCartDec)
        val itemShoppingCartQty = itemView.findViewById<EditText>(R.id.itemShoppingCartQty)
        val itemShoppingCartDelete = itemView.findViewById<TextView>(R.id.itemShoppingCartDelete)
        val itemShoppingCartPlus = itemView.findViewById<ImageView>(R.id.itemShoppingCartPlus)
        val itemShoppingCartMinus = itemView.findViewById<ImageView>(R.id.itemShoppingCartMinus)
        var decTextWatcher: DecTextWatcher? = null
        var qtyTextWatcher: QtyTextWatcher? = null
    }

    inner class DecTextWatcher(val position: Int): android.text.TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (s?.isNotEmpty() == true) {
                saveItemDec(position,s.toString())
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    }

    inner class QtyTextWatcher(val editText: EditText,val textView: TextView,val position: Int): android.text.TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (!s.isNullOrEmpty()) {
                if (s.toString() != "0") {
                    var qty = s.toString().toInt()
                    var price = itemData.getOrNull(position)?.itemPrice ?: 0
                    itemData.getOrNull(position)?.itemQty = qty
                    itemData.getOrNull(position)?.itemAmount = qty * price
                    textView.text = "$ ${itemData.getOrNull(position)?.itemAmount}"
                    countPrice(position)
                } else {
                    editText.setText("1")
                }
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    }
}