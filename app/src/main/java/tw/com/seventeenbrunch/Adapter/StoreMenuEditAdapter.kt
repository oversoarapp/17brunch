package tw.com.seventeenbrunch.Adapter

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tw.com.seventeenbrunch.Model.*
import tw.com.seventeenbrunch.R

class StoreMenuEditAdapter(private var context: Context,val removeData:(String)->Unit,val nextPageClick:(Int)->Unit) : RecyclerView.Adapter<StoreMenuEditAdapter.ViewHolder>() {

    private var dataList :List<OrderMenuTypeModel>?= null

    fun updateData(dataList:List<OrderMenuTypeModel>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder (parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_store_menu_edit, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemStoreMenuEditText.text = dataList?.getOrNull(position)?.name

        holder.itemView.setOnClickListener {
            nextPageClick(position)
        }

        holder.itemStoreMenuEditDelete.setOnClickListener {
            AlertDialog.Builder(context)
                .setTitle("訊息提示")
                .setMessage("注意！刪除類別會將其中的細項一併刪除\n請問是否刪除類別:${dataList?.getOrNull(position)?.name}？")
                .setPositiveButton("取消") { d, w -> d.cancel() }
                .setNegativeButton("確定") { d, w ->
                    removeData(dataList?.getOrNull(position)?.id?:"")
                    d.cancel()
                }
                .show()
        }
    }

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val itemStoreMenuEditText = itemView.findViewById<TextView>(R.id.itemStoreMenuEditText)
        val itemStoreMenuEditDelete = itemView.findViewById<ImageView>(R.id.itemStoreMenuEditDelete)
    }
}