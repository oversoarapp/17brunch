package tw.com.seventeenbrunch

class FoodsBean {
    private var name = ""
    private var price = ""
    private var number = ""
    private var dec = ""

    public fun getName():String {
        return name;
    }
    public fun setName(name: String) {
        this.name = name
    }
    public fun getPrice():String{
        return price
    }
    public fun setPrice(price: String) {
        this.price = price
    }
    public fun getNumber(): String {
        return number;
    }
    public fun setNumber(number: String) {
        this.number = number
    }
    public fun getDec(): String {
        return dec;
    }
    public fun setDec(dec: String) {
        this.dec = dec
    }
}