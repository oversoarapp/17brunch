package tw.com.seventeenbrunch

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_signup.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.Call
import okhttp3.Response
import org.json.JSONException
import org.json.JSONObject
import tw.com.seventeenbrunch.Api.GetAllStoreApi
import tw.com.seventeenbrunch.Api.SignUpApi
import tw.com.seventeenbrunch.Model.AllStoreInfoModel
import tw.com.seventeenbrunch.Model.AllStoreModel
import java.io.IOException
import android.text.Editable
import android.text.TextWatcher
import android.view.ViewGroup
import java.util.regex.Pattern
import java.util.regex.PatternSyntaxException


class SignUpActivity : AppCompatActivity() {

    companion object {
        const val IDENTITY = "identity"
        const val TOKEN = "token"
    }

    private var identity = ""
    private var etHeight = 0
    private var mHeight = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        initView()

    }

    private fun initView() {

        identity = intent?.getStringExtra(IDENTITY)?:""

        if (identity == "1") {
            linearLayout9.visibility = View.VISIBLE
            signUpTitle.text = "店家註冊"
            signUpAddress.hint = "店址"
        } else {
            signUpTitle.text = "會員註冊"
        }

        signUpRandomNum.text = random(9000,1000)
        val lp = signUpAddress.layoutParams
        etHeight = lp.height

        signUpCancel.setOnClickListener {
            finish()
        }

        signUpDone.setOnClickListener {
            var list: List<String>? = null
            val name = signUpName.text.toString()
            val acc = signUpAccount.text.toString()
            val pwd = signUpPassword.text.toString()
            val rePwd = signUpRePassword.text.toString()
            val num = signUpCaptcha.text.toString()
            var mail = signUpEmail.text.toString()
            val ads = signUpAddress.text.toString()
            var storeName =""
            var storeBh = ""
            var storePhone = ""
            when (identity) {
                "0" -> {
                    list = listOf(name, acc, pwd, rePwd, mail, ads, num)
                }
                "1" -> {
                    storeName = signUpStoreName.text.toString()
                    storeBh = signUpStoreBh.text.toString()
                    storePhone = signUpStorePhone.text.toString()
                    list = listOf(name, acc, pwd, rePwd, num, mail ,storeName, storeBh, storePhone, ads)
                }
            }
            var isEmpty = false
            for (i in list?.indices!!) {
                if (list[i].isEmpty()) {
                    isEmpty = true
                }
            }
            if (!isEmpty) {
                if (acc.length == 10 ) {
                    if (pwd.length >= 4 && rePwd.length >= 4) {
                        if (pwd == rePwd) {
                            if (signUpCaptcha.text.toString() == signUpRandomNum.text.toString()) {
                                progressBar12.visibility = View.VISIBLE
                                SignUpApi().signUp(
                                    name,
                                    acc,
                                    pwd,
                                    mail,
                                    ads,
                                    identity,
                                    storeName,
                                    storeBh,
                                    ads,
                                    storePhone,
                                    intent?.getStringExtra(TOKEN) ?: "",
                                    Callback()
                                )
                            } else {
                                Toast.makeText(this, "驗證碼錯誤！", Toast.LENGTH_LONG).show()
                            }
                        } else {
                            Toast.makeText(this, "密碼與重複密碼不符！", Toast.LENGTH_LONG).show()
                        }
                    } else {
                        Toast.makeText(this, "密碼需符合四碼以上！", Toast.LENGTH_LONG).show()
                    }
                } else {
                    Toast.makeText(this, "手機號碼應為十碼！", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(this, "欄位皆不得空白！", Toast.LENGTH_LONG).show()
            }
        }

        signUpReFresh.setOnClickListener {
            signUpRandomNum.text = random(9000,1000)
        }

        signUpAddress.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if (s?.isNotEmpty() == true) {
                    if (s.length >= 15) {
                        lp.height = ViewGroup.LayoutParams.WRAP_CONTENT
                        signUpAddress.layoutParams = lp
                    } else {
                        lp.height = etHeight
                        signUpAddress.layoutParams = lp
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })

        signUpName.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val s = signUpName.text.toString()
                val str = stringFilter(s)
                if(s != str){
                    signUpName.setText(str)
                    signUpName.setSelection(str.length)
                }
            }
        })
    }

    fun stringFilter(str: String): String {
        try {
        // 只允許字母、漢字
            val regEx = "[^a-zA\u4E00-\u9FA5]"
            val p = Pattern.compile(regEx)
            val m = p.matcher(str)
            return m.replaceAll("").trim()
        } catch (e:PatternSyntaxException) {
            e.printStackTrace()
        }
        return ""
    }

    private fun random(max:Int,min:Int):String{
        var r = ((Math.random() * (max - min + 1))+ 20).toInt().toString()
        if (r.length < 4) {
            r = ((Math.random() * (max - min + 1))+ 20).toInt().toString()
        }
        return r
    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                progressBar12.visibility = View.INVISIBLE
                e.printStackTrace()
                AlertDialog.Builder(this@SignUpActivity)
                    .setTitle("訊息提示")
                    .setMessage("系統或網路出現錯誤，請稍後再試！")
                    .setPositiveButton("確定") {d,w ->
                        d.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {
            try {
                val mResponse = response.body()?.string()
                val jb = JSONObject(mResponse)
                val success = jb.getBoolean("success")

                if (success) {
                    runOnUiThread {
                        progressBar12.visibility = View.INVISIBLE
                        Toast.makeText(this@SignUpActivity,"註冊成功！\n帳號進入審核，待儲值完畢即可使用！",Toast.LENGTH_LONG).show()
                        startActivity(Intent(this@SignUpActivity,MainActivity::class.java))
                        finish()
                    }
                } else {
                    val msg = jb.getString("msg")
                    runOnUiThread {
                        progressBar12.visibility = View.INVISIBLE
                        AlertDialog.Builder(this@SignUpActivity)
                            .setTitle("訊息提示")
                            .setMessage(msg)
                            .setPositiveButton("確定") {d,w ->
                                d.cancel()
                            }
                            .show()
                    }
                }
            }catch (e:JSONException) {
                e.printStackTrace()
            }

        }
    }
}