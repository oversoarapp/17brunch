package tw.com.seventeenbrunch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_print_setting.*
import okhttp3.Call
import okhttp3.Response
import org.json.JSONObject
import tw.com.seventeenbrunch.Model.UserInfoModel
import java.io.IOException

class PrintActivity : AppCompatActivity() {

    companion object {
        const val USERRESPONSE = "userResponse"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_print_setting)

        initView()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("是否離開APP？")
                .setNegativeButton("確定") { dialog, which ->
                    finish()
                }
                .setPositiveButton("取消") { dialog, which ->
                    dialog.cancel()
                }
                .show()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun initView() {

//        val mResponse = intent.getStringExtra(StoreActivity.USERRESPONSE)
//        val listType = object : TypeToken<UserInfoModel>() {}.type
//        val userInfoModel: UserInfoModel = Gson().fromJson(mResponse, listType)

        val sp = getSharedPreferences("userData", Context.MODE_PRIVATE)
        val ip = sp?.getString("ip","")
        val sIp = sp?.getString("sIp","")
        if (ip != "") {
            printIp.setText(ip)
        }
        if (sIp != "") {
            printStickerIp.setText(sIp)
        }

        printMenu.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,intent.getStringExtra(USERRESPONSE)?:"",this).StoreMenu())

        printDone.setOnClickListener {
            sp.edit()?.putString("ip",printIp.text.toString())?.apply()
            sp.edit()?.putString("sIp",printStickerIp.text.toString())?.apply()
            Toast.makeText(this,"設定成功！",Toast.LENGTH_LONG).show()
        }
    }
}