package tw.com.seventeenbrunch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.godex.Godex
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_trade_detail_record.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONObject
import tw.com.seventeenbrunch.Adapter.TradeDetailRecordAdapter
import tw.com.seventeenbrunch.Api.GetTradeDetailRecordApi
import tw.com.seventeenbrunch.Model.TradeDetailRecordModel
import tw.com.seventeenbrunch.Model.TradeItemModel
import tw.com.seventeenbrunch.Model.UserInfoModel
import java.io.IOException
import java.net.UnknownHostException

class STradeDetailRecordActivity : AppCompatActivity() {

    companion object {
        const val TRADENO = "tradeNo"
        const val TOTAL = "total"
        const val RESPONSE = "response"
        const val FROM = "from"
    }

    private var ip = ""
    private var sIp = ""
    private var total = 0
    private var userInfoModel: UserInfoModel? = null
    private var pos: Pos? = null
    private var foodsBean: MutableList<FoodsBean>? = null
    private var tradeItemList: List<TradeItemModel>? = null
    private var tradeRecordModel: TradeDetailRecordModel? = null
    private var nAdapter = TradeDetailRecordAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trade_detail_record)

        initView()

    }

    private fun initView() {

        val response = intent?.getStringExtra(RESPONSE)
        val listType = object :TypeToken<UserInfoModel>(){}.type
        userInfoModel = Gson().fromJson<UserInfoModel>(response,listType)

        val sp = getSharedPreferences("userData", Context.MODE_PRIVATE)
        ip = sp?.getString("ip","")?:""
        sIp = sp?.getString("sIp","")?:""
        total = intent?.getStringExtra(TOTAL)?.toInt()?:0

        GetTradeDetailRecordApi().getTradeDetailRecord(intent.getStringExtra(TRADENO)?:"",Callback())

        navigation_view.menu.clear()
        navigation_view.inflateMenu(R.menu.store_menu)
        view4.background = resources.getDrawable(R.color.colorDarkPeach)
        textView12.setTextColor(resources.getColor(R.color.colorWhite))
        layout.background = resources.getDrawable(R.color.colorLightPeach)
        if (intent?.getStringExtra(FROM) == "過去訂單") {
            linearLayout5.visibility = View.INVISIBLE
        } else {
            linearLayout5.visibility = View.VISIBLE
        }
        tradeDetailRecordRv.layoutManager = LinearLayoutManager(this)
        tradeDetailRecordRv.adapter = nAdapter

        Godex.getMainContext(this)

        val mutiThread = Runnable {
            pos = Pos()   //第一个参数是打印机网口IP
            pos?.pos(this, ip, 9100, charset("GBK"))
            if (pos?.sock?.isConnected == true) {
                printReceipt()
            } else {
                runOnUiThread {
                    AlertDialog.Builder(this)
                        .setTitle("錯誤提示")
                        .setMessage("收據機連線異常，請確認網路、機台狀態與IP位置！")
                        .setPositiveButton("確定") { d, w -> d.cancel() }
                        .show()
                }
            }
        }

//        print.setOnClickListener {
//            if (ip != "" && sIp != "" ) {
//                val thread = Thread(mutiThread)
//                thread.start()
//                printSticker()
//            } else if (ip == "") {
//                Toast.makeText(this,"收據機IP位置空白，請至標籤機設定中確認！",Toast.LENGTH_LONG).show()
//            } else if (sIp == "") {
//                Toast.makeText(this,"貼紙機IP位置空白，請至標籤機設定中確認！",Toast.LENGTH_LONG).show()
//            }
//        }

        printReceipt.setOnClickListener {
            if (ip != "" ) {
                val thread = Thread(mutiThread)
                thread.start()
            } else {
                Toast.makeText(this,"收據機IP位置空白，請至標籤機設定中確認！",Toast.LENGTH_LONG).show()
            }
        }

        printSticker.setOnClickListener {
            if (sIp != "" ) {
                printSticker()
            } else {
                Toast.makeText(this,"貼紙機IP位置空白，請至標籤機設定中確認！",Toast.LENGTH_LONG).show()
            }
        }

        tradeDetailRecordMenu.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,intent.getStringExtra(RESPONSE)?:"",this).StoreMenu())

    }

    private fun initData() {
        foodsBean = mutableListOf()

        for (i in 0 until tradeItemList?.size!!) {
            var fb = FoodsBean()
            fb.setName(tradeItemList?.getOrNull(i)?.item_name?:"")
            fb.setPrice(tradeItemList?.getOrNull(i)?.amount?:"")
            fb.setNumber(tradeItemList?.getOrNull(i)?.item_qty.toString())
            fb.setDec("備註:${tradeItemList?.getOrNull(i)?.item_dec?:""}")
            foodsBean?.add(fb)
        }
    }

    private fun printSticker() {
        if (!Godex.openport(sIp, 1)) {
            AlertDialog.Builder(this)
                .setTitle("錯誤提示")
                .setMessage("貼紙機連線異常，請確認網路、機台狀態與IP位置！")
                .setPositiveButton("確定") {d,w->d.cancel()}
                .show()
        } else {
            Godex.setup("30", "10", "2", "0", "1", "0")
            for (i in 0 until tradeItemList?.size!!) {
                var tradeItem = tradeItemList?.getOrNull(i)
                var qty = tradeItem?.item_qty?:1
                for (x in 0 until qty) {
                    Godex.sendCommand("^L")
                    Godex.AsiaFont_TextOut(
                        Godex.AsianFontID.Z1,
                        20,
                        20,
                        2,
                        2,
                        1,
                        "0",
                        "APP ${tradeRecordModel?.result?.tradeNo?.substring(11)}",
                        Godex.AsiaEncoding.BIG5
                    )
                    Godex.AsiaFont_TextOut(
                        Godex.AsianFontID.Z1,
                        250,
                        30,
                        1,
                        1,
                        2,
                        "0",
                        "${x+1}/$qty",
                        Godex.AsiaEncoding.BIG5
                    )
                    Godex.AsiaFont_TextOut(
                        Godex.AsianFontID.Z1,
                        20,
                        60,
                        1,
                        1,
                        2,
                        "0",
                        "--------------------------",
                        Godex.AsiaEncoding.BIG5
                    )
                    Godex.AsiaFont_TextOut(
                        Godex.AsianFontID.Z1,
                        20,
                        80,
                        1,
                        2,
                        3,
                        "0",
                        tradeItem?.item_name,
                        Godex.AsiaEncoding.BIG5
                    )
                    Godex.AsiaFont_TextOut(
                        Godex.AsianFontID.Z1,
                        20,
                        130,
                        1,
                        1,
                        3,
                        "0",
                        "備註:",
                        Godex.AsiaEncoding.BIG5
                    )
                    Godex.AsiaFont_TextOut(
                        Godex.AsianFontID.Z1,
                        20,
                        160,
                        1,
                        1,
                        3,
                        "0",
                        tradeItem?.item_dec,
                        Godex.AsiaEncoding.BIG5
                    )
                    Godex.AsiaFont_TextOut(
                        Godex.AsianFontID.Z1,
                        20,
                        210,
                        1,
                        1,
                        1,
                        "0",
                        userInfoModel?.result?.store?.get(0)?.store_name+" "+userInfoModel?.result?.store?.get(0)?.phone,
                        Godex.AsiaEncoding.BIG5
                    )
//            AsiaFont_TextOut(AsianFontID.Z1,20,20,1,1,2,"0","APP 100",AsiaEncoding.BIG5)
//            AsiaFont_TextOut(AsianFontID.Z1,20,40,1,1,2,"0","--------------------------",AsiaEncoding.BIG5)
//            AsiaFont_TextOut(AsianFontID.Z1,20,70,2,2,3,"0","招牌漢堡",AsiaEncoding.BIG5)
//            AsiaFont_TextOut(AsianFontID.Z1,20,130,1,1,3,"0","備註:不要洋蔥",AsiaEncoding.BIG5)
//            AsiaFont_TextOut(AsianFontID.Z1,20,210,1,1,2,"0","07-123456",AsiaEncoding.BIG5)
                    Godex.sendCommand("E")
                }
            }
            Godex.close()
        }
    }

    private fun printReceipt() {
        pos?.initPos()
        //初始化订单数据
        initData()

        pos?.bold(true)
        pos?.printTabSpace(2)
        pos?.printWordSpace(1)
        pos?.printText(tradeRecordModel?.result?.store?:"")

        pos?.printLocation(0)
        pos?.printTextNewLine("------------------------------------------------")
        pos?.bold(false)
        pos?.printTextNewLine("訂 單 編 號：${tradeRecordModel?.result?.tradeNo}")
        pos?.printTextNewLine("訂 單 日 期：${tradeRecordModel?.result?.createTime?.substring(0,16)}")
        pos?.printTextNewLine("訂 購 人：${tradeRecordModel?.result?.userName}")
        pos?.printTextNewLine("連 絡 電 話：${tradeRecordModel?.result?.account}")
        pos?.printLine(2)
        pos?.printText("品項")
        pos?.printLocation(20, 1)
//                        pos?.printWordSpace(1)
        pos?.printText("數量")
        pos?.printWordSpace(5)
        pos?.printText("小計")
        pos?.printTextNewLine("------------------------------------------------")

        for (foods in foodsBean!!) {
            pos?.printTextNewLine(foods.getName())
            pos?.printLocation(20, 1)
            pos?.printWordSpace(1)
            pos?.printText(foods.getNumber())
            pos?.printWordSpace(6)
            pos?.printText(foods.getPrice())
            pos?.printTextNewLine(foods.getDec())
            pos?.printLocation(0)
        }
        pos?.printLine(1)
        pos?.printText("------------------------------------------------")
        pos?.printLine(1)

        pos?.printLocation(2)
        pos?.printTextNewLine("小 計：${tradeRecordModel?.result?.discount?.toInt()?:0+total}")
        pos?.printTextNewLine("折 扣：${tradeRecordModel?.result?.discount?.toInt()?:0}")
        pos?.printTextNewLine("總 計：$total")
        pos?.printLine(2)

        //切纸
        pos?.feedAndCut()
        pos?.closeIOAndSocket()
        pos = null
    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                progressBar7.visibility = View.INVISIBLE
                androidx.appcompat.app.AlertDialog.Builder(this@STradeDetailRecordActivity)
                    .setTitle("訊息提示")
                    .setMessage("無法正常執行，請稍後再試！")
                    .setCancelable(false)
                    .setPositiveButton("確定") {d,w ->
                        finish()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {

            val mResponse = response.body()?.string()
            val listType = object : TypeToken<TradeDetailRecordModel>() {}.type
            tradeRecordModel = Gson().fromJson(mResponse,listType)

            if (tradeRecordModel?.success == true && tradeRecordModel?.result?.item?.isNotEmpty() == true) {
                tradeItemList = tradeRecordModel?.result?.item
                var time = tradeRecordModel?.result?.createTime
                runOnUiThread {
                    progressBar7.visibility = View.INVISIBLE
                    nAdapter?.updateData(tradeItemList)
                    tradeDetailRecordNo.text = "訂單編號：${intent.getStringExtra(TRADENO)}"
                    tradeDetailRecordDate.text = "訂單日期：${time?.substring(0,time.length-3)}"
                    tradeDetailRecordStore.text = "店家：${tradeRecordModel?.result?.store}"
                    tradeDetailRecordName.text = "訂購人：${tradeRecordModel?.result?.userName}"
                    tradeDetailRecordPhone.text = "聯絡電話：${tradeRecordModel?.result?.account}"
                    val discount = tradeRecordModel?.result?.discount?.toInt()?:0
                    tradeDetailRecordAmount.text = "小計：$ ${total+discount}"
                    tradeDetailRecordDiscount.text = "折扣：$ $discount"
                    tradeDetailRecordTotal.text = "總計：$ $total"
                }
            } else {
                runOnUiThread {
                    progressBar7.visibility = View.INVISIBLE
                    androidx.appcompat.app.AlertDialog.Builder(this@STradeDetailRecordActivity)
                        .setTitle("訊息提示")
                        .setMessage("資料或系統出現問題，請稍後再試！")
                        .setCancelable(false)
                        .setPositiveButton("確定") {d,w ->
                            finish()
                        }
                        .show()
                }
            }
        }
    }
}