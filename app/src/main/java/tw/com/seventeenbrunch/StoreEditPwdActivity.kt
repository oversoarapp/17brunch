package tw.com.seventeenbrunch

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_edit_pwd.*
import okhttp3.Call
import okhttp3.Response
import org.json.JSONObject
import tw.com.seventeenbrunch.Api.EditPwdApi
import tw.com.seventeenbrunch.Model.UserInfoDataModel
import tw.com.seventeenbrunch.Model.UserInfoModel
import java.io.IOException

class StoreEditPwdActivity : AppCompatActivity() {

    companion object {
        const val USERRESPONSE = "userResponse"
    }

    private var userInfoData:UserInfoDataModel? = null
    private var mResponse = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_pwd)

        initView()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("是否離開APP？")
                .setNegativeButton("確定") { dialog, which ->
                    finish()
                }
                .setPositiveButton("取消") { dialog, which ->
                    dialog.cancel()
                }
                .show()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun initView() {

        mResponse = intent.getStringExtra(USERRESPONSE)
        val listType = object : TypeToken<UserInfoModel>() {}.type
        val userInfoModel: UserInfoModel = Gson().fromJson(mResponse, listType)
        userInfoData = userInfoModel.result

        view.setBackgroundColor(this.getColor(R.color.colorDarkPeach))
        conLayout.setBackgroundColor(this.getColor(R.color.colorLightPeach))
        textView.setTextColor(this.getColor(R.color.colorWhite))
        navigation_view.menu.clear()
        navigation_view.inflateMenu(R.menu.store_menu)

        editPwdDone.setOnClickListener {
            var acc = userInfoData?.account ?: ""
            var oldPwd = editPwdOldPwd.text.toString()
            var newPwd = editPwdNewPwd.text.toString()
            var rePwd = editPwdRePwd.text.toString()
            if (oldPwd == userInfoData?.password) {
                if (oldPwd.isNotEmpty() && newPwd.isNotEmpty() && rePwd.isNotEmpty()) {
                    if (newPwd.length >= 4 && rePwd.length >= 4) {
                        if (oldPwd != newPwd) {
                            if (newPwd == rePwd) {
                                progressBar10.visibility = View.VISIBLE
                                EditPwdApi().editPwd(acc, "1", newPwd, Callback())
                            } else {
                                Toast.makeText(this, "重複密碼有誤！", Toast.LENGTH_LONG).show()
                            }
                        } else {
                            Toast.makeText(this, "新密碼不可與舊密碼相同！", Toast.LENGTH_LONG).show()
                        }
                    } else {
                        Toast.makeText(this, "密碼需符合四碼以上！", Toast.LENGTH_LONG).show()
                    }
                } else {
                    Toast.makeText(this, "欄位皆不可空白！", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(this, "原密碼錯誤！", Toast.LENGTH_LONG).show()
            }
        }

        editPwdMenu.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,mResponse,this).StoreMenu())

    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                e.printStackTrace()
                progressBar10.visibility = View.INVISIBLE
                android.app.AlertDialog.Builder(this@StoreEditPwdActivity).setTitle("錯誤訊息")
                    .setMessage("無法正常執行，請稍後再試！")
                    .setPositiveButton("確定") { dialog, which ->
                        dialog.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {

            mResponse = response.body()?.string()?:""
            val listType = object : TypeToken<UserInfoModel>() {}.type
            val userInfoModel: UserInfoModel = Gson().fromJson(mResponse, listType)
            userInfoData = userInfoModel.result

            if (userInfoModel.success == true){
                runOnUiThread {
                    progressBar10.visibility = View.INVISIBLE
                    navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,mResponse,this@StoreEditPwdActivity).StoreMenu())
                    val sp = getSharedPreferences("userData", Context.MODE_PRIVATE)
                    sp?.edit()?.putString("pwd",editPwdNewPwd.text.toString())?.apply()
                    editPwdOldPwd.text.clear()
                    editPwdNewPwd.text.clear()
                    editPwdRePwd.text.clear()
                    Toast.makeText(this@StoreEditPwdActivity,"修改成功！",Toast.LENGTH_LONG).show()
                }
            } else {
                runOnUiThread {
                    progressBar10.visibility = View.INVISIBLE
                    Toast.makeText(this@StoreEditPwdActivity,"修改失敗，請稍後再試！",Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}