package tw.com.seventeenbrunch

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import tw.com.seventeenbrunch.Adapter.CustomerOrderAdapter
import tw.com.seventeenbrunch.Api.GetMenuApi
import kotlinx.android.synthetic.main.activity_customer_order.*
import kotlinx.android.synthetic.main.customer_info_dialog.*
import okhttp3.Call
import okhttp3.Response
import tw.com.seventeenbrunch.Model.*
import tw.com.seventeenbrunch.seventeenbrunch.orderItem
import java.io.IOException

class CustomerOrderActivity : AppCompatActivity() {

    companion object {
        const val USERRESPONSE = "userResponse"
        const val STOREID = "storeId"
    }

    private var switch = false
    private var menuTypeModelList: List<OrderMenuTypeModel>? = null
    private var menuItemList: List<OrderMenuItemModel>? = null
    private var nAdapter: CustomerOrderAdapter? = null
    private var mOrderItem = mutableListOf<OrderDataModel>()
    private val itemClick:(String, String, Int) -> Unit = { name, qty, position ->
        val price = menuItemList?.getOrNull(position)?.price?.toInt() ?: 0
        var orderDataModel = OrderDataModel(
            menuItemList?.getOrNull(position)?.name ?: "",
            qty.toInt(),
            price,
            "",
            qty.toInt() * price,
            menuTypeModelList?.getOrNull(customerOrderTabLayout.selectedTabPosition)?.id.toString()
        )
        mOrderItem.add(orderDataModel)
        Toast.makeText(this, "加入購物車成功！", Toast.LENGTH_SHORT).show()
        if (customerOrderShoppingCartQty.text.isNotEmpty()) {
            customerOrderShoppingCartQty.text = mOrderItem.size.toString()
        } else {
            customerOrderShoppingCartQty.text = "1"
        }
    }


    override fun onResume() {
        super.onResume()

        nAdapter = CustomerOrderAdapter(mOrderItem,itemClick)
        customerOrderRv.adapter = nAdapter
        nAdapter?.updateData(menuTypeModelList?.getOrNull(customerOrderTabLayout?.selectedTabPosition?:0)?.item)
        menuItemList = menuTypeModelList?.getOrNull(customerOrderTabLayout?.selectedTabPosition?:0)?.item

        if (orderItem.size != 0) {
            customerOrderShoppingCartQty.text = orderItem.size.toString()
        } else {
            customerOrderShoppingCartQty.text = ""
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_order)

        initView()

    }

    private fun initView() {

        GetMenuApi().getMenu(intent.getStringExtra(STOREID)?:"",Callback())
        customerOrderRv.layoutManager = LinearLayoutManager(this)

        val response = intent.getStringExtra(USERRESPONSE)
        val listType = object : TypeToken<UserInfoModel>() {}.type
        val userInfoModel: UserInfoModel = Gson().fromJson(response, listType)
        val userInfoData = userInfoModel.result

        for (i in 0 until userInfoData?.points?.size!!) {
            if (userInfoData.points.getOrNull(i)?.id == intent.getStringExtra(STOREID)?:"") {
                money.text = "$ ${userInfoData?.points?.getOrNull(i)?.points}"
            }
        }

        customerOrderShoppingCart.setOnClickListener {
            if (customerOrderShoppingCartQty.text.toString() != "") {
                orderItem = mOrderItem
//                nAdapter = CustomerOrderAdapter(this@CustomerOrderActivity,itemClick)
//                customerOrderRv.adapter = nAdapter
//                nAdapter?.updateData(menuTypeModelList?.getOrNull(customerOrderTabLayout?.selectedTabPosition?:0)?.item)
                val storeId = intent.getStringExtra(STOREID)
                val mIntent = Intent()
                mIntent.putExtra(CustomerShoppingCartActivity.STOREID,storeId)
                mIntent.putExtra(CustomerShoppingCartActivity.USERRESPONSE,intent.getStringExtra(USERRESPONSE))
                mIntent.setClass(this, CustomerShoppingCartActivity::class.java)
                startActivity(mIntent)
            } else {
                AlertDialog.Builder(this@CustomerOrderActivity).setTitle("錯誤訊息")
                    .setMessage("您尚未選購餐點！" )
                    .setPositiveButton("確定") { dialog, which -> dialog.cancel() }
                    .show()
            }
        }

        customerOrderTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                nAdapter = CustomerOrderAdapter(mOrderItem,itemClick)
                customerOrderRv.adapter = nAdapter
                nAdapter?.updateData(menuTypeModelList?.getOrNull(tab?.position?:0)?.item)
                menuItemList = menuTypeModelList?.getOrNull(tab?.position?:0)?.item
            }
        })

    }

    inner class Callback : okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                e.printStackTrace()
                progressBar.visibility = View.INVISIBLE
                android.app.AlertDialog.Builder(this@CustomerOrderActivity).setTitle("錯誤訊息")
                    .setMessage("無法正常執行，請稍後再試！" )
                    .setCancelable(false)
                    .setPositiveButton("關閉") { dialog, which -> finish() }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {

            val result = response.body()!!.string()
            val listType = object : TypeToken<OrderMenuModel>() {}.type
            val menuList:OrderMenuModel = Gson().fromJson(result.toString(), listType)

            if(menuList.success == true) {
                if (!menuList.result.isNullOrEmpty()) {
                    menuTypeModelList = menuList.result
                    runOnUiThread {
                        progressBar.visibility = View.INVISIBLE
                        for (i in menuTypeModelList!!.indices) {
                            customerOrderTabLayout.addTab(customerOrderTabLayout.newTab())
                            customerOrderTabLayout.getTabAt(i)?.text = menuTypeModelList?.getOrNull(i)?.name
                        }
                        //更新第一個類別的菜單
                        nAdapter?.updateData(menuTypeModelList?.getOrNull(0)?.item)
                    }
                } else {
                    runOnUiThread {
                        Toast.makeText(this@CustomerOrderActivity,"尚無菜單資料！",Toast.LENGTH_LONG).show()
                        finish()
                    }
                }
            } else {
                runOnUiThread {
                    Toast.makeText(this@CustomerOrderActivity,"尚無菜單資料！",Toast.LENGTH_LONG).show()
                    finish()
                }
            }
        }
    }
}