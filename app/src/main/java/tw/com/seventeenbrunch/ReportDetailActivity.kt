package tw.com.seventeenbrunch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_report.*
import kotlinx.android.synthetic.main.activity_report_detail.*
import kotlinx.android.synthetic.main.activity_report_detail.drawerLayout
import kotlinx.android.synthetic.main.activity_report_detail.navigation_view
import kotlinx.android.synthetic.main.activity_store_main.*
import okhttp3.Call
import okhttp3.Response
import org.json.JSONObject
import tw.com.seventeenbrunch.Adapter.ReportDetailAdapter
import tw.com.seventeenbrunch.Model.ReportModel
import tw.com.seventeenbrunch.Model.UserInfoModel
import java.io.IOException

class ReportDetailActivity: AppCompatActivity() {

    companion object{
        const val USERRESPONSE = "userResponse"
        const val ITEMDATA = "itemData"
        const val POSITION = "position"
    }
    private val nAdapter = ReportDetailAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_detail)

        initView()

    }

    private fun initView() {

//        val mResponse = intent.getStringExtra(USERRESPONSE)
//        val mListType = object : TypeToken<UserInfoModel>() {}.type
//        val userInfoModel: UserInfoModel = Gson().fromJson(mResponse, mListType)

        val position = intent?.getIntExtra(POSITION,0)?:0
        val response = intent.getStringExtra(ITEMDATA)
        val listType = object : TypeToken<ReportModel>() {}.type
        val reportModel:ReportModel = Gson().fromJson(response, listType)
        val itemDetailModel = reportModel.result.detail?.getOrNull(position)?.items

        reportDetailRv.adapter = nAdapter
        reportDetailRv.layoutManager = LinearLayoutManager(this)
        nAdapter.updateData(itemDetailModel)

        reportMenu2.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,intent.getStringExtra(USERRESPONSE),this).StoreMenu())

    }
}