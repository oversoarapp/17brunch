package tw.com.seventeenbrunch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_store_menu_edit.*
import kotlinx.android.synthetic.main.activity_store_top_up.*
import kotlinx.android.synthetic.main.activity_store_top_up.drawerLayout
import kotlinx.android.synthetic.main.activity_store_top_up.navigation_view
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONObject
import tw.com.seventeenbrunch.Api.CustomerTopUpApi
import tw.com.seventeenbrunch.Model.UserInfoDataModel
import tw.com.seventeenbrunch.Model.UserInfoModel
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class StoreTopUpActivity: AppCompatActivity() {

    companion object {
        const val USERRESPONSE = "userResponse"
    }

    private var userInfoData: UserInfoDataModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_top_up)

        initView()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("是否離開APP？")
                .setNegativeButton("確定") { dialog, which ->
                    finish()
                }
                .setPositiveButton("取消") { dialog, which ->
                    dialog.cancel()
                }
                .show()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun initView() {

        val response = intent.getStringExtra(USERRESPONSE)
        val listType = object : TypeToken<UserInfoModel>() {}.type
        val userInfoModel: UserInfoModel = Gson().fromJson(response, listType)
        userInfoData = userInfoModel.result

        storeTopUpName.text = "儲值店家:${userInfoData?.store?.getOrNull(0)?.store_name}"
        storeTopUpDate.text = "儲值日期:${getNow()}"

        storeTopUpDone.setOnClickListener {
            val storeId = userInfoData?.store?.get(0)?.id?:""
            val userAcc = userInfoData?.account?:""
            val topUpAcc = storeTopUpAcc.text.toString()
            val topUpPoints = storeTopUpPoints.text.toString()

            if (topUpPoints.isNotEmpty() && topUpPoints != "0") {
                //儲值
                AlertDialog.Builder(this)
                    .setTitle("訊息提示")
                    .setMessage("儲值帳號：$topUpAcc\n儲值金額：$ $topUpPoints\n請核對以上儲值資訊是否正確？")
                    .setNegativeButton("確定") {d,w ->
                        progressBar6.visibility = View.VISIBLE
                        CustomerTopUpApi().customerTopUp(userAcc,topUpAcc,topUpPoints,storeId,TopUpCallback())
                    }
                    .setPositiveButton("取消") {d,w ->
                        d.cancel()
                    }
                    .show()
            } else {
                Toast.makeText(this,"請輸入儲值金額！",Toast.LENGTH_SHORT).show()
            }
        }

        storeTopUpMenu.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,response?:"",this).StoreMenu())

    }

    private fun getNow(): String {
        return if (android.os.Build.VERSION.SDK_INT >= 24){
            SimpleDateFormat("yyyy-MM-dd HH:mm").format(Date())
        }else{
            var tms = Calendar.getInstance()
            tms.get(Calendar.YEAR).toString() + "-" + tms.get(Calendar.MONTH).toString() + "-" + tms.get(
                Calendar.DAY_OF_MONTH).toString() + " " + tms.get(Calendar.HOUR_OF_DAY).toString() + ":" + tms.get(
                Calendar.MINUTE).toString() +":" + tms.get(Calendar.SECOND).toString()
        }
    }

    private fun clearData() {
        storeTopUpAcc.text.clear()
        storeTopUpPoints.text.clear()
    }

    inner class TopUpCallback: Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                e.printStackTrace()
                progressBar6.visibility = View.INVISIBLE
                android.app.AlertDialog.Builder(this@StoreTopUpActivity).setTitle("錯誤訊息")
                    .setMessage("無法正常執行，請稍後再試！")
                    .setPositiveButton("確定") { dialog, which ->
                        dialog.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {
            val mResponse = response.body()?.string()
            Log.d("topUp",mResponse)
            val jsonObject = JSONObject(mResponse)
            val success = jsonObject.getBoolean("success")

            if (success) {
                runOnUiThread {
                    Toast.makeText(this@StoreTopUpActivity,"儲值成功！",Toast.LENGTH_LONG).show()
                    progressBar6.visibility = View.INVISIBLE
                    clearData()
                }
            } else {
                runOnUiThread {
                    progressBar6.visibility = View.INVISIBLE
                    AlertDialog.Builder(this@StoreTopUpActivity).setTitle("錯誤訊息")
                        .setMessage("${jsonObject.getString("msg")}")
                        .setPositiveButton("確定") { dialog, which ->
                            dialog.cancel()
                        }
                        .show()
                }
            }
        }
    }
}