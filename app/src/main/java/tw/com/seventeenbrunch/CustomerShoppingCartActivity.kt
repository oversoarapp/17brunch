package tw.com.seventeenbrunch

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_customer_shopping_cart.*
import kotlinx.android.synthetic.main.customer_info_dialog.*
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONObject
import tw.com.seventeenbrunch.Adapter.CustomerShoppingCartAdapter
import tw.com.seventeenbrunch.Api.NewOrderApi
import tw.com.seventeenbrunch.Model.UserCouponDataModel
import tw.com.seventeenbrunch.Model.UserCouponModel
import tw.com.seventeenbrunch.Model.UserInfoDataModel
import tw.com.seventeenbrunch.Model.UserInfoModel
import tw.com.seventeenbrunch.seventeenbrunch.orderItem
import java.io.IOException
import kotlin.math.floor

class CustomerShoppingCartActivity : AppCompatActivity() {

    companion object {
        const val USERRESPONSE = "userResponse"
        const val STOREID = "storeId"
    }

    private var switch = false
    private var nAdapter: CustomerShoppingCartAdapter? = null
    private var userInfo: UserInfoDataModel? = null
    private var limited = 0
    private var discount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_shopping_cart)

        initView()

    }

    private fun initView() {

        val response = intent.getStringExtra(USERRESPONSE)
        val listType = object : TypeToken<UserInfoModel>() {}.type
        val userInfoModel: UserInfoModel = Gson().fromJson(response, listType)
        userInfo = userInfoModel.result

        for (i in 0 until userInfo?.store?.size!!) {
            if (userInfo?.store?.getOrNull(i)?.id.equals(intent?.getStringExtra(STOREID))) {
                limited = userInfo?.store?.getOrNull(i)?.limited?.toInt()?:0
                discount = userInfo?.store?.getOrNull(i)?.discount?.toInt()?:0
                break
            }
        }

        for (i in 0 until userInfo?.points?.size!!) {
            if (userInfo?.points?.getOrNull(i)?.id == intent.getStringExtra(STOREID)?:"") {
                money.text = "$ ${userInfo?.points?.getOrNull(i)?.points}"
            }
        }
        shoppingCartQty.text = orderItem.size.toString()

        var amount = 0
        for (i in 0 until orderItem.size) {
            amount += orderItem.getOrNull(i)?.itemAmount?:0
        }
        shoppingCartAmount.text = amount.toString()

        if ( limited != 0 ) {
            if (amount >= limited) {
                var n = amount / limited.toDouble()
                var mDiscount = discount * floor(n).toInt()
                shoppingCartDiscount.text = mDiscount.toString()
                shoppingCartTotal.text = "${amount - mDiscount}"
            } else {
                shoppingCartDiscount.text = "0"
                shoppingCartTotal.text = amount.toString()
            }
        } else {
            shoppingCartDiscount.text = discount.toString()
            shoppingCartTotal.text = "${amount - discount}"
        }

        val itemCancel: (Int) -> Unit = {
            if (orderItem.size != 0) {
                nAdapter?.itemData?.removeAt(it)
                orderItem = nAdapter?.itemData!!
            }
            if (orderItem.size == 0 ) {
                android.app.AlertDialog.Builder(this@CustomerShoppingCartActivity)
                    .setTitle("訊息提示")
                    .setMessage("您尚未選購餐點！" )
                    .setCancelable(false)
                    .setPositiveButton("確定") { dialog, which -> finish() }
                    .show()
            } else {
                shoppingCartQty.text = orderItem.size.toString()
                var amount = 0
                for (i in 0 until orderItem.size) {
                    amount += orderItem.getOrNull(i)?.itemAmount?:0
                }
                shoppingCartAmount.text = amount.toString()
                nAdapter?.updateData(orderItem)
            }
        }
        val countPrice: (Int) -> Unit = {
            orderItem = nAdapter?.itemData!!
            var amount = 0
            for (i in 0 until orderItem.size) {
                amount += orderItem.getOrNull(i)?.itemAmount ?: 0
            }
            shoppingCartAmount.text = "$amount"
        }
        val saveItemDec: (Int,String) -> Unit = { pos,dec ->
            nAdapter!!.itemData[pos].itemDec = dec
            orderItem = nAdapter?.itemData!!
        }
//        val useCouponClick:() -> Unit = {
//            orderItem = nAdapter?.itemData!!
//            var discount = 0
//            for (i in 0 until orderItem.size) {
//                discount += orderItem.getOrNull(i)?.itemDiscount?:0
//            }
//            shoppingCartDiscount.text = discount.toString()
//        }

        shoppingCartAmount.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                val amount = s.toString().toInt()
                var mDiscount = 0
                if ( limited != 0 ) {
                    if (amount >= limited ) {
                        var n = amount / limited.toDouble()
                        mDiscount = discount * floor(n).toInt()
                        shoppingCartTotal.text = "${s.toString().toInt()-mDiscount}"
                        shoppingCartDiscount.text = mDiscount.toString()
                    } else {
                        shoppingCartTotal.text = "${s.toString().toInt()-mDiscount}"
                        shoppingCartDiscount.text = "0"
                    }
                } else {
                    shoppingCartTotal.text = "${s.toString().toInt()-mDiscount}"
                    shoppingCartDiscount.text = discount.toString()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        shoppingCartDone.setOnClickListener {
            var orderItemJson = Gson().toJson(orderItem)
            var total = shoppingCartTotal.text.toString()
            var mDiscount = shoppingCartDiscount.text.toString()
            var points = 0
            for (i in 0 until userInfo?.points?.size!!) {
                if (userInfo?.points?.getOrNull(i)?.id == intent.getStringExtra(STOREID)?:"") {
                    points = userInfo?.points?.getOrNull(i)?.points?:0
                }
            }
            if (points >= total.toInt()) {
                android.app.AlertDialog.Builder(this@CustomerShoppingCartActivity)
                    .setTitle("訊息提示")
                    .setMessage("請確認訂單是否正確？" )
                    .setNegativeButton("確定") { dialog, which ->
                        progressBar7.visibility = View.VISIBLE
                        shoppingCartDone.isEnabled = false
                        shoppingCartDone.isFocusableInTouchMode = false
                        NewOrderApi().newOrder(
                            userInfo?.account ?: "",
                            intent.getStringExtra(STOREID) ?: "",
                            orderItemJson,
                            total,
                            mDiscount,
                            NewOrderCallback()
                        )
                    }
                    .setPositiveButton("取消") {d,w ->
                        d.cancel()
                    }
                    .show()
            } else {
                AlertDialog.Builder(this)
                    .setTitle("訊息提示")
                    .setMessage("提醒您！您的餘額不足，導致交易失敗，別忘記儲值喔！")
                    .show()
            }
        }

        shoppingCartCancel.setOnClickListener {
            val mIntent = Intent(this,CustomerActivity::class.java)
            mIntent.putExtra(CustomerActivity.USERRESPONSE,intent?.getStringExtra(USERRESPONSE))
            startActivity(mIntent)
            finishAffinity()
        }

        nAdapter = CustomerShoppingCartAdapter(this, countPrice, itemCancel, saveItemDec)
        shoppingCartRv.layoutManager = LinearLayoutManager(this)
        shoppingCartRv.adapter = nAdapter
        nAdapter?.updateData(orderItem)

    }

//    inner class GetCouponCallback: okhttp3.Callback {
//        override fun onFailure(call: Call, e: IOException) {
//            runOnUiThread {
//                e.printStackTrace()
//                progressBar7.visibility = View.INVISIBLE
//                android.app.AlertDialog.Builder(this@CustomerShoppingCartActivity).setTitle("錯誤訊息")
//                    .setMessage("無法正常執行，請稍後再試！" )
//                    .setCancelable(false)
//                    .setPositiveButton("關閉") { dialog, which -> finish() }
//                    .show()
//            }
//        }
//
//        @SuppressLint("SetTextI18n")
//        override fun onResponse(call: Call, response: Response) {
//            val mResponse = response.body()?.string()
//            Log.d("userCoupon",mResponse)
//            val listType = object : TypeToken<UserCouponModel>() {}.type
//            val model:UserCouponModel = Gson().fromJson(mResponse.toString(), listType)
//
//            if (model.success) {
//                couponDataModel = model.result
//
//                for (i in 0 until orderItem.size) {
//                    val mQty = orderItem.getOrNull(i)?.itemQty?:0
//                    val mPrice = orderItem.getOrNull(i)?.itemPrice?:0
//                    orderItem.getOrNull(i)?.itemAmount = mQty*mPrice
//                }
//
//                var amount = 0
//                for (i in 0 until orderItem.size) {
//                    amount += orderItem.getOrNull(i)?.itemAmount?:0
//                }
//
//                var flagList = mutableListOf<Boolean>()
//                for (i in 0 until orderItem.size) {
//                    flagList.add(false)
//                }
//                var haveCouponList = mutableListOf<Boolean>()
//                for (i in 0 until orderItem.size) {
//                    haveCouponList.add(false)
//                }
//
//                runOnUiThread {
//                    progressBar7.visibility = View.INVISIBLE
////                    orderItem.removeAt(0)
//                    nAdapter?.updateData(flagList,haveCouponList,orderItem,couponDataModel)
//                    shoppingCartAmount.text = amount.toString()
//                    shoppingCartDiscount.text = "0"
//                    shoppingCartTotal.text = amount.toString()
//                }
//            }
//        }
//    }

    inner class NewOrderCallback: Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                e.printStackTrace()
                progressBar7.visibility = View.INVISIBLE
                android.app.AlertDialog.Builder(this@CustomerShoppingCartActivity).setTitle("錯誤訊息")
                    .setMessage("訂單送出失敗，系統或網路出現問題，請稍後再試！" )
                    .setCancelable(false)
                    .setPositiveButton("關閉") { dialog, which -> finish() }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {
            val mResponse = response.body()?.string()
            val jsonObject = JSONObject(mResponse?:"")
            val success = jsonObject.getBoolean("success")
            if (success) {
                runOnUiThread {
                    Toast.makeText(this@CustomerShoppingCartActivity,"訂單送出成功！",Toast.LENGTH_LONG).show()
                    val mIntent = Intent()
                    mIntent.putExtra(CustomerActivity.USERRESPONSE,intent.getStringExtra(
                        USERRESPONSE))
                    mIntent.putExtra(CustomerActivity.FROM,"order")
                    mIntent.setClass(this@CustomerShoppingCartActivity,CustomerActivity::class.java)
                    startActivity(mIntent)
                    finishAffinity()
                }
            } else {
                runOnUiThread {
                    val msg = jsonObject.getString("msg")
                    progressBar7.visibility = View.INVISIBLE
                    android.app.AlertDialog.Builder(this@CustomerShoppingCartActivity)
                        .setTitle("錯誤訊息")
                        .setMessage("$msg，訂單送出失敗！" )
                        .setCancelable(false)
                        .setPositiveButton("確定") { dialog, which ->
                            val mIntent = Intent()
                            mIntent.putExtra(CustomerActivity.USERRESPONSE,intent.getStringExtra(
                                USERRESPONSE))
                            mIntent.putExtra(CustomerActivity.FROM,"order")
                            mIntent.setClass(this@CustomerShoppingCartActivity,CustomerActivity::class.java)
                            startActivity(mIntent)
                            finishAffinity()
                        }
                        .show()
                }
            }
        }
    }
}