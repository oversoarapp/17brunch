package tw.com.seventeenbrunch

import android.content.Context
import tw.com.seventeenbrunch.Model.OrderDataModel

object seventeenbrunch {

    val apiHost = "https://17brunch.com.tw/17BrunchWeb/public/api/"
    val getAllStoreApiUrl = "${apiHost}getAllStore"
    val getAllUserApiUrl = "${apiHost}getAllUser"
    val getMenuApiUrl = "${apiHost}getMenu"
    val loginApiUrl = "${apiHost}login"
    val logoutApiUrl = "${apiHost}logout"
    val signUpApiUrl = "${apiHost}signUp"
    val editPwdApiUrl = "${apiHost}editPwd"
    val forgetPwdApiUrl = "${apiHost}forgetPwd"
    val customerTopUpApiUrl = "${apiHost}customerTopUp"
    val storeTopUpApiUrl = "${apiHost}storeTopUp"
    val actionToMenuTypeApiUrl = "${apiHost}actionToMenuType"
    val actionToMenuItemApiUrl = "${apiHost}actionToMenuItem"
    val addStoreApiUrl = "${apiHost}addStore"
    val deleteStoreApiUrl = "${apiHost}deleteStore"
    val editStoreApiUrl = "${apiHost}editStore"
    val setStoreCouponApiUrl = "${apiHost}setStoreCoupon"
    val getCouponApiUrl = "${apiHost}getCoupon"
    val addCouponApiUrl = "${apiHost}addCoupon"
    val deleteCouponApiUrl = "${apiHost}deleteCoupon"
    val couponTopUpApiUrl = "${apiHost}couponTopUp"
    val newOrderApiUrl = "${apiHost}newOrder"
    val getTradeRecordApiUrl = "${apiHost}getTradeRecord"
    val getTradeDetailRecordApiUrl = "${apiHost}getTradeDetailRecord"
    val updateOrderStatusApiUrl = "${apiHost}updateOrderStatus"
    val getPointsTradeRecordApiUrl = "${apiHost}getPointTradeRecord"
    val editInfoApiUrl = "${apiHost}editInfo"
    val getTopUpRecordApiUrl = "${apiHost}getTopUpRecord"
    val getReportApiUrl = "${apiHost}getReport"

    var orderItem = mutableListOf(OrderDataModel())

}