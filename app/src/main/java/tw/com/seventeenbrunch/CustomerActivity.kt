package tw.com.seventeenbrunch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_customer_main.*
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.Call
import okhttp3.Response
import org.json.JSONObject
import tw.com.seventeenbrunch.Adapter.CustomerMoneyAdapter
import tw.com.seventeenbrunch.Api.LoginApi
import tw.com.seventeenbrunch.Model.UserInfoDataModel
import tw.com.seventeenbrunch.Model.UserInfoModel
import tw.com.seventeenbrunch.seventeenbrunch.orderItem
import java.io.IOException

class CustomerActivity : AppCompatActivity() {

    companion object {
        const val USERRESPONSE = "userResponse"
        const val FROM = "from"
    }

    private var nAdapter: CustomerMoneyAdapter? = null
    private var mResponse = ""
    private var userInfoData: UserInfoDataModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_main)

        initView()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("是否離開APP？")
                .setNegativeButton("確定") { dialog, which ->
                    finish()
                }
                .setPositiveButton("取消") { dialog, which ->
                    dialog.cancel()
                }
                .show()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun initView() {

        mResponse = intent.getStringExtra(USERRESPONSE)?:""
        val listType = object : TypeToken<UserInfoModel>() {}.type
        val userInfoModel: UserInfoModel = Gson().fromJson(mResponse, listType)
        userInfoData = userInfoModel.result

        if (intent?.getStringExtra(FROM)?.equals("order") == true) {
            var acc = userInfoData?.account ?: ""
            var pwd = userInfoData?.password ?: ""
            var identity = userInfoData?.identity.toString()
            var token = userInfoData?.token?:""
            progressBar11.visibility = View.VISIBLE
            LoginApi().login(acc, pwd, identity, token, Callback())
        }

        customerMainName.text = userInfoData?.name
        var points = mutableListOf<String>()
        for (i in 0 until userInfoData?.points?.size!!) {
            points.add("${userInfoData?.points?.getOrNull(i)?.store_name}:${userInfoData?.points?.getOrNull(i)?.points}元\n")
        }

        nAdapter = CustomerMoneyAdapter(points)
        moneyRv.layoutManager = LinearLayoutManager(this)
        moneyRv.adapter = nAdapter

        //取得店家資訊
        val storeList = mutableListOf<String>()
        val size = userInfoData?.store?.size?:0
        if (size > 0) {
            for (i in 0 until size) {
                storeList.add(userInfoData?.store?.get(i)?.store_name ?: "")
            }
        }
        //塞進spinner
        val storeArrayAdapter =
            ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, storeList)
        storeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        customerMainSpinner.adapter = storeArrayAdapter
        if (intent?.getStringExtra(FROM)?.equals("order") != true) {
            customerMainSpinner.onItemSelectedListener = SpinnerListener()
        }
        navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,mResponse,this).CustomerMenu())

        shoppingCartMenu.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers()
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }

        customerMainOrder.setOnClickListener {
            orderItem.clear()
            val status = userInfoData?.store?.get(customerMainSpinner.selectedItemPosition)?.status
            val storeId =
                userInfoData?.store?.get(customerMainSpinner.selectedItemPosition)?.id
            if (status != "0") {
                if (userInfoData?.points?.getOrNull(customerMainSpinner.selectedItemPosition)?.points?:0 > 0) {
                    val mIntent = Intent()
                    mIntent.putExtra(CustomerOrderActivity.USERRESPONSE, mResponse)
                    mIntent.putExtra(CustomerOrderActivity.STOREID, storeId)
                    mIntent.setClass(this, CustomerOrderActivity::class.java)
                    startActivity(mIntent)
                } else {
                    Toast.makeText(this, "您在此店家的儲值金額不足", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(this, "此店家休息中。", Toast.LENGTH_LONG).show()
            }
        }

        customerMainRefresh.setOnClickListener {
            var acc = userInfoData?.account?:""
            var pwd = userInfoData?.password?:""
            var identity = userInfoData?.identity.toString()
            var token = userInfoData?.token?:""
            progressBar11.visibility = View.VISIBLE
            LoginApi().login(acc,pwd,identity,token,Callback())
        }
    }

    private fun checkPoints(points:Int) {
        if (points < 100) {
            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("提醒您！您的儲值金額已低於 $100 ，別忘記儲值哦！")
                .setPositiveButton("確定") {d,w ->
                    d.cancel()
                }
                .show()
        }
    }

    inner class Callback: okhttp3.Callback {
        override fun onFailure(call: Call, e: IOException) {
            runOnUiThread {
                e.printStackTrace()
                progressBar11.visibility = View.INVISIBLE
                android.app.AlertDialog.Builder(this@CustomerActivity).setTitle("錯誤訊息")
                    .setMessage("無法正常執行，請稍後再試！")
                    .setPositiveButton("確定") { dialog, which ->
                        dialog.cancel()
                    }
                    .show()
            }
        }

        override fun onResponse(call: Call, response: Response) {
            mResponse = response.body()?.string()?:""
            val listType = object : TypeToken<UserInfoModel>() {}.type
            val userInfoModel: UserInfoModel = Gson().fromJson(mResponse, listType)
            userInfoData = userInfoModel.result
            if (userInfoModel.success == true) {
                runOnUiThread {
                    progressBar11.visibility = View.INVISIBLE
                    navigation_view.setNavigationItemSelectedListener(NavLis(drawerLayout,mResponse,this@CustomerActivity).CustomerMenu())
                    customerMainName.text = userInfoData?.name

                    var points = mutableListOf<String>()
                    for (i in 0 until userInfoData?.points?.size!!) {
                        points.add("${userInfoData?.points?.getOrNull(i)?.store_name}:${userInfoData?.points?.getOrNull(i)?.points}元\n")
                    }
                    nAdapter?.updateData(points)

                    //取得店家資訊
                    val storeList = mutableListOf<String>()
                    val size = userInfoData?.store?.size?:0
                    if (size > 0) {
                        for (i in 0 until size) {
                            storeList.add(userInfoData?.store?.get(i)?.store_name ?: "")
                        }
                    }
                    //塞進spinner
                    val storeArrayAdapter =
                        ArrayAdapter(this@CustomerActivity, R.layout.support_simple_spinner_dropdown_item, storeList)
                    storeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    customerMainSpinner.adapter = storeArrayAdapter
                    customerMainSpinner.onItemSelectedListener = SpinnerListener()
//                    checkPoints(userInfoData?.points?.getOrNull(customerMainSpinner.selectedItemPosition)?.points?:0)
                }
            } else {
                runOnUiThread {
                    progressBar11.visibility = View.INVISIBLE
                    AlertDialog.Builder(this@CustomerActivity)
                        .setTitle("錯誤提示")
                        .setCancelable(false)
                        .setMessage("${userInfoModel.msg}")
                        .setPositiveButton("確定") { d,w ->
                            finishAffinity()
                        }
                        .show()
                }
            }
        }
    }

    inner class SpinnerListener: AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {

        }

        override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
        ) {
            checkPoints(userInfoData?.points?.get(position)?.points ?: 0)
            val storeData = userInfoData?.store?.get(position)
            customerMainStorePhone.text = "電話:${storeData?.phone}"
            customerMainStoreBh.text = "營業時間:${storeData?.business_hours}"
            customerMainStoreAds.text = "地址:${storeData?.address}"
            customerMainStoreMB.text = "手機:${storeData?.mobilePhone}"
            when (storeData?.status) {
                "0" -> {
                    customerMainStatus.setTextColor( applicationContext.getColor(R.color.colorWhite))
                    customerMainStatus.background = resources.getDrawable(R.drawable.spinner_red_shape)
                    customerMainStatus.text = "店面狀態:休息中"
                }
                "1" -> {
                    customerMainStatus.background = resources.getDrawable(R.drawable.spinner_green_shape)
                    customerMainStatus.setTextColor( applicationContext.getColor(R.color.colorWhite))
                    customerMainStatus.text = "店面狀態:營業中"
                }
            }
            if (storeData?.limited != "0" && storeData?.discount != "0") {
                customerMainEvent.text =
                    "消費滿:${storeData?.limited}元 即送 ${storeData?.discount}元折扣 !!"
            } else {
                customerMainEvent.text = ""
            }
        }
    }
}